#include <iostream>
#include <unistd.h>

int reverseDigits(int number1);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }

    int number1;
    std::cin >> number1;

    std::cout << "Reverse dight is " << reverseDigits(number1) << std::endl;

    return 0;
}

int
reverseDigits(int number1)
{
    int counter = 0;
    while (number1 > 0) {
        counter = counter * 10 + number1 % 10;
        number1 /= 10;
    }
    return counter;
}

