#include "Employee.hpp"

#include <iostream>

Employee::Employee(std::string name, std::string surName, int monthSalary)
{
    setName(name);
    setSurName(surName);
    setMonthSalary(monthSalary);
}

void
Employee::setName(std::string name)
{
    name_ = name;
}

std::string
Employee::getName()
{
    return name_;
}

void
Employee::setSurName(std::string surName)
{
    surName_ = surName;
}

std::string
Employee::getSurName()
{
    return surName_;
}

int
Employee::setMonthSalary(int monthSalary)
{
    if (0 > monthSalary) {
        std::cout << "Warning 1: salary can't be negative" << std::endl
                  << "Reseting it to 0" << std::endl;
        return monthSalary_ = 0;
    }
    return monthSalary_ = monthSalary;
}

int
Employee::getMonthSalary()
{
    return monthSalary_;
}

int
Employee::getYearSalary()
{
    return monthSalary_ * 12;
}

int
Employee::getRaiseSalary()
{
    return monthSalary_ * 12 + monthSalary_ * 1.2;
}

