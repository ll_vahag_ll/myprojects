#include "headers/PhoneNumber.hpp"
#include <iomanip>

std::ostream&
PhoneNumber::operator<<(std::ostream& output)
{
    output << "(" << getAreaCode() << ") "
           << getExchange() << "-" << getLine();
    return output;
}                                     

std::istream& operator>>(std::istream& input, PhoneNumber& number)
{
    input.ignore();                   
    input >> std::setw(3) >> number.areaCode_;
    input.ignore(2);
    input >> std::setw(3) >> number.exchange_;
    input.ignore();
    input >> std::setw(4) >> number.line_;
    return input;
}

std::string
PhoneNumber::getAreaCode() const
{
    return areaCode_;
}

std::string
PhoneNumber::getExchange() const
{
    return exchange_;
}

std::string
PhoneNumber::getLine() const
{
    return line_;
}

