#include <iostream>
#include <unistd.h>
#include <cstring>
#include <cstdlib>

bool isInteractive = ::isatty(STDIN_FILENO);
void compareStrings(char* firstLine, char* secondLine);

int
main()
{
    if (isInteractive) {
        std::cout << "Input two strings: ";
    }
    const int SIZE = 20;
    char firstLine[SIZE];
    std::cin.getline (firstLine, SIZE);
    char secondLine[SIZE];
    std::cin.getline (secondLine, SIZE);
    compareStrings(firstLine, secondLine);
    return 0;
}

void
compareStrings(char* firstLine, char* secondLine)
{
    const int compare = strcmp(firstLine, secondLine);
    if (compare < 0) {
        std::cout << "First string is less than second" << std::endl;
        exit(0);
    }

    if (compare > 0) {
        std::cout << "First string is greater than second" << std::endl;
        exit(0);
    }
    std::cout << "First string is equal to second" << std::endl;
}

