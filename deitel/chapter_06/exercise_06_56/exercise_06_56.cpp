#include <iostream>

int tripleByValue(int count);
int tripleByReference(int& count);

int
main()
{
    std::cout << "Triple by value: ";
    int count = 5;
    for (int i = 0; i <= 3; ++i) {
        tripleByValue(count);
        std::cout << count << ' ';
    }
    std::cout << std::endl;

    std::cout << "Triple by reference: ";
    for (int i = 0; i <= 3; ++i) {
        tripleByReference(count);
        std::cout << count << ' ';
    }
    std::cout << std::endl;
    return 0;
}

int
tripleByValue(int count)
{
    count *= 3;
    return count;
}

int
tripleByReference(int& count)
{
    count *= 3;
    return count;
}

