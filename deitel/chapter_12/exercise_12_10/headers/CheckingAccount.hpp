#ifndef __CHECKINGACCOUNT_HPP__
#define __CHECKINGACCOUNT_HPP__

#include "Account.hpp"

class CheckingAccount : public Account
{
public:
    CheckingAccount(const double balance, const double fee);
    void setFee(const double fee);
    void chargeFee();
    void credit(const double credit);
    bool debit(const double debit);
    double getFee() const;
private:
    double fee_;
};

#endif /// __CHECKINGACCOUNT_HPP__
