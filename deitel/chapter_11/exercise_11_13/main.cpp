#include "headers/Complex.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input first comlex real and imaginary parts: ";
    }
    Complex complex1;
    std::cin >> complex1;


    if (isInteractive) {
        std::cout << "Input second comlex real and imaginary parts: ";
    }
    Complex complex2;
    std::cin >> complex2;

    std::cout << "Adding Complex numbers: ";
    Complex complex3 = complex1 + complex2;
    std::cout << complex3;

    std::cout << "Subtracting Complex numbers: ";
    Complex complex4 = complex1 - complex2;
    std::cout << complex4;

    std::cout << "Multiplication Complex numbers: ";
    Complex complex5 = complex1 * complex2;
    std::cout << complex5;

    if (complex1 == complex2) {
        std::cout << "First complex is equal to second" << std::endl;
    }

    if (complex1 != complex2) {
        std::cout << "First complex is not equal to second" << std::endl;
    }
    return 0;
}

