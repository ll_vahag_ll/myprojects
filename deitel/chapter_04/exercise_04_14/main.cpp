#include <iostream>

#include <iomanip>

int
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Input account number (-1 to end): ";
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (-1 > accountNumber) {
            std::cout << "Error 1: account number can't be negative" << std::endl;
            return 1;
        }


        double beginingBalance;
        std::cout << "Input beginning balance: ";
        std::cin >> beginingBalance;
        if (0 > beginingBalance) {
            std::cout << "Error 2: balance can't be negative" << std::endl;
            return 2;
        }

        double totalCharges;
        std::cout << "Input total charges: ";
        std::cin >> totalCharges;
        if (0 > totalCharges) {
            std::cout << "Error 3: total charges can't be negative" << std::endl;
            return 3;
        }

        double totalCredits;
        std::cout << "Input total credits: ";
        std::cin >> totalCredits;
        if(0 > totalCredits) {
            std::cout << "Error 4: total credits can't be negative" << std::endl;
            return 4;
        }

        double creditLimit;
        std::cout << "input credit limit: ";
        std::cin >> creditLimit;
        if (0 > creditLimit) {
            std::cout << "Error 5: Invalid credit limit" << std::endl;
            return 5;
        }

        double newBalance = beginingBalance + totalCharges - totalCredits;
        std::cout << "New balance is " << newBalance << std::endl;
        if (creditLimit < newBalance) {
            std::cout << std::setprecision(2) << std::fixed;
            std::cout << "Account: " << accountNumber << std::endl;
            std::cout << "Credit limit: " << creditLimit << std::endl;
            std::cout << "Balance: " << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    }
    return 0;
}

