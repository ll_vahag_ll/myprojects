#include "headers/Set.hpp"

#include <gtest/gtest.h>

TEST(Set, constructor)
{
    cd06::Set<int> set1;
    EXPECT_EQ(set1.empty(), true);
}

TEST(Set, insert)
{
    cd06::Set<int> set1;
    set1.insert(30);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

