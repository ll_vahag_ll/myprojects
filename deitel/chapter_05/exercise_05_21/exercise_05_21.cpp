#include <iostream>
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input payment code(-1 to exit): ";
        }
        int paymentCode;
        std::cin >> paymentCode;
        if (-1 == paymentCode) {
            return 0;
        }
        if (paymentCode > 4 || paymentCode < 1) {
            std::cout << "Error 1: Invalid payment code" << std::endl;
            return 1;
        }
        switch (paymentCode) {
        case 1: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input salary: ";
            }
            int salary;
            std::cin >> salary;
            if (salary < 0) {
                std::cout << "Error 2: Invalid salary" << std::endl;
                return 2;
            }
            std::cout << "Managers salary is " << salary << '$' << std::endl;
                }
                break;
        case 2: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input hourly salary: ";
            }
            double hourlyWage;
            std::cin >> hourlyWage;
            if (hourlyWage < 1) {
                std::cout << "Error 3: Invalid hourly salary" << std::endl;
                return 3;
            }
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input hours worked: ";
            }
            double workedHours;
            std::cin >> workedHours;
            if (workedHours < 1) {
                std::cout << "Error 4: Invalid worked hours" << std::endl;
                return 4;
            }
            double salary = workedHours * hourlyWage ;
            if (workedHours > 40) {
                salary += hourlyWage * 0.5 * (workedHours - 40);
            }
            std::cout << "Hourly workers salary is " << salary << '$' << std::endl;
                }
                break;
        case 3: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input weekly sales: ";
            }
            double weeklySales;
            std::cin >> weeklySales;
            if (weeklySales < 0) {
                std::cout << "Error 5: Invalid weekly sales" << std::endl;
                return 5;
            }
            double salary = 250 + 0.057 * weeklySales;
            std::cout << "Commission workers salary is " << salary << '$' << std::endl;
                }
                break;
        case 4: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input price of item: ";
            }
            int price;
            std::cin >> price;
            if (price < 0) {
                std::cout << "Error 6: Invalid item price" << std::endl;
                return 6;
            }
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Input item quantity: ";
            }
            int itemQuantity;
            std::cin >> itemQuantity;
            if (itemQuantity < 0) {
                std::cout << "Error 7: Invalid item quantity" << std::endl;
                return 7;
            }
            std::cout << "Piece workers salary is " << price * itemQuantity << '$' << std::endl;
                }
                break;
        }
    }
    return 0;
}

