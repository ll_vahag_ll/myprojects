#include "headers/SavingsAccount.hpp"
#include <iostream>

int
main()
{
    SavingsAccount saver1(2000.00);
    saver1.calculateMonthlyInterest();
    saver1.increaseBalance();
    saver1.printResult();

    SavingsAccount saver2(3000.00);
    saver2.calculateMonthlyInterest();
    saver2.increaseBalance();
    saver2.printResult();

    SavingsAccount saver3(2000.00);
    saver3.modifyInterestRate(4.0);
    saver3.calculateMonthlyInterest();
    saver3.increaseBalance();
    saver3.printResult();

    SavingsAccount saver4(3000.00);
    saver4.calculateMonthlyInterest();
    saver4.increaseBalance();
    saver4.printResult();

    return 0;
}

