#include <iostream>
#include <list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void
input(std::list<int>& l)
{
    const int SIZE = 10;
    if (isInteractive) {
        std::cout << "Input List elements: ";
    }
    for (int i = 0; i < SIZE; ++i) {
        int value;
        std::cin >> value;
        l.push_back(value);
    }
}

void
printListBackward(const std::list<int>& l, std::list<int>::const_iterator it)
{
    if (it == l.begin()) {
        std::cout << *it;
        return;
    }
    std::cout << *it << ", ";
    return printListBackward(l, --it);
}

int
main()
{
    std::list<int> l;
    input(l);
    std::cout << "List in reverse order: ";
    printListBackward(l, l.end());
    std::cout << std::endl;
    return 0;
}

