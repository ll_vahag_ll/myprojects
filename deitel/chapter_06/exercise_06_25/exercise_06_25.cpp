#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cassert>

int integerPart(const int number1, const int number2);
int remainderPart(const int number1, const int number2);
void seriesOfDigits();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first number: ";
    }

    int number1;
    std::cin >> number1;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second number: ";
    }

    int number2;
    std::cin >> number2;
    if (0 == number2) {
        std::cout << "Error 1: division by 0" << std::endl;
        return 1;
    }

    std::cout << "Integer part is " << integerPart(number1, number2) << "\nRemainder part is " << remainderPart(number1, number2) << std::endl;
    seriesOfDigits();
    std::cout << std::endl;

    return 0;
}

int
integerPart(const int number1, const int number2)
{
    assert(0 != number2);
    return number1 / number2;
}

int
remainderPart(const int number1, const int number2)
{
    assert(0 != number2);
    return number1 % number2;
}

void
seriesOfDigits()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    int number;
    std::cin >> number;
    int counter = 1000;
    while (counter > 0) {
        std::cout << integerPart(number, counter) << "  ";
        number = remainderPart(number, counter);
        counter = integerPart(counter, 10);
    }
}

