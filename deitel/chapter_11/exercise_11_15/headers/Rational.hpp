#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational(const int numerator = 0, const int denominator = 1);
    void setNumber(const int numerator, const int denominator);
    int gcd(int number1, int number2) const;
    Rational operator+(const Rational& rhv) const;
    Rational operator-(const Rational& rhv) const;
    Rational operator*(const Rational& rhv) const;
    Rational operator/(const Rational& rhv) const;
    bool operator>(const Rational& rhv) const;
    bool operator<(const Rational& rhv) const;
    bool operator==(const Rational& rhv) const;
    bool operator!=(const Rational& rhv) const;
    void printResult() const;
private:
    int numerator_;
    int denominator_;
};

#endif ///__RATIONAL_HPP__

