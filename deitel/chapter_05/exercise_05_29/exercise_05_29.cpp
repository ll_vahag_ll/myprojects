#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        double principal = 24.00;
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::setw(28) << "Yearly rate percent: " << rate << '%' << std::endl;
        std::cout << std::fixed << std::setprecision(2);
        double percent = (1.0 + static_cast<double>(rate) / 100);
        for (int year = 1; year <= 379; ++year) {
            principal *= percent;
            std::cout << std::setw(4) << year << std::setw(24) << principal << std::endl;
        }
        std::cout << std::endl;
    }
    return 0;
}

