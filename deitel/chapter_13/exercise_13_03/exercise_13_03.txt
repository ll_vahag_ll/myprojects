Polymorphism enables us to "program in the general" rather than "program in the specific." In particular,
polymorphism enables us to write programs that process objects of classes that are part of the same class hierarchy
as if they are all objects of the hierarchy's base class.
