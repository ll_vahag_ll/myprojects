#include <iostream>
#include <unistd.h>
#include <cassert>
#include <cctype>

void printSquare(const int side1, const char character);
void printTriangle(const int side1, const char character);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input length: ";
    }

    int side1;
    std::cin >> side1;

    if (side1 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input character: ";
    }
    char character;
    std::cin >> character;

    if (0 == std::isprint(character)) {
        std::cout << "Error 2: Character is not printable" << std::endl;
        return 2;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "What do you want to print ?"
            << "\n1 - square\n2 - triangle\nCommand: ";
    }
    int menu;
    std::cin >> menu;
    std::cout << std::endl;

    if (menu > 2 || menu < 1) {
        std::cout << "Eror 3: Invalid menu command" << std::endl;
        return 3;
    }
    if (1 == menu) {
        printSquare(side1, character);
    } else {
        printTriangle(side1, character);
    }
    return 0;
}

void
printSquare(const int side1, const char character)
{
    assert(side1 > 0);
    assert(0 != std::isprint(character));
    for (int counter = 1; counter <= side1; ++counter) {
        for (int calculator = 1; calculator <= side1; ++calculator) {
            std::cout << character;
        }
        std::cout << std::endl;
    }
}

void
printTriangle(const int side1, const char character)
{
    assert(side1 > 0);
    assert(0 != std::isprint(character));
    for (int counter = 1; counter <= side1; ++counter) {
        for (int triangle = 1; triangle <= counter; ++triangle) {
            std::cout << character;
        }
        std::cout << std::endl;
    }
}

