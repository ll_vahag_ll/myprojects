#include "headers/List.hpp"

#include <gtest/gtest.h>

TEST(IntListTest, GeneralTest)
{
    List<int> ls;
    EXPECT_TRUE(ls.isEmpty());
    ls.insertAtBack(1);
    EXPECT_FALSE(ls.isEmpty());
}

TEST(IntListTest, InsertBeforePos)
{
    List<int> ls;
    ls.insertAtBack(1);
    ls.insertAtBack(2);
    ls.insertAtBack(4);
    ls.insertAtBack(5);
    ls.insertAtBack(6);
    ls.print();
    ls.insert(2, 3);
    ls.print();
}

TEST(IntListTest, RemoveBeforePos)
{
    List<int> ls;
    ls.insertAtBack(1);
    ls.insertAtBack(2);
    ls.insertAtBack(3);
    ls.insertAtBack(4);
    ls.insertAtBack(5);
    ls.insertAtBack(6);
    ls.print();
    ls.erase(3);
    ls.print();
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

