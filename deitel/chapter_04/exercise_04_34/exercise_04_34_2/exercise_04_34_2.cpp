#include <iostream>

int
main()
{
    std::cout << "Input encrypted password: ";
    int password;
    std::cin >> password;

    if (password < 1000) {
        std::cout << "Error 1: Input four-dight password" << std::endl;
        return 1;
    }

    if (password > 9999) {
        std::cout << "Error 1: Input four-dight password" << std::endl;
        return 1;
    }

    int number1 = password % 10;
    int number2 = password % 100 / 10;
    int number3 = password % 1000 / 100;
    int number4 = password % 10000 / 1000;

    int original1 = (number1 + 3) % 10;
    int original2 = (number2 + 3) % 10;
    int original3 = (number3 + 3) % 10;
    int original4 = (number4 + 3) % 10;

    std::cout << "Original number is " << original1 << original2 << original3 << original4 << std::endl;
    return 0;
}
