#include "headers/Stack.hpp"

#include <gtest/gtest.h>
#include <vector>

TEST(StackTest, DefaultConst)
{
    cd06::Stack<int> stack1;
}

TEST(StackTest, Equality)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(7);
    stack1.push(7);
    cd06::Stack<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_TRUE(stack1.size() == stack2.size());
}

TEST(StackTest, Less)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Stack<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(10);
    EXPECT_TRUE(stack1.size() < stack2.size());
    EXPECT_TRUE(stack1.top() < stack2.top());
}

TEST(StackTest, More)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Stack<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_FALSE(stack1.size() > stack2.size());
}

TEST(StackTest, LessOrEqual)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Stack<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_TRUE(stack1.size() <= stack2.size());
    stack1.push(7);
    stack1.push(7);
    EXPECT_FALSE(stack1.size() <= stack2.size());

}

TEST(StackTest, MoreOrEqual)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Stack<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_FALSE(stack1.size() >= stack2.size());
    stack1.push(7);
    stack1.push(7);
    EXPECT_TRUE(stack1.size() >= stack2.size());

}

TEST(StackTest, Empty)
{
    cd06::Stack<int> stack1;
    EXPECT_TRUE(stack1.empty());
}

TEST(StackTest, Top)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(8);
    EXPECT_EQ(stack1.top(), 8);
}

TEST(StackTest, pop)
{
    cd06::Stack<int> stack1;
    stack1.push(7);
    stack1.push(8);
    EXPECT_EQ(stack1.top(), 8);
    stack1.pop();
    EXPECT_EQ(stack1.top(), 7);
    EXPECT_EQ(stack1.size(), 1);
}
   
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

