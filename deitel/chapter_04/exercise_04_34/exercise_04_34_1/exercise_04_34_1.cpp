#include <iostream>

int
main()
{
    std::cout << "Input four-digit integer: ";
    int password;
    std::cin >> password;

    if (password < 1000) {
        std::cout << "Error 1: Input four-dight password" << std::endl;
        return 1;
    }

    if (password > 9999) {
        std::cout << "Error 1: Input four-dight password" << std::endl;
        return 1;
    }

    int number1 = password % 10;
    int number2 = password % 100 / 10;
    int number3 = password % 1000 / 100;
    int number4 = password % 10000 / 1000;

    int encrypted1 = (number1 + 7) % 10;
    int encrypted2 = (number2 + 7) % 10;
    int encrypted3 = (number3 + 7) % 10;
    int encrypted4 = (number4 + 7) % 10;

    std::cout << "Encrypted number is " << encrypted3 << encrypted4 << encrypted1 << encrypted2 << std::endl;
    return 0;
}

