#include "MiniComputer.hpp"

#include <iostream>
#include <unistd.h>
#include <cassert>
#include <cstdlib>

MiniComputer::MiniComputer()
{
    setComputerName("Mini");
}

MiniComputer::MiniComputer(std::string name)
{
    setComputerName(name);
}

void
MiniComputer::setComputerName(std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

int
MiniComputer::run()
{
    /// Not implemented yet
    /// Print program name and version
    printProgramNameAndVersion();

    while (true) {
        /// Print main menu
        printMainMenu();

        /// Get command from user
        int command = getCommandFromUser();
        validateCommand(command);

        /// Execute the command
        executeCommand(command);
    } /// Start again from main menu
    return 0;
}

void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << getComputerName() << " 1.0" << std::endl;
}

void
MiniComputer::printMainMenu()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Main Menu:\n";
        for (int i = 0; i <= 4; ++i) {
            printMainMenuItem(i);
        }
        std::cout << "> Command: ";
    }
}

void
MiniComputer::printMainMenuItem(int index)
{
    std::cout << '\t' << index << " - ";
    switch (index) {
    case 0: std::cout << "exit";  break;
    case 1: std::cout << "load";  break;
    case 2: std::cout << "store"; break;
    case 3: std::cout << "print"; break;
    case 4: std::cout << "add";   break;
    default: assert(0 && "Wrong main menu items");
    }
    std::cout << std::endl;
}

void
MiniComputer::printLoadMenu()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Load (into register)\n";
        for (int i = 0; i <= 3; ++i) {
            printLoadMenuItem(i);
        }
        std::cout << "> Register: ";
    }
}

char
MiniComputer::getRegisterName(int index)
{
    return static_cast<char>(index) + 'a';
}

void
MiniComputer::printLoadMenuItem(int index)
{
    std::cout << '\t' << index << " - " << getRegisterName(index) << std::endl;
}

void
MiniComputer::printRegisterMenu(int load)
{
    if (::isatty(STDIN_FILENO)) {
        char registerName = getRegisterName(load);
        std::cout << "Load into " << registerName << std::endl;
        std::cout << "Input the value to load into register " << registerName << std::endl;
        std::cout << "> " << registerName << ": ";
    }
}

void
MiniComputer::setRegisterValue(int load, int registerValue)
{
    switch (load) {
    case 0: registerA_ = registerValue; break;
    case 1: registerB_ = registerValue; break;
    case 2: registerC_ = registerValue; break;
    case 3: registerD_ = registerValue; break;
    default: assert(0 && "There are only four registers");
    }
}

int
MiniComputer::getRegisterValue(int load)
{
    switch (load) {
    case 0: return registerA_;
    case 1: return registerB_;
    case 2: return registerC_;
    case 3: return registerD_;
    default: assert(0 && "There are only four registers");
    }
    return 0;
}

void
MiniComputer::executeLoadIntoRegister(int load)
{
    printRegisterMenu(load);
    int registerValue = getCommandFromUser();
    setRegisterValue(load, registerValue);
    std::cout << getRegisterName(load) << " = " << getRegisterValue(load) << std::endl;
}

void
MiniComputer::printPrintMenu()
{
    if (::isatty(STDIN_FILENO)) {
        for (int i =  0; i <= 1; ++i) {
            printPrintMenuItem(i);
        }
        std::cout << "> Print: ";
    }
}

void
MiniComputer::printPrintMenuItem(int index)
{
    std::cout << '\t' << index << " - ";
    switch (index) {
    case 0: std::cout << "register"; break;
    case 1: std::cout << "string"; break;
    default: assert(0 && "Wrong print menu items");
    }
    std::cout << std::endl;
}

int
MiniComputer::getCommandFromUser()
{
    int command;
    std::cin >> command;
    return command;
}

void
MiniComputer::executeCommand(int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executeStoreCommand(); break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    default: assert(0 && "Invalid command");
    }
}

void
MiniComputer::validateCommand(int command)
{
    if (command < 0) {
        std::cerr << "Error 1: Invalid command" << std::endl;
        ::exit(1);
    }
    if (command > 4) {
        std::cerr << "Error 1: Invalid command" << std::endl;
        ::exit(1);
    }
}

void
MiniComputer::validateLoad(int load)
{
    if (load < 0) {
        std::cerr << "Error 2: Invalid command" << std::endl;
        ::exit(2);
    }
    if (load > 3) {
        std::cerr << "Error 2: Invalid command" << std::endl;
        ::exit(2);
    }
}

void
MiniComputer::validatePrint(int print)
{
    if (print < 0) {
        std::cerr << "Error 3: Invalid command" << std::endl;
        ::exit(3);
    }
    if (print > 1) {
        std::cerr << "Error 3: Invalid command" << std::endl;
        ::exit(3);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
    ::exit(0);
}

void
MiniComputer::executeLoadCommand()
{
    printLoadMenu();
    int load = getCommandFromUser();
    validateLoad(load);
    executeLoadIntoRegister(load);
}

void
MiniComputer::executePrintCommand()
{
    printPrintMenu();
    int print = getCommandFromUser();
    validatePrint(print);
    (0 == print) ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executeStoreCommand()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::executeAddCommand()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::printPrintRegisterMenu()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Choose register\n";
        for (int i = 0; i <= 3; ++i) {
            printPrintRegisterMenuItem(i);
        }
        std::cout << "> Register: ";
    }
}

void
MiniComputer::printPrintRegisterMenuItem(int index)
{
    std::cout << '\t' << index << " - " << getRegisterName(index) << std::endl;
}


void
MiniComputer::executePrintRegister()
{
    printPrintRegisterMenu();
    int chooseRegister = getCommandFromUser();
    validateLoad(chooseRegister);
    std::cout << getRegisterName(chooseRegister) << " = " << getRegisterValue(chooseRegister) << std::endl;
}

void
MiniComputer::executePrintString()
{
    /// Not implemented yet
    ::abort();
}

