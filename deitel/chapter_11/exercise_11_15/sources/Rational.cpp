#include "headers/Rational.hpp"
#include <iostream>

Rational::Rational(const int numerator, const int denominator)
{
    setNumber(numerator, denominator);
}

void
Rational::setNumber(const int numerator, const int denominator)
{
    const int nod = gcd(numerator, denominator);
    numerator_ = numerator / nod;
    denominator_ = (denominator > 0) ? denominator / nod : 1;
}

int
Rational::gcd(int number1, int number2) const
{
    if (0 == number2) {
        return number1;
    }
    return gcd(number2, number1 % number2);
}

Rational
Rational::operator+(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.denominator_ + rhv.numerator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::operator-(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.denominator_ - rhv.numerator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::operator*(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.numerator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::operator/(const Rational& rhv) const
{
    const int numerator = numerator_ * rhv.denominator_;
    const int denominator = denominator_ * rhv.numerator_;
    const Rational result(numerator, denominator);
    return result;
}

bool
Rational::operator>(const Rational& rhv) const
{
    return numerator_ * rhv.denominator_ > rhv.numerator_ * denominator_;
}

bool
Rational::operator<(const Rational& rhv) const
{
    return numerator_ * rhv.denominator_ < rhv.numerator_ * denominator_;\
}

bool
Rational::operator==(const Rational& rhv) const
{
    return numerator_ * rhv.denominator_ == rhv.numerator_ * denominator_;
}

bool
Rational::operator!=(const Rational& rhv) const
{
    return numerator_ * rhv.denominator_ != rhv.numerator_ * denominator_;
}

void
Rational::printResult() const
{
    std::cout << numerator_ << '/' << denominator_ << std::endl;
}

