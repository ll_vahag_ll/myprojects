#include "headers/Package.hpp"

Package::Package(const std::string& name, const std::string& city,
                 const std::string& state, const std::string& sender,
                 const std::string& recipient, const int& zip,
                 const std::string& address, const double& weight, const double& cost)
    : name_(name)
    , city_(city)
    , state_(state)
    , sender_(sender)
    , recipient_(recipient)
    , zip_(zip)
    , address_(address)
    , weight_(weight)
    , cost_(cost)
{
}

double
Package::calculateCost() const
{
	return weight_ * cost_;
}

