#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input numbers (9999 to end): ";
    }

    int numberQuantity = 0;
    int sum = 0;
    while (true) {
        int number1;
        std::cin >> number1;
        if (9999 == number1) {
            break;
        }
        sum += number1;
        ++numberQuantity;
    }
    if (0 == numberQuantity) {
        std::cout << "Error 1: No number to calculate middle" << std::endl;
        return 1;
    }
    std::cout << "Middle is " << sum / numberQuantity << std::endl;
    return 0;
}

