#include <iostream>
#include <unistd.h>

int mystery(int a, int b);

int
main()
{
    int x, y;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter two integers: ";
    }
    std::cin >> x >> y;
    std::cout << "The result is " << mystery(x, y) << std::endl;
    return 0;
}

int
mystery(int a, int b)
{
    if (1 == b) {
        return a;
    }

    if (b < 0) {
        return a + mystery(-a, -b + 1);
    }
    return a + mystery(a, b - 1);
}

