#include <iostream>

int
main()
{
    std::cout << "Input first side of a right triangle: ";
    int side1;
    std::cin >> side1;
    std::cout << "Input second side of a right triangle: ";
    int side2;
    std::cin >> side2;
    std::cout << "Input third side of a right triangle: ";
    int side3;
    std::cin >> side3;

    if (side1 <= 0) {
        std::cout << "Error 1: Inavalid side" << std::endl;
        return 1;
    }

    if (side2 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    if (side3 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    int flank1 = side1 * side1;
    int flank2 = side2 * side2;
    int flank3 = side3 * side3;

    if (flank1 == flank2 + flank3) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }

    if (flank2 == flank1 + flank3) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }

    if (flank3 == flank1 + flank2) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }
    std::cout << "they could not be the sides of a right triangle." << std::endl;

    return 0;
}

