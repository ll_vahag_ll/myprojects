#include "GradeBook.hpp"

#include <iostream>

GradeBook::GradeBook(std::string courseName, std::string instructorName)
{
    setCourseName(courseName);
    setInstructorName(instructorName);
}

void
GradeBook::setCourseName(std::string courseName)
{
     courseName_ = courseName;
}

std::string
GradeBook::getCourseName()
{
     return courseName_;
}

void
GradeBook::setInstructorName(std::string instructorName)
{
    instructorName_ = instructorName;
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::displayMessage()
{
     std::cout << "Welcome to the GradeBook for " << getCourseName() << "!" << std::endl;
     std::cout << "This course was presented by " << getInstructorName() << "!" << std::endl;
}

