#include "headers/SavingsAccount.hpp"
#include <iostream>

double SavingsAccount::annualInterestRate_ = 3.0;
 
SavingsAccount::SavingsAccount(const double balance)
{
    savingsBalance_ = (balance >= 0) ? balance : 0;
}
 
double
SavingsAccount::calculateMonthlyInterest() const
{
    const double interest = savingsBalance_ * annualInterestRate_ / 12;
    return interest;
}

void
SavingsAccount::increaseBalance()
{
    savingsBalance_ += calculateMonthlyInterest();
}
 
void
SavingsAccount::modifyInterestRate(const double rate)
{
    annualInterestRate_ = rate;
}

void
SavingsAccount::printResult() const
{
    std::cout << "Account balance: " << savingsBalance_ << std::endl;
}

