#ifndef __FUNCTIONS_HPP__
#define __FUNCTIONS_HPP__

#include "headers/Functors.hpp"

#include <iostream>
#include <utility>
#include <cassert>

namespace cd06 {

template <typename InputIterator, typename UnaryFunction>  
UnaryFunction for_each(InputIterator first, InputIterator last, UnaryFunction f);

template <typename InputIterator, typename UnaryPredicate>  
InputIterator find_if(InputIterator first, InputIterator last, UnaryPredicate f);

template <typename ForwardIterator, typename BinaryPredicate>  
ForwardIterator adjacent_find(ForwardIterator first, ForwardIterator last, BinaryPredicate f);

template <typename InputIterator, typename ForwardIterator, typename BinaryPredicate>  
InputIterator find_first_of(InputIterator first1, InputIterator last1, ForwardIterator first2, ForwardIterator last2, BinaryPredicate f);

template <typename InputIterator, typename UnaryPredicate, typename Size>  
void count_if(InputIterator first, InputIterator last, UnaryPredicate f, Size& n);

template <typename InputIterator1, typename InputIterator2, typename BinaryPredicate>  
std::pair<InputIterator1, InputIterator2> mismatch(InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, BinaryPredicate f);

template <typename InputIterator1, typename InputIterator2, typename BinaryPredicate>
bool equal(InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, BinaryPredicate f);

template <typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate>  
ForwardIterator1 search(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2, BinaryPredicate f);

template <typename ForwardIterator, typename Integer, typename T, typename BinaryPredicate>  
ForwardIterator search_n(ForwardIterator first, ForwardIterator last, Integer count, const T& value, BinaryPredicate f);

template <typename ForwardIterator1, typename ForwardIterator2, typename BinaryPredicate>  
ForwardIterator1 find_end(ForwardIterator1 first1, ForwardIterator1 last1, ForwardIterator2 first2, ForwardIterator2 last2, BinaryPredicate f);

template <typename InputIterator1, typename InputIterator2, typename OutputIterator, typename BinaryFunction>
OutputIterator transform(InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, OutputIterator result, BinaryFunction f);

template<typename ForwardIterator, typename UnaryPredicate, class T>
void replace_if(ForwardIterator first, ForwardIterator last, UnaryPredicate p, const T& new_value);

template<typename InputIterator, typename OutputIterator, typename UnaryPredicate, typename T>
OutputIterator replace_copy_if(InputIterator first, InputIterator last, OutputIterator d_first, UnaryPredicate p, const T& new_value);

template<typename ForwardIterator, typename Generator>
void generate(ForwardIterator first, ForwardIterator last, Generator g);

template<typename OutputIterator, typename size_type, typename Generator>
OutputIterator generate_n(OutputIterator first, size_type count, Generator g);

template<typename ForwardIterator, typename UnaryPredicate>
ForwardIterator remove_if(ForwardIterator first, ForwardIterator last, UnaryPredicate p);

template<typename InputIterator, typename OutputIterator, typename UnaryPredicate>
OutputIterator remove_copy_if(InputIterator first1, InputIterator last1, OutputIterator first2, UnaryPredicate p);
}

#include "sources/Functions.cpp"

#endif ///__FUNCTIONS_HPP__

