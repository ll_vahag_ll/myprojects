#include <iostream>
#include <vector>
#include <list>
#include <cstdlib>

namespace CD06 {

template <typename ForwardIterator, typename T>
void
fill(ForwardIterator first, ForwardIterator last, const T& v)
{
    for ( ; first != last; ++first) {
        *first = v;
    }
}

template <typename ForwardIterator, typename T>
void
iota(ForwardIterator first, ForwardIterator last, T value)
{
    for ( ; first != last; ++first, ++value) {
        *first = value;
    }
}

template <typename ForwardIterator, typename Generator>
void
generate(ForwardIterator first, ForwardIterator last, Generator gen)
{
    for ( ; first != last; ++first) {
        *first = gen();
    }
}

template <typename InputIterator, typename T>
T
accumulate(InputIterator first, InputIterator last, T init)
{
    for ( ; first != last; ++first) {
        init += *first;
    }
    return init;
}

template <typename InputIterator, typename EqualityComparable, typename Size>
void
count(InputIterator first, InputIterator last,  const EqualityComparable& value, Size& n)
{
    for ( ; first != last; ++first) {
        if (*first == value) {
            ++n;
        }
    }
}

template <typename InputIterator, typename OutputIterator>
OutputIterator
copy(InputIterator first, InputIterator last, OutputIterator result)
{
    for ( ; first != last; ++first, ++result) {
        *result = *first;
    }
    return result;
}

}

template <typename T>
void
printVector(const std::vector<T>& rhv)
{
    typename std::vector<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
printList(const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

int
main()
{
    const size_t size = 10;

    std::vector<int> v1(size);
    std::cout << "Vector before fill: ";
    printVector(v1);
    CD06::fill(v1.begin(), v1.end(), 7);
    std::cout << "Vector after fill: ";
    printVector(v1);

    std::vector<int> v2(size);
    std::cout << "Vector before iota: ";
    printVector(v2);
    CD06::iota(v2.begin(), v2.end(), 7);
    std::cout << "Vector after iota: ";
    printVector(v2);

    std::vector<int> v3(size);
    std::cout << "Vector before generate: ";
    printVector(v3);
    CD06::generate(v3.begin(), v3.end(), std::rand);
    std::cout << "Vector after generate: ";
    printVector(v3);

    std::vector<int> v4(size, 7);
    std::cout << "Vector elements: ";
    printVector(v4);
    std::cout << "Sum of Vector elements: ";
    std::cout << CD06::accumulate(v4.begin(), v4.end(), 0) << std::endl;

    std::vector<int> v5(size, 7);
    std::cout << "Vector elements: ";
    printVector(v5);
    int count = 0;
    CD06::count(v5.begin(), v5.end(), 7, count);
    std::cout << "Count of number 7 in vector: " << count << std::endl;

    std::vector<int> v6;
    for (size_t i = 0; i != size; ++i) {
        v6.push_back(i * i);
    }

    std::cout << "List before copy: ";
    std::list<int> l1;
    printList(l1);

    std::cout << "List after copy: ";
    copy(v6.begin(), v6.end(), std::back_inserter(l1));
    printList(l1);

    return 0;
}

