#include <iostream>
#include <unistd.h>
#include <cctype>
#include <cassert>

void printSquare(const int side1, const char character);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input length: ";
    }

    int side1;
    std::cin >> side1;

    if (side1 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input character: ";
    }
    char character;
    std::cin >> character;

    if (std::isprint(character) == 0) {
        std::cout << "Error 2: Character is not printable";
        return 2;
    }

    printSquare(side1, character);
    return 0;
}

void
printSquare(const int side1, const char character)
{
    assert(side1 > 0);
    assert(0 != std::isprint(character));
    for (int counter = 1; counter <= side1; ++counter) {
        for (int calculator = 1; calculator <= side1; ++calculator) {
            std::cout << character << ' ';
        }
        std::cout << std::endl;
    }
}

