#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time(const int hour = 0, const int minute = 0, const int second = 0);
    void setTime(const int hour, const int minute, const int second);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    void setErrorIndicator(const int errorIndicator);
    int getHour() const;
    int getMinute() const;
    int getSecond() const;
    int getErrorIndicator() const;
    void printUniversal() const;
    void printStandard() const;

private:
    int hour_;
    int minute_;
    int second_;
    int errorIndicator_; 
};

#endif ///__TIME_HPP__

