#include "headers/Complex.hpp"
#include <iostream>

Complex::Complex()
{
    realPart_ = 0;
    imaginaryPart_ = 0;
}

Complex::Complex(const double realPart, const double imaginaryPart)
{
    setComplex(realPart, imaginaryPart);
}

void
Complex::setComplex(const double realPart, const double imaginaryPart)
{
    realPart_ = realPart;
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::add(const Complex& rhv)
{
    const double realPart = realPart_ + rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ + rhv.imaginaryPart_;
    const Complex result(realPart, imaginaryPart);
    return result;
}

Complex
Complex::subtract(const Complex& rhv)
{
    const double realPart  = realPart_ - rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ - rhv.imaginaryPart_;
    const Complex result(realPart, imaginaryPart);
    return result;
}

void
Complex::print()
{
    std::cout << '(' << realPart_ << ", " << imaginaryPart_ << ')' << std::endl;
}

