#include <iostream>
#include <unistd.h>
#include <list>
#include <cstdlib>

void
generate(std::list<int>& l, const int SIZE)
{
    for (int i = 0; i < SIZE; ++i) {
        const int generate = std::rand() % 100;
        l.push_back(generate);
    }
}

void
print(std::list<int>& l)
{
    std::cout << "Integer list contains: ";
    for (std::list<int>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ", ";
    }
    std::cout << std::endl;
}

int
sum(const std::list<int>& l)
{
    int sum = 0;
    for (std::list<int>::const_iterator it = l.begin(); it != l.end(); ++it) {
        sum += *it;
    }
    return sum;
}

int
average(const std::list<int>& l, const int SIZE)
{
    return sum(l) / SIZE;    
}

int
main()
{
    const int SIZE = 25;
    std::list<int> lst1;
    generate(lst1, SIZE);
    print(lst1);

    std::cout << "Sum is " << sum(lst1) << std::endl;
    std::cout << "Average is " << average(lst1, SIZE) << std::endl;
    return 0;
}

