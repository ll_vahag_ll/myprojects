#include <iostream>
#include <unistd.h>

template <typename T>
T
minimum(const T& number1, const T& number2)
{
    return (number1 > number2) ? number2 : number1;
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two numbers: ";
    }
    int number1;
    int number2;
    std::cin >> number1 >> number2;
    std::cout << "Minimum is " << minimum(number1, number2) << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two characters: ";
    }
    char number3;
    char number4;
    std::cin >> number3 >> number4;
    std::cout << "Minimum is " << minimum(number3, number4) << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input tw floating-point numbers: ";
    }
    double number5;
    double number6;
    std::cin >> number5 >> number6;
    std::cout << "Minimum is " << minimum(number5, number6) << std::endl;

    return 0;
}

