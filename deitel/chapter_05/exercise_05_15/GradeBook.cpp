#include "GradeBook.hpp"
#include <iostream>
#include <cstdio>
#include <cstdlib>

GradeBook::GradeBook(std::string name)
{
    setCourseName(name);
    aCount_ = 0;
    bCount_ = 0;
    cCount_ = 0;
    dCount_ = 0;
    fCount_ = 0; 
}

void
GradeBook::setCourseName(std::string name)
{
    if (name.length() <= 25) {
        courseName = name;
        return;
    } 
    courseName = name.substr(0, 25);
    std::cout << "Name \"" << name << "\" exceeds maximum length (25).\n"
              << "Limiting courseName to first 25 characters.\n" << std::endl;
}

std::string
GradeBook::getCourseName()
{
    return courseName;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!\n" << std::endl;
}

void
GradeBook::inputGrades()
{
    int grade;
    std::cout << "Enter the letter grades." << std::endl;
    std::cout << "Enter the EOF character to end input." << std::endl;
    while ((grade = std::cin.get()) != EOF) {
        switch (grade) {
        case 'A':  case 'a': ++aCount_; break;
        case 'B':  case 'b': ++bCount_; break;
        case 'C':  case 'c': ++cCount_; break;
        case 'D':  case 'd': ++dCount_; break; 
        case 'F':  case 'f': ++fCount_; break;
        case '\n': case '\t': case ' ': break;
        default: std::cout << "Incorrect letter grade entered."
                           << " Enter a new grade." << std::endl; break; 
        } 
    } 
}
void
GradeBook::calculateAverage()
{
    int allGrades = 4 * aCount_ + 3 * bCount_ + 2 * cCount_ + 1 * dCount_;
    int gradeCount = aCount_ + bCount_ + cCount_ + dCount_ + fCount_;
    if (0 == gradeCount) {
        std::cout << "Error 1: Invalid average" << std::endl;
        ::exit (1);
    }
    totalAverage_ = allGrades / gradeCount;
}



void
GradeBook::displayGradeReport()
{
    std::cout << "\n\nNumber of students who received each letter grade:"
              << "\nA: " << aCount_
              << "\nB: " << bCount_
              << "\nC: " << cCount_
              << "\nD: " << dCount_
              << "\nF: " << fCount_
              << "\nTotal average: " << totalAverage_ << std::endl;
}

