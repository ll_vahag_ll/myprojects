#ifndef __T_SINGLE_LIST_HPP__
#define __T_SINGLE_LIST_HPP__

#include <iostream>

namespace cd06 {

template <typename T> class SingleList;

template <typename T>
std::istream& operator>>(std::istream& in, SingleList<T>& v);

template <typename T>
std::ostream& operator<<(std::ostream& out, const SingleList<T>& v);

template <typename T>
class SingleList
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

private:
    struct Node {
        Node() {};
        Node(const_reference d, Node* n = NULL)
            : data_(d), next_(n) {};
        value_type data_;
        Node* next_;
    };

public:
    class const_iterator {
        friend SingleList<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        
        const const_iterator& operator++();
        const const_iterator operator++(value_type);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        operator bool() const;
    private:
        Node* next() const;
    private:
        explicit const_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

public:
    class iterator : public const_iterator {
        friend SingleList<value_type>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        value_type& operator*();
        value_type* operator->();
        const iterator& operator=(const iterator& rhv);
    private:
        explicit iterator(Node* ptr);
    };

public:
    SingleList();
    SingleList(const size_type size);
    SingleList(const int size, const_reference init);
    SingleList(const SingleList<value_type>& rhv);
    template <typename InputIterator>
    SingleList(InputIterator f, InputIterator l);
    ~SingleList();
    
    void swap(SingleList<value_type>& rhv);
    const_reference get(const size_type index) const;

    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void resize(const size_type size, const_reference init = value_type());
    void clear();
    size_type capacity() const;

    void push_front(const_reference element);
    void pop_front();
    void push_back(const_reference element);
    const_reference front() const;
    reference front();
    const_reference back() const;
    reference back();

    const SingleList<value_type>& operator=(const SingleList<value_type>& rhv);
    bool operator==(const SingleList<value_type>& rhv) const;
    bool operator!=(const SingleList<value_type>& rhv) const;
    bool operator<(const SingleList<value_type>& rhv) const;
    bool operator<=(const SingleList<value_type>& rhv) const;
    bool operator>(const SingleList<value_type>& rhv) const;
    bool operator>=(const SingleList<value_type>& rhv) const;

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();

public:
    iterator insert_after(iterator pos, const value_type& x);
    void insert_after(iterator pos, const int n, const T& x);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);

    iterator insert(iterator pos, const value_type& x);
    void insert(iterator pos, const int n, const T& x);
    template <typename InputIterator>
    void insert(iterator pos, InputIterator f, InputIterator l);
    
    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);

private:
    Node* begin_;
    Node* end_;
};

}

#include "sources/SingleList.cpp"

#endif /// __T_SINGLE_LIST_HPP__

