#include "headers/Complex.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input first comlex real and imaginary parts: ";
    }
    double realPart1, imaginaryPart1;
    std::cin >> realPart1 >> imaginaryPart1;
    Complex complex1;
    complex1.setComplex(realPart1, imaginaryPart1);

    if (isInteractive) {
        std::cout << "Input second comlex real and imaginary parts: ";
    }
    double realPart2, imaginaryPart2;
    std::cin >> realPart2 >> imaginaryPart2;
    Complex complex2;
    complex2.setComplex(realPart2, imaginaryPart2);

    std::cout << "Adding Complex numbers: ";
    Complex complex3 = complex1.add(complex2);
    complex3.print();

    std::cout << "Subtracting Complex numbers: ";
    Complex complex4 = complex1.subtract(complex2);
    complex4.print();
    return 0;
}
