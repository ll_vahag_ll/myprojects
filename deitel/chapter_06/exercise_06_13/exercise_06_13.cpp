#include <iostream>
#include <unistd.h>
#include <cmath>

double roundToInteger(const double number1);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    double number1;
    std::cin >> number1;

    std::cout << "Original: " << number1 << std::endl;
    std::cout << "Rounded: " << roundToInteger(number1) << std::endl;

    return 0;
}

double
roundToInteger(const double number1)
{
    return std::floor(number1 + 0.5);
}

