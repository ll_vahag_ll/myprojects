#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational(const int numerator = 0, const int denominator = 1);
    void setNumber(const int numerator, const int denominator);
    int gcd(int number1, int number2);
    Rational add(const Rational& rhv);
    Rational substract(const Rational& rhv);
    Rational multiplicate(const Rational& rhv);
    Rational divide(const Rational& rhv);
    void printResult();

private:
    int numerator_;
    int denominator_;
};

#endif ///__RATIONAL_HPP__
