#include <string> 

class GradeBook
{
public:
    GradeBook(std::string); 
    void setCourseName(std::string);
    std::string getCourseName(); 
    void displayMessage(); 
    void inputGrades(); 
    void calculateAverage();
    void displayGradeReport();
private:
    std::string courseName; 
    int aCount_; 
    int bCount_;
    int cCount_;
    int dCount_;
    int fCount_;
    int totalAverage_;
};

