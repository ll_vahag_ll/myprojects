#include <iostream>
#include <cmath>

void testFunctions();

int
main()
{
    testFunctions();
    return 0;
}

void
testFunctions()
{
    std::cout << std::ceil(7.8) << std::endl;
    std::cout << std::cos(0) << std::endl;
    std::cout << std::fabs(-7.56) << std::endl;
    std::cout << std::floor(-7.3) << std::endl;
    std::cout << std::sqrt(25) << std::endl;
    std::cout << std::pow(5, 3) << std::endl;
}

