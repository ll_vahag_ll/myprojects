#include <string>

class GradeBook
{
public:
    GradeBook(std::string courseName, std::string instructorName);
    void setCourseName(std::string courseName);
    std::string getCourseName();
    void displayMessage();
    void setInstructorName(std::string instructorName);
    std::string getInstructorName();

private:
    std::string courseName_;
    std::string instructorName_;
};

