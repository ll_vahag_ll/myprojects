#include <iostream>

int
main()
{
    std::cout << "Input a binary integer: ";
    int number1;
    std::cin >> number1;

    int decimalNumber = 0;
    int system = 1;

    while (number1 > 0) {
        int binary = number1 % 10;
        if (binary > 1) {
            std::cout << "Error 1: Invalid binary number" << std::endl;
            return 1;
        }

        number1 /= 10;
        decimalNumber += binary * system;
        system *= 2;
    }

    std::cout << "Decimal is: " << decimalNumber << std::endl;
    return 0;
}

