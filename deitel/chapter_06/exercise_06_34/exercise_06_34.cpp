#include <iostream>
#include <cstdlib>

bool coinSimulate();

int
main()
{
    int heads = 0;
    int tails = 0;
    for (int counter = 1; counter <= 100; ++counter) {
        if (coinSimulate()) {
            std::cout << "Head" << std::endl;
            ++heads;
        } else {
            std::cout << "Tail" << std::endl;
            ++tails;
        }
    }
    std::cout << heads << " heads" << std::endl;
    std::cout << tails << " tails" << std::endl;
    return 0;
}

bool
coinSimulate()
{
    return 0 == std::rand() % 2;
}

