#include <iostream>
#include <unistd.h>

int gcd(int number1, int number2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two numbers: ";
    }
    int number1;
    int number2;
    std::cin >> number1 >> number2;

    std::cout << "Greatest common divisor of two integers is " << gcd(number1, number2) << std::endl;
    return 0;
}

int
gcd(int number1, int number2)
{
    while(number1 != number2) {
        if(number1 > number2) {
            number1 -= number2;
        } else {
            number2 -= number1;
        }
    }
    return number1;
}

