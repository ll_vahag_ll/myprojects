#include <iostream>
#include <unistd.h>
#include <cstdlib>

int randomNumber();
void randomAnswerForRightAnswer();
void randomAnswerForWrongAnswer();
void checkTheEquation();
bool inputAnswer(const int firstNumber, const int secondNumber);

int
main()
{
    checkTheEquation();
    return 0;
}

int
randomNumber()
{
    return (1 + std::rand() % 9);
}

void
randomAnswerForRightAnswer()
{
    switch(std::rand() % 4) {
    case 1: std::cout << "Very good!" << std::endl; break;
    case 2: std::cout << "Excellent!" << std::endl; break;
    case 3: std::cout << "Nice work!" << std::endl; break;
    case 4: std::cout << "Keep up the good work!" << std::endl; break;
    }
}

void
randomAnswerForWrongAnswer()
{
    switch(std::rand() % 4) {
    case 1: std::cout << "No. Please try again." << std::endl; break;
    case 2: std::cout << "Wrong. Try once more." << std::endl; break;
    case 3: std::cout << "Don't give up!" << std::endl;        break;
    case 4: std::cout << "No. Keep trying." << std::endl;      break;
    }
}

void checkTheEquation()
{
    const int firstNumber = randomNumber();
    const int secondNumber = randomNumber();

    if (::isatty(STDIN_FILENO)) {
        std::cout << "How much is " << firstNumber << " * " << secondNumber << "? "<< std::endl;
    }

    while (true) {
        if (inputAnswer(firstNumber, secondNumber)) {
            randomAnswerForRightAnswer();
            return;
        }
        randomAnswerForWrongAnswer();
    }
}

bool
inputAnswer(const int firstNumber, const int secondNumber)
{
    int multiplication;
    std::cin >> multiplication;
    return multiplication == firstNumber * secondNumber;
}

