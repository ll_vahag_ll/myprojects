#include <iostream>

int
main()
{
    int row = 1;
    while (row <= 8) {
        if (0 == row % 2) {
            std::cout << ' ';
        }

        int counter = 1;
        while (counter <= 8) {
            ++counter;
            std::cout << "* ";
        } 
        std::cout << std::endl;
        ++row;
    }
    return 0;
}
