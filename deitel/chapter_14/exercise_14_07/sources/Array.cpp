#include "headers/Array.hpp"
#include <iostream>
#include <iomanip>
#include <cstdlib>

template <typename elementType, size_t numberOfElements>
std::istream&
operator>>(std::istream& input, Array<elementType, numberOfElements>& rhv)
{
   for (size_t i = 0; i < numberOfElements; ++i)
      input >> rhv[i];

   return input;
}

template <typename elementType, size_t numberOfElements>
std::ostream&
operator<<(std::ostream& output, const Array<elementType, numberOfElements>& rhv)
{
   size_t i;
   for (i = 0; i < numberOfElements; ++i) {
       output << std::setw(12) << rhv[i];
       if (0 == (i + 1) % 4)
           output << std::endl;
   }

   if (i % 4 != 0) {
       output << std::endl;
   }

   return output;
} 

template <typename elementType, size_t numberOfElements>
Array<elementType, numberOfElements>::Array(const elementType element)
{
    for (size_t i = 0; i < numberOfElements; ++i) {
        ptr_[i] = element;
    }
}

template <typename elementType, size_t numberOfElements>
Array<elementType, numberOfElements>::Array(const Array<elementType, numberOfElements>& arrayToCopy)
{
    for (size_t i = 0; i < numberOfElements; ++i) {
        ptr_[i] = arrayToCopy.ptr_[i];
    }
}

template <typename elementType, size_t numberOfElements>
size_t
Array<elementType, numberOfElements>::getSize() const
{
    return numberOfElements;
}

template <typename elementType, size_t numberOfElements>
const Array<elementType, numberOfElements>&
Array<elementType, numberOfElements>::operator=(const Array<elementType, numberOfElements>& rhv)
{
    if (&rhv != this) {
        for (size_t i = 0; i < numberOfElements; ++i) {
            ptr_[i] = rhv.ptr_[i];
        }
    }
    return *this;
}

template <typename elementType, size_t numberOfElements>
bool
Array<elementType, numberOfElements>::operator==(const Array<elementType, numberOfElements>& rhv) const
{
    for (size_t i = 0; i < numberOfElements; ++i) {
        if (ptr_[i] != rhv.ptr_[i]) {
            return false;
        }
    }

    return true;
}

template <typename elementType, size_t numberOfElements>
bool
Array<elementType, numberOfElements>::operator!=(const Array<elementType, numberOfElements>& rhv) const
{
    return !(*this == rhv);
}

template <typename elementType, size_t numberOfElements>
elementType&
Array<elementType, numberOfElements>::operator[](size_t subscript)
{
   return ptr_[subscript];
}

template <typename elementType, size_t numberOfElements>
elementType
Array<elementType, numberOfElements>::operator[](size_t subscript) const
{
   return ptr_[subscript];
} 

