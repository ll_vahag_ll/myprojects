#include <iostream>
#include <stack>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void
input(std::stack<char>& s, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input chcharacters: ";
    }
    char temp;
    for (int i = 0; i < SIZE; ++i) {
        std::cin >> temp;
        s.push(temp);
    }
}

int
main()
{
    std::stack<char> stack1;
    const int SIZE = 10;
    input(stack1, SIZE);
    std::cout << "Chcharacter stack contains: ";

    while (!stack1.empty()) {
        std::cout << stack1.top();
        stack1.pop();
    }
    return 0;
}

