#include <iostream>

int
main()
{
    int number1, number2, number3;
    std::cout << "Input three different integers:";
    std::cin >> number1;
    std::cin >> number2;
    std::cin >> number3;

    int sum = number1 + number2 + number3;
    std::cout << "Sum is " << sum << std::endl;

    int average = sum / 3;
    std::cout << "Average is " << average << std::endl;

    int product = number1 * number2 * number3;
    std::cout << "Product is " << product << std::endl;

    int min = number1;

    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }
    std::cout << "Smallest is " << min << std::endl;

    int max = number1;

    if (number2 > max) {
        max = number2; 
    }
    if (number3 > max) {
        max = number3;
    }
    std::cout << "Largest is " << max << std::endl;

    return 0;
}

