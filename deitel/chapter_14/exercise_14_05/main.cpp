#include <iostream>
#include <iomanip>

template <typename T>
void
printArray(const T* array, const int count)
{
    for (int i = 0; i < count; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

void
printArray(const std::string* array, const int count)
{
     for (int i = 0; i < count; ++i) {
        if (0 == i % 3) {
            std::cout << std::endl;
        }
        std::cout << std::left << std::setw(10) << array[i];
    }
    std::cout << std::endl;
}

int
main()
{
    const int ACOUNT = 5;
    const int BCOUNT = 7;
    const int CCOUNT = 6;
    const int DCOUNT = 12;

    const int a[ACOUNT] = { 1, 2, 3, 4, 5 };
    const double b[BCOUNT] = { 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7 };
    const char c[CCOUNT] = "HELLO";
    const std::string d[DCOUNT] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve" };

    std::cout << "Array a contains:" << std::endl;
    printArray(a, ACOUNT);

    std::cout << "Array b contains:" << std::endl;
    printArray(b, BCOUNT);

    std::cout << "Array c contains:" << std::endl;
    printArray(c, CCOUNT);

    std::cout << "Array d contains:" << std::endl;
    printArray(d, DCOUNT);

    return 0;
}

