#ifndef __PHONENUMBER_HPP__
#define __PHONENUMBER_HPP__

#include <iostream>
#include <string>

class PhoneNumber
{
    friend std::istream& operator>>(std::istream& input, PhoneNumber& number);
public:
    std::ostream& operator<<(std::ostream& output);
    std::string getAreaCode() const;
    std::string getExchange() const;
    std::string getLine() const;
private:
    std::string areaCode_;
    std::string exchange_;
    std::string line_;
};
#endif /// __PHONENUMBER_HPP__
