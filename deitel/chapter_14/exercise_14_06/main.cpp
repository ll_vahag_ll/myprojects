#include "headers/Complex.hpp"
#include <iostream>
#include <unistd.h>

template <typename T>
bool isEqualTo(const T& number1, const T& number2)
{
    return number1 == number2;
}

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input two integers: ";
    }
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << number1 << " is" << (isEqualTo(number1, number2) ? " " : " not ") << "equal to " << number2 << std::endl;

    if (isInteractive) {
        std::cout << "Input two characters: ";
    }
    char number3, number4;
    std::cin >> number3 >> number4;
    std::cout << number3 << " is" << (isEqualTo(number3, number4) ? " " : " not ") << "equal to " << number4 << std::endl;

    if (isInteractive) {
        std::cout << "Input two complex numbers: ";
    }
    Complex complex1, complex2;
    std::cin >> complex1 >> complex2;
    std::cout << complex1 << " is" << (isEqualTo(complex1, complex2) ? " " : " not ") << "equal to " << complex2 << std::endl;
    return 0;
}

