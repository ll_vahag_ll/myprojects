#include "headers/OvernightPackage.hpp"
#include <iostream>

OvernightPackage::OvernightPackage(const std::string& name, const std::string& city,
                                   const std::string& state, const std::string& sender,
                                   const std::string& recipient, const int& zip,
                                   const std::string& address, const double& weight,
                                   const double& cost, const double& overnightCost)
    : Package(name, city, state, sender, recipient, zip, address, weight, cost)
    , overnightCost_(overnightCost)
{
}

double
OvernightPackage::calculateCost() const
{
    return Package::calculateCost() + (overnightCost_ * getWeight());
}
