#include <iostream>

int
main()
{
    int number1;
    std::cout << "Input integer: ";
    std::cin >> number1;

    if (number1 < 0) {
        std::cout << "Error 1: Invalid integer" << std::endl;
        return 1;
    }

    int factorial = 1;
    while (number1 > 1) {
        factorial *= number1;
        --number1;
    }
        std::cout << "Factorial: " << factorial << std::endl;
    return 0;
}

