#include <iostream>
#include <iomanip>

int
main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        double principal = 1000.0;
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::setw(28) << "Yearly rate percent: " << rate << '%' << std::endl;
        std::cout << std::fixed << std::setprecision(2);
        double percent = (1.0 + static_cast<double>(rate) / 100);
        for (int year = 1; year <= 10; ++year) {
            principal *= percent;
            std::cout << std::setw(4) << year << std::setw(21) << principal << std::endl;
        }
        std::cout << std::endl;
    }
    return 0; 
}

