#ifndef __OVERNIGHTPACKAGE_HPP__
#define __OVERNIGHTPACKAGE_HPP__

#include "Package.hpp"

class OvernightPackage: public Package
{
public:
    OvernightPackage(const std::string& name, const std::string& city,
                     const std::string& state, const std::string& sender,
                     const std::string& recipient, const int& zip,
                     const std::string& address, const double& weight,
                     const double& cost, const double& overnightCost);
    void setOvernightCost(const double& overnightCost) { overnightCost_ = overnightCost; }
    double calculateCost() const;
private:
    double overnightCost_;
};

#endif /// __OVERNIGHTPACKAGE_HPP__
