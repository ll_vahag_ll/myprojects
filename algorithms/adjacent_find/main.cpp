#include <iostream>
#include <unistd.h>
#include <vector>

const bool isInteractive = ::isatty(STDIN_FILENO);

namespace cd06 {

template <class ForwardIterator>
ForwardIterator
adjacent_find(ForwardIterator first, ForwardIterator last)
{
    ForwardIterator next = first++;
    for (; first != last; ++next, ++first) {
        if (*first == *next) {
            return first;
        }
    }
    return last;
}

template <typename T>
void
input(std::vector<T>& v, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (int i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

}

int
main()
{
    if (isInteractive) {
        std::cout << "Input vector size: ";
    }
    int SIZE;
    std::cin >> SIZE;

    std::vector<int> v;
    cd06::input(v, SIZE);

    std::vector<int>::iterator result = cd06::adjacent_find(v.begin(), v.end());

    if (result == v.end()) {
        std::cout << "No matching adjacent elements" << std::endl;
        return 0;
    }
    std::cout << "First adjacent pair of identical numbers: " << *result << ' ' << *result << std::endl;
    return 0;
}

