#include "headers/List.hpp"
#include <iostream>

template <typename NODETYPE>
List<NODETYPE>::List()
    : firstPtr_(NULL), lastPtr_(NULL)
{
}

template <typename NODETYPE>
List <NODETYPE>::~List()
{
    if (!isEmpty()) {
        ListNode<NODETYPE>* currentPtr = firstPtr_;
        ListNode<NODETYPE>* tempPtr;
        while (currentPtr != NULL) {
            tempPtr = currentPtr;
            currentPtr = currentPtr->nextPtr_;
            delete tempPtr;
        }
    }
}

template <typename NODETYPE>
void
List<NODETYPE>::insertAtFront(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr =  new ListNode<NODETYPE>(value);

    if (isEmpty()) {
        firstPtr_ = lastPtr_ = newPtr;
    } else {
        newPtr->nextPtr_ = firstPtr_;
        firstPtr_ = newPtr;
    }
}

template <typename NODETYPE>
void
List<NODETYPE>::insertAtBack(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr =  new ListNode<NODETYPE>(value);
    if (isEmpty()) {
        firstPtr_ = lastPtr_ = newPtr;
    } else {
        lastPtr_->nextPtr_ = newPtr;
        lastPtr_ = newPtr;
    }
}

template <typename NODETYPE>
void
List<NODETYPE>::insert(const size_t pos, const NODETYPE& value)
{
    if (0 == pos) {
        insertAtFront(value);
        return;
    }
    ListNode<NODETYPE>* temp = firstPtr_;
    for (size_t i = 1; i < pos; ++i) {
        temp = temp->nextPtr_;
    }

    ListNode<NODETYPE>* newPtr = new ListNode<NODETYPE>(value, temp->nextPtr_);
    temp->nextPtr_ = newPtr;
}

template <typename NODETYPE>
bool
List<NODETYPE>::removeFromFront(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    } else {
        ListNode<NODETYPE>* tempPtr = firstPtr_;

        if (firstPtr_ == lastPtr_) {
            firstPtr_ = lastPtr_ = NULL;
        } else {
            firstPtr_ = firstPtr_->nextPtr_;
        }

        value = tempPtr->data_;
        delete tempPtr;
        return true;
    }
}

template <typename NODETYPE>
bool
List<NODETYPE>::removeFromBack(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    } else {
        ListNode<NODETYPE> *tempPtr = lastPtr_;

        if (firstPtr_ == lastPtr_) {
            firstPtr_ = lastPtr_ = NULL;
        } else {
            ListNode<NODETYPE> *currentPtr = firstPtr_;

            while (currentPtr->nextPtr_ != lastPtr_) {
                currentPtr = currentPtr->nextPtr_;
            }

            lastPtr_ = currentPtr;
            currentPtr->nextPtr_ = 0;
        }

        value = tempPtr->data_;
        delete tempPtr;
        return true;
   }
}

template <typename NODETYPE>
void
List<NODETYPE>::erase(const size_t pos)
{
    if (lastPtr_ == firstPtr_) {
        delete firstPtr_;
        firstPtr_ = lastPtr_ = NULL;
        return;
    }

    ListNode<NODETYPE>* posPtr = firstPtr_;
    for (size_t i = 1; i < pos; ++i) {
        posPtr = posPtr->nextPtr_;
    }

    if (posPtr == firstPtr_) {
        firstPtr_ = firstPtr_->nextPtr_;
        delete posPtr;
        return;
    }

    ListNode<NODETYPE>* temp = posPtr->nextPtr_;
    posPtr->nextPtr_ = posPtr->nextPtr_->nextPtr_;
    delete temp;
}

template <typename NODETYPE>
bool
List<NODETYPE>::isEmpty() const
{
    return NULL == firstPtr_;
}

template <typename NODETYPE>
void
List<NODETYPE>::print() const
{
    if (isEmpty()) {
        std::cout << "The list is empty\n\n";
        return;
    }

    ListNode<NODETYPE>* currentPtr = firstPtr_;

    std::cout << "The list is: ";

    while (currentPtr != 0) {
        std::cout << currentPtr->data_ << ' ';
        currentPtr = currentPtr->nextPtr_;
    }

    std::cout << std::endl;
}

