#include <iostream>

int
main()
{
    int number1, number2;

    std::cout << "Enter two numbers ";
    std::cin >> number1;
    std::cin >> number2;

    if (0 == number2) {
        std::cout << "Error 1: division by 0" << std::endl;
        return 1;
    }
    
    if (0 == number1 % number2) {
        std::cout << "First is multiple of the second" << std::endl;
        return 0;
    }
    std::cout << "First is not multiple of the second" << std::endl;

    return 0;
}

