#include <iostream>
#include <vector>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

template <typename T>
T
recursiveLinearSearch(const std::vector<T> v, const T value, size_t i)
{
    if (i == v.size()) { return -1; }
    if (v[i] == value) { return i; }
    return recursiveLinearSearch(v, value, ++i);
}

template <typename T>
void
input(std::vector<T>& v, const size_t SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (size_t i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

int
main()
{
    const size_t SIZE = 10;
    std::vector<int> v;
    input(v, SIZE);

    if (isInteractive) {
        std::cout << "Input value to search: ";
    }
    int value;
    std::cin >> value;
    int index = recursiveLinearSearch(v, value, 0);

    if (-1 == index) {
        std::cout << "Value does not found" << std::endl;
        return 0;
    }

    std::cout << "Index of value: " << index << std::endl;
    return 0;
}

