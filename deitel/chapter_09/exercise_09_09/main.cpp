#include "headers/DateAndTime.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input time(hour, minute, second) and date(month, day, year): ";
    }
    int hour, minute, second, month, day, year;
    std::cin >> hour >> minute >> second >> month >> day >> year;

    DateAndTime dateAndTime(hour, minute, second, month, day, year);
    dateAndTime.tick();
    dateAndTime.printUniversal();
    std::cout << std::endl;
    dateAndTime.printStandard();
    return 0;
}

