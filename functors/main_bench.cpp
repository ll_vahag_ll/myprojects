#include "headers/Functions.hpp"
#include <benchmark/benchmark.h>

static const int ITER_COUNT = 2<<5;

static void
BM_for_each(benchmark::State& state)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(4);
    v.push_back(8);
    v.push_back(8);
    v.push_back(5);
    v.push_back(7);

    cd06::square<size_t> sqr;

    while (state.KeepRunning()) {
        cd06::for_each(v.begin(), v.end(), sqr);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}

static void
BM_find_if(benchmark::State& state)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(3);
    v.push_back(11);
    v.push_back(27);
    v.push_back(6);
    v.push_back(7);

    cd06::isEven<size_t> even;

    while (state.KeepRunning()) {
        cd06::find_if(v.begin(), v.end(), even);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}


static void
BM_adjacent_find(benchmark::State& state)
{
   std::vector<size_t> v;
    v.push_back(1);
    v.push_back(3);
    v.push_back(7);
    v.push_back(6);
    v.push_back(6);
    v.push_back(7);

    cd06::isEqual<size_t> eq;

    while (state.KeepRunning()) {
        cd06::adjacent_find(v.begin(), v.end(), eq);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}

static void
BM_find_first_of(benchmark::State& state)
{
    std::vector<size_t> v1;
    v1.push_back(1);
    v1.push_back(3);
    v1.push_back(7);
    v1.push_back(13);
    v1.push_back(6);
    v1.push_back(7);

    std::vector<size_t> v2;
    v2.push_back(2);
    v2.push_back(4);
    v2.push_back(9);
    v2.push_back(13);
    v2.push_back(27);
    v2.push_back(81);

    cd06::isEqual<size_t> eq;
 
    while (state.KeepRunning()) {
        cd06::find_first_of(v1.begin(), v1.end(), v2.begin(), v2.end(), eq);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}

static void
BM_count_if(benchmark::State& state)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(7);
    v.push_back(7);
    v.push_back(8);
    v.push_back(9);

    cd06::isEven<size_t> even;
    size_t sumOfEven = 0;

    while (state.KeepRunning()) {
        cd06::count_if(v.begin(), v.end(), even, sumOfEven);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}

static void
BM_mismatch(benchmark::State& state)
{
    std::vector<size_t> v;
    v.push_back(3);
    v.push_back(2);
    v.push_back(9);
    v.push_back(3);
    v.push_back(7);
    v.push_back(4);
    v.push_back(9);
    v.push_back(8);
    v.push_back(9);

    std::list<size_t> ls1;
    ls1.push_back(3);
    ls1.push_back(2);
    ls1.push_back(9);
    ls1.push_back(3);
    ls1.push_back(7);
    ls1.push_back(5);

    cd06::isEqual<size_t> equal;

    while (state.KeepRunning()) {
        cd06::mismatch(v.begin(), v.end(), ls1.begin(), equal);
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}


BENCHMARK(BM_for_each);
BENCHMARK(BM_find_if);
BENCHMARK(BM_adjacent_find);
BENCHMARK(BM_find_first_of);
BENCHMARK(BM_count_if);
BENCHMARK(BM_mismatch);

BENCHMARK_MAIN();

