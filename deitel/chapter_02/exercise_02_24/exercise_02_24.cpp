#include <iostream>

int
main()
{
    int number1;
    std::cout << "Enter integer: ";
    std::cin >> number1;

    if (number1 % 2 == 0) {
        std::cout << "Number is even" << std::endl;
        return 0;
    }

    std::cout << "Number is odd" << std::endl;

    return 0;
}

