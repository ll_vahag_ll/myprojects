#include <iostream>

int
main()
{
    int number1;
    std::cout << "Input numbers quantity: ";
    std::cin >> number1;

    int sum = 0;
    for (int counter = 1; counter <= number1; ++counter) {
        std::cout << "Number " << counter << ": " << std::endl;
        int variable;
        std::cin >> variable;
        sum += variable;
    }

    std::cout << "Sum is " << sum << std::endl;
    return 0;
}

