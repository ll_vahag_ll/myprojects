#ifndef __DateAndTime_HPP__
#define __DateAndTime_HPP__

class DateAndTime
{
private:
    static const int SIZE = 13;
    static const int days[SIZE];
public:
    DateAndTime(const int hour = 0, const int minute = 0, const int second = 0, const int month = 1, const int day = 1, const int year = 1);
    void setDateAndTime(const int hour, const int minute, const int second, const int month, const int day, const int year);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    void setMonth(const int month);
    void setDay(const int day);
    void setYear(const int year);

    int getHour();
    int getMinute();
    int getSecond();
    int getMonth();
    int getDay();
    int getYear();
    int getLastDay();

    bool isLeapYear();

    void nextDay();
    void tick();
    void printUniversal();
    void printStandard();

private:
    int hour_;
    int minute_;
    int second_;
    int month_;
    int day_;
    int year_;
};

#endif ///__DateAndTime_HPP__
