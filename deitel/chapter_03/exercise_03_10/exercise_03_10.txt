A class might provide a set function and get function for data member
because data members are in private area and other functions that are not in class can't use them.
