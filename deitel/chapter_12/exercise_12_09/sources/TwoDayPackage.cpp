#include "headers/TwoDayPackage.hpp"

TwoDayPackage::TwoDayPackage(const std::string& name, const std::string& city,
                             const std::string& state, const std::string& sender,
                             const std::string& recipient, const int& zip,
                             const std::string& address, const double& weight,
                             const double& cost, const double& flatFee)
    : Package(name, city, state, sender, recipient, zip, address, weight, cost)
    , flatFee_(flatFee)
{
}

double
TwoDayPackage::calculateCost() const
{
    return Package::calculateCost() + flatFee_;
}

