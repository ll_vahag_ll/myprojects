#include "GradeBook.hpp"

#include <iostream>

int
main()
{
    GradeBook gradeBook1("CS101 Introduction to C++ Programming", "Vardan Qerobyan");
    GradeBook gradeBook2("CS102 Data Structures in C++"," Maria Khachatryan");
    std::cout << "gradeBook1 created for: " << gradeBook1.getCourseName() << std::endl;
    std::cout << "gradeBook2 created for: " << gradeBook2.getCourseName() << std::endl;

    gradeBook1.displayMessage();
    gradeBook2.displayMessage();
    return 0;
}

