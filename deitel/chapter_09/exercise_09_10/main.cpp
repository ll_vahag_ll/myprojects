#include "headers/Time.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input time(hour, minute, second): ";
    }
    int hour, minute, second;
    std::cin >> hour >> minute >> second;

    Time time1(hour, minute, second);
    if (time1.getErrorIndicator() != 0) {
        std::cout << "Error " << time1.getErrorIndicator() << ": Invalid time" << std::endl;
        return time1.getErrorIndicator();
    }
    time1.printUniversal();
    time1.printStandard();
    return 0;
}


