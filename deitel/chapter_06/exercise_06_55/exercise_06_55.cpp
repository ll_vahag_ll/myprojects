#include <iostream>
#include <unistd.h>
#include <cassert>

inline double circleArea(const double radius);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input radius of a circle: ";
    }
    double radius;
    std::cin >> radius;

    if (radius <= 0) {
        std::cout << "Error 1: Invalid radius" << std::endl;
        return 1;
    }

    std::cout << "Area of circle is " << circleArea(radius) << std::endl;
    return 0;
}

inline
double
circleArea(const double radius)
{
    assert(radius > 0);
    return 3.14 * radius * radius;
}

