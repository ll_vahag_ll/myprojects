#ifndef __DATE_HPP__
#define __DATE_HPP__

class Date
{
private:
    static const int SIZE = 13;
    static const int days[SIZE];
public:
    Date(const int month = 1, const int day = 1, const int year = 2000);
    void setMonth(const int month);
    void setDay(const int day);
    void setYear(const int year);
    int getLastDay();
    bool isLeapYear();
    void print();
    void nextDay();
private:
    int month_;
    int day_;
    int year_;
};

#endif /// __DATE_HPP__

