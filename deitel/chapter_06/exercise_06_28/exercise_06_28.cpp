#include <iostream>
#include <unistd.h>
#include <iomanip>

double smallestNumber(const double number1, const double number2, const double number3);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first number: ";
    }
    double number1;
    std::cin >> number1;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second number: ";
    }
    double number2;
    std::cin >> number2;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input third number: ";
    }
    double number3;
    std::cin >> number3;

    std::cout << "Smallest is " << std::setprecision(2) << std::fixed << smallestNumber(number1, number2, number3) << std::endl;

    return 0;
}


double
smallestNumber(const double number1, const double number2, const double number3)
{
    double min = number1;

    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }

    return min;
}

