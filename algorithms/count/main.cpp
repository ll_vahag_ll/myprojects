#include <iostream>
#include <unistd.h>
#include <vector>

const bool isInteractive = ::isatty(STDIN_FILENO);

namespace cd06 {

template <class InputIterator, class EqualityComparable>
typename std::iterator_traits<InputIterator>::difference_type
count(InputIterator first, InputIterator last,
      const EqualityComparable& value)
{
    typename std::iterator_traits<InputIterator>::difference_type count = 0;
    for (; first != last; ++first) {
        if (*first == value) {
            ++count;
        }
    }
    return count;
}

template <typename T>
void
input(std::vector<T>& v, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (int i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

}

int
main()
{
    if (isInteractive) {
        std::cout << "Input vector size: ";
    }
    int SIZE;
    std::cin >> SIZE;

    std::vector<int> v1;
    cd06::input(v1, SIZE);

    if (isInteractive) {
        std::cout << "Input value: ";
    }
    int value;
    std::cin >> value;

    const int result = cd06::count(v1.begin(), v1.end(), value);

    std::cout << "Number: " << value << std::endl << "Count: " << result << std::endl;
    return 0;
}

