#include "headers/Rational.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    Rational rational1;
    if (isInteractive) {
        std::cout << "Input first number numerator: ";
    }
    int numerator;
    std::cin >> numerator;

    if (isInteractive) {
        std::cout << "Input first number denominator: ";
    }
    int denominator;
    std::cin >> denominator;
    if (0 == denominator) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }
    rational1.setNumber(numerator, denominator);

    Rational rational2;
    if (isInteractive) {
        std::cout << "Input second number numerator: ";
    }
    std::cin >> numerator;

    if (isInteractive) {
        std::cout << "Input second number denominator: ";
    }
    std::cin >> denominator;
    if (0 == denominator) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }
    rational2.setNumber(numerator, denominator);

    Rational rational3 = rational1 + rational2;
    std::cout << "The addion is ";
    rational3.printResult();

    Rational rational4 = rational1 - rational2;
    std::cout << "The substraction is ";
    rational4.printResult();

    Rational rational5 = rational1 * rational2;
    std::cout << "The multiplicateon is ";
    rational5.printResult();

    Rational rational6 = rational1 / rational2;
    std::cout << "The dividing is ";
    rational6.printResult();

    if (rational1 == rational2) {
        std::cout << "Rational1 is equal to Rational2" << std::endl;
        return 0;
    }

    if (rational1 != rational2) {
        std::cout << "Rational1 is not equal to Rational2" << std::endl;
    }

    if (rational1 > rational2) {
        std::cout << "Rational1 is great than Rational2" << std::endl;
        return 0;
    }

    if (rational1 < rational2) {
        std::cout << "Rational2 is greater than Rational1" << std::endl;
    }
    return 0;
}
