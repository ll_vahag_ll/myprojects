#ifndef __SAVINGSACCOUNT_HPP__
#define __SAVINGSACCOUNT_HPP__

#include "Account.hpp"

class SavingsAccount : public Account
{
public:
    SavingsAccount(const double balance, const double interestRate);
    void setInterestRate(const double interestRate);
    double calculateInterest() const;
    void increaseBalance();
    int getInterestRate() const;
private:
    double interestRate_;

};

#endif /// __SAVINGSACCOUNT_HPP__
