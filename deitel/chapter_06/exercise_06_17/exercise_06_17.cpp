#include <iostream>
#include <cstdlib>

int
main()
{
    const int random1 = 2 * (1 + std::rand() % 5);
    std::cout << random1 << std::endl;

    const int random2 = 1 + (2 + std::rand() % 10);
    std::cout << random2 << std::endl;

    const int random3 = 2 + (4 * (1 + std::rand() % 5));
    std::cout << random3 << std::endl;

    return 0;
}
