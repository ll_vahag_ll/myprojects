#include <iostream>
#include <vector>

namespace cd06 {
    
template <class InputIterator1, class InputIterator2>
bool
includes(InputIterator1 first1, InputIterator1 last1,
         InputIterator2 first2, InputIterator2 last2)
{
    while (first1 != last1 && first2 != last2) {
        if (*first1 == *first2) {
            ++first2;
        }
        ++first1;
    }
    return first2 == last2;
}

}

int
main()
{
    const std::vector<int> v1(7, 7);
    const std::vector<int> v2(3, 7);
    const std::vector<int> v3(3, 2);
    const std::vector<int> v4(8, 1);
    const std::vector<int> v5(4, 5);
    const std::vector<int> v6(4, 1);

    std::cout << "v2 contained in v1: " << (cd06::includes(v1.begin(), v1.end(), v2.begin(), v2.end()) ? "true" : "false") << std::endl;
    std::cout << "v3 contained in v1: " << (cd06::includes(v1.begin(), v1.end(), v3.begin(), v3.end()) ? "true" : "false") << std::endl;
    std::cout << "v5 contained in v4: " << (cd06::includes(v4.begin(), v4.end(), v5.begin(), v5.end()) ? "true" : "false") << std::endl;
    std::cout << "v6 contained in v4: " << (cd06::includes(v4.begin(), v4.end(), v6.begin(), v6.end()) ? "true" : "false") << std::endl;

    return 0;
}

