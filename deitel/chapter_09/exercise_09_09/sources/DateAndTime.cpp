#include "headers/DateAndTime.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>

const int DateAndTime::days[SIZE] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

DateAndTime::DateAndTime(const int hour, const int minute, const int second, const int month, const int day, const int year)
{
    setDateAndTime(hour, minute, second, month, day, year);
}

void
DateAndTime::setDateAndTime(const int hour, const int minute, const int second, const int month, const int day, const int year)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
DateAndTime::setHour(const int hour)
{
    if (hour < 0 || hour > 23) {
        std::cout << "Error 1: Invalid hour" << std::endl;
        ::exit(1);
    }
    hour_ = hour;
}

void
DateAndTime::setMinute(const int minute)
{
    if (minute < 0 || minute > 59) {
        std::cout << "Error 2: Invalid minute" << std::endl;
        ::exit(2);
    }
    minute_ = minute;
}

void
DateAndTime::setSecond(const int second)
{
    if (second < 0 || second > 59) {
        std::cout << "Error 3: Invalid second" << std::endl;
        ::exit(3);
    }
    second_ = second;
}

void
DateAndTime::setMonth(const int month)
{
    if (month < 1 || month > 12) {
        std::cout << "Error 1: Invalid month" << std::endl;
        ::exit(1);
    }
    month_ = month;
}

void
DateAndTime::setDay(const int day)
{
    if (day < 1 || day > getLastDay()) {
        std::cout << "Error 2: Invalid day" << std::endl;
        ::exit(2);
    }
    day_ = day;
}

void
DateAndTime::setYear(const int year)
{
    if (year < 0) {
        std::cout << "Error 3: Invalid year" << std::endl;
        ::exit(3);
    }
    year_ = year;
}

int
DateAndTime::getHour()
{
    return hour_;
}

int
DateAndTime::getMinute()
{
    return minute_;
}

int
DateAndTime::getSecond()
{
    return second_;
}

int
DateAndTime::getMonth()
{
    return month_;
}

int
DateAndTime::getDay()
{
    return day_;
}

int
DateAndTime::getYear()
{
    return year_;
}

bool
DateAndTime::isLeapYear()
{
    return year_ % 400 == 0 || (year_ % 100 != 0 && year_ % 4 == 0);
}

int
DateAndTime::getLastDay()
{
    return (2 == month_ && isLeapYear()) ? 29 : days[month_];
}

void
DateAndTime::nextDay()
{
    ++day_;
    if (day_ > getLastDay()) {
        ++month_;
        day_ = 1;
        if (13 == month_) {
            ++year_;
            month_ = 1;
        }
    }
}

void
DateAndTime::tick()
{
    ++second_;
    if (60  == second_) {
        second_ = 0;
        ++minute_;
        if (60 == minute_) {
            minute_ = 0;
            ++hour_;
            if (24 == hour_) {
                hour_ = 0;
                nextDay();
            }
        }
    }
}

void
DateAndTime::printUniversal()
{
    const char prev = std::cout.fill (' ');
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond() << std::endl;;
    std::cout.fill(prev);
    std::cout << getMonth() << '/' << getDay() << '/' << getYear() << std::endl;
}

void
DateAndTime::printStandard()
{
    const char prev = std::cout.fill (' ');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(prev);
    std::cout << getMonth() << '/' << getDay() << '/' << getYear() << std::endl;
}

