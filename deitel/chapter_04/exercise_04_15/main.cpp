#include <iostream>

#include <iomanip>

int
main()
{
    while (true) {
        std::cout << "Input sales in dollars (-1 to end): ";
        int salesBalance;
        std::cin >> salesBalance;
        if (-1 == salesBalance) {
            return 0;
        }

        if (0 > salesBalance) {
            std::cout << "Error 1: Invalid sales balance" << std::endl;
            return 1;
        }

        double sallary = 200 + static_cast<double>(salesBalance) * 0.09;
        std::cout << std::setprecision(2) << std::fixed;
        std::cout << "Salary is: " << sallary << "$" << std::endl;
    }

    return 0;
}

