#include <iostream>
#include <unistd.h>
#include <cstdlib>

int randomNumber();
void checkTheEquation();

int
main()
{
    checkTheEquation();
    return 0;
}

int
randomNumber()
{
    return (1 + std::rand() % 9);
}

void
checkTheEquation()
{
    const int number1 = randomNumber();
    const int number2 = randomNumber();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "How much is " << number1 << " * " << number2 << "? "<< std::endl;
    }

    while (true) {
        int multiplication;
        std::cin >> multiplication;
        if (multiplication == number1 * number2) {
            std::cout << "Very good" << std::endl;
            return;
        } else {
            std::cout << "No. Please try again" << std::endl;
        }
    }
}

