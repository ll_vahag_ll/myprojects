#include <iostream>
#include <list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void
input(std::list<char>& l, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input chcharacters: ";
    }
    char temp;
    for (int i = 0; i < SIZE; ++i) {
        std::cin >> temp;
        l.push_back(temp);
    }
}

void
concatenate(std::list<char>& list1, const std::list<char>& list2)
{
    std::list<char>::const_iterator it = list2.begin();
    for (; it != list2.end(); ++it) {
        list1.push_back(*it);
    }
}

void
print(const std::list<char>& l)
{
    for (std::list<char>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it;
    }
}

int
main()
{
    std::list<char> lst1;
    const int SIZE = 5;
    input(lst1, SIZE);
    std::cout << "First chcharacter list contains: ";
    print(lst1);
    std::cout << std::endl;

    std::list<char> lst2;
    input(lst2, SIZE);
    std::cout << "Second chcharacter list contains: ";
    print(lst2);
    std::cout << std::endl;

    std::cout << "Character list after concatenation: ";
    concatenate(lst1, lst2);
    print(lst1);
    return 0;
}

