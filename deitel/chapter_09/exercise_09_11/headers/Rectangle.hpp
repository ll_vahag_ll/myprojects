#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const double length = 1, const double width = 1);
    
    void setLength(const double length);
    void setWidth(const double width);
    int getLength();
    int getWidht();

    int calculatePerimeter();
    void printPerimeter();
    int calculateArea();
    void printArea();

private:
    double length_;
    double width_;
};

#endif /// __RECTANGLE_HPP__

