#include <iostream>
#include <vector>
#include <unistd.h>
#include <iterator>
#include <cassert>

const bool isInteractive = ::isatty(STDIN_FILENO);

namespace cd06 {

template <class InputIterator1, class InputIterator2, class OutputIterator>
OutputIterator
set_difference(InputIterator1 first1,
               InputIterator1  last1,
               InputIterator2 first2,
               InputIterator2  last2,
               OutputIterator result)
{
    assert(first1 != InputIterator1() && last1 != InputIterator1());
    assert(first2 != InputIterator2() && last2 != InputIterator2());

    while (first1 != last1 && first2 != last2) {
        if (*first1 < *first2) {
            *result = *first1;
            ++first1;
            continue;
        }
        if (*first2 < *first1) {
            ++first2;
            continue;
        }
        ++first1;
        ++first2;
    }
    return std::copy(first1, last1, result);
}

template <typename T>
void
input(std::vector<T>& v, const size_t SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (size_t i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

}

int
main()
{
    const size_t SIZE = 5;
    std::vector<int> v1;
    cd06::input(v1, SIZE);
    std::vector<int> v2;
    cd06::input(v2, SIZE);

    std::cout << "Difference of v1 and v2: ";
    cd06::set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
    return 0;
}

