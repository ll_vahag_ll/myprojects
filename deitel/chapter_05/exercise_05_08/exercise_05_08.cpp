#include <iostream>
#include <limits>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number quantity: ";
    }

    int number1;
    std::cin >> number1;

    int smallest = std::numeric_limits<int>::max();

    for (int counter = 1; counter <= number1; ++counter) {

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Number " << counter << ": ";
        }

        int variable;
        std::cin >> variable;
        if (variable < smallest) {
            smallest = variable;
        }
    }

    std::cout << "Smallest is " << smallest << std::endl;
    return 0;
}

