#include "headers/Package.hpp"
#include "headers/TwoDayPackage.hpp"
#include "headers/OvernightPackage.hpp"
#include <iostream>
#include <iomanip>

int
main()
{

    TwoDayPackage delivery1("Tom Brown", "Phoenix", "Arizona","Tom", "John", 89754, "123 Main Street", 3.00, 5.00, 4.00);
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "2 Day Delivery\n\n";
    std::cout << "Sender: " << delivery1.getName() << "\n";
    std::cout << '\t' << delivery1.getAddress() << "\n";
    std::cout << '\t' << delivery1.getCity() << " " 
                      << delivery1.getState() << " " 
                      << delivery1.getZip() << "\n";

    TwoDayPackage delivery2("John", "Hartford ", "Connecticut", "Tom", "John", 87540, "123 bent street", 3.00, 5.00, 4.00);
    std::cout << "\n";
    std::cout << "Recipient: " << delivery2.getName()<< "\n";
    std::cout << "\t   " << delivery2.getAddress() << "\n";
    std::cout << "\t   " << delivery2.getCity() << " " 
                      << delivery2.getState() << " " 
                      << delivery2.getZip() << "\n";
    std::cout << "Cost\t   $ " << delivery2.calculateCost() << "\n\n";

    std::cout << "_______________________________________\n\n";

    OvernightPackage delivery3("Tom Brown", "Phoenix", "Arizona","Tom", "John", 89754, "123 Main Street", 4.00, 7.00, 8.00);
    std::cout << "Overnight Delivery\n\n";
    std::cout << "Sender: " << delivery3.getName()<< "\n";
    std::cout << '\t' << delivery3.getAddress() << "\n";
    std::cout << '\t' << delivery3.getCity() << " "
                      << delivery3.getState() << " "
                      << delivery3.getZip() << "\n";

    OvernightPackage delivery4("John", "Hartford ", "Connecticut", "Tom", "John", 87540, "123 bent street", 4.00, 7.00, 8.00);
    std::cout << "\n";
    std::cout << "Recipient: " << delivery4.getName()<< "\n";
    std::cout << "\t   " << delivery4.getAddress() << "\n";
    std::cout << "\t   " << delivery4.getCity() << " "
                         << delivery4.getState() << " "
                         << delivery4.getZip() << "\n";
    std::cout << "Cost\t   $ " << delivery4.calculateCost() <<"\n";
	return 0;
}
