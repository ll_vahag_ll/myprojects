#include <iostream>
#include <unistd.h>
#include <cmath>

double distance(const double x1, const double y1, const double x2, const double y2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input firsts position: ";
    }
    double x1, y1;
    std::cin >> x1 >> y1;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seconds position: ";
    }
    double x2, y2;
    std::cin >> x2 >> y2;

    std::cout << "Distance is " << distance(x1, x2, y1, y2) << std::endl;
    return 0;
}

double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double alphaX = x1 - x2;
    const double deltaX = alphaX * alphaX;
    const double alphaY = y1 - y2;
    const double deltaY = alphaY * alphaY;
    return std::sqrt(deltaX + deltaY);
}

