progname=exercise_12_09
lib=lib$(progname).a
utest=utest_$(progname)
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03 -I.
BUILDS=builds
TEST_DIR=tests

ifeq ($(MAKECMDGOALS),)
    BUILD_DIR:=$(BUILDS)/debug
else
    BUILD_DIR:=$(BUILDS)/$(MAKECMDGOALS)
endif

SOURCES:=main.cpp $(wildcard sources/*.cpp)
DEPENDS:=$(patsubst %.cpp,$(BUILD_DIR)/%.d,$(SOURCES))
PREPROCS:=$(patsubst %.cpp,$(BUILD_DIR)/%.ii,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,$(BUILD_DIR)/%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(SOURCES))

LIB_SOURCES:=$(wildcard sources/*.cpp)
LIB_DEPENDS:=$(patsubst %.cpp,$(BUILD_DIR)/%.d,$(LIB_SOURCES))
LIB_PREPROCS:=$(patsubst %.cpp,$(BUILD_DIR)/%.ii,$(LIB_SOURCES))
LIB_ASSEMBLES:=$(patsubst %.cpp,$(BUILD_DIR)/%.s,$(LIB_SOURCES))
LIB_OBJS:=$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(LIB_SOURCES))

UTEST_SOURCES:=main_utest.cpp $(wildcard sources/*.cpp)
UTEST_DEPENDS:=$(patsubst %.cpp,$(BUILD_DIR)/%.d,$(UTEST_SOURCES))
UTEST_PREPROCS:=$(patsubst %.cpp,$(BUILD_DIR)/%.ii,$(UTEST_SOURCES))
UTEST_ASSEMBLES:=$(patsubst %.cpp,$(BUILD_DIR)/%.s,$(UTEST_SOURCES))
UTEST_OBJS:=$(patsubst %.cpp,$(BUILD_DIR)/%.o,$(UTEST_SOURCES))

TEST_INPUTS:=$(wildcard $(TEST_DIR)/test*.input)
TESTS:=$(patsubst $(TEST_DIR)/%.input,%,$(TEST_INPUTS))

debug:   $(BUILD_DIR) $(BUILD_DIR)/$(lib) test1
release: $(BUILD_DIR)

debug:   CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG

debug: qa
release: qa

qa: $(TESTS)

utest: $(BUILD_DIR)/$(utest)
	./$<

test%: $(BUILD_DIR)/$(progname)
	./$< < $(TEST_DIR)/$@.input > $(BUILD_DIR)/$(TEST_DIR)/$@.output || echo "Negative $@..."
	diff $(BUILD_DIR)/$(TEST_DIR)/$@.output $(TEST_DIR)/$@.expected && echo PASSED || echo FAILED

$(BUILD_DIR)/$(utest): $(UTEST_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -lgtest -lpthread -o $@

$(BUILD_DIR)/$(lib): $(LIB_OBJS) | .gitignore
	ar -crv $@ $^

$(BUILD_DIR)/$(progname): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@
	ln $(BUILD_DIR)/$(progname) $(progname)

$(BUILD_DIR)/%.ii: %.cpp
	$(CXX) $(CXXFLAGS) -E $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.cpp,$(BUILD_DIR)/%.d,$<)

$(BUILD_DIR)/%.s: $(BUILD_DIR)/%.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

$(BUILD_DIR)/%.o: $(BUILD_DIR)/%.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

.gitignore:
	echo $(progname) > .gitignore
	echo $(utest)   >> .gitignore

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)/sources $(BUILD_DIR)/$(TEST_DIR)

install:
	cp headers/*.hpp /usr/local/include
	cp builds/debug/$(lib) /usr/local/lib

clean:
	rm -rf $(BUILDS) .gitignore $(progname)

.PRECIOUS: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)

sinclude $(DEPENDS) $(UTEST_DEPENDS)

