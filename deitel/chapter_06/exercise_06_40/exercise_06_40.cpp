#include <iostream>
#include <unistd.h>
#include <cassert>

int integerPower(const int base, const int exponent, int result = 1);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input base: ";
    }
    int base;
    std::cin >> base;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input exponent: ";
    }
    int exponent;
    std::cin >> exponent;

    std::cout << base << "^" << exponent << " = " << integerPower(base, exponent) << std::endl;
    return 0;
}

int
integerPower(const int base, const int exponent, int result)
{
    if (0 == exponent) {
        return result;
    }
    if (1 == exponent % 2) {
        result *= base;
    }
    return integerPower(base * base, exponent / 2, result);
}

