a.
int a = 5;
int* number = &a;
cout << *number << endl;

b.
int a = 7;
long* realPtr = &a;
long* integerPtr;
integerPtr = realPtr;

c.
int a = 3;
int* x = &a;
int* y;
x = y;

d.
char s[] = "this is a character array";
char* a = s;
for ( ; *s != '\0'; s++)
cout << *s << ' ';

e.
short* numPtr = &result;
short result = 4;
short* genericPtr = numPtr;
result = *genericPtr + 7;

f.
double x = 19.34;
double* xPtr = &x;
cout << xPtr << endl;

g.
char a[] = "hello";
char* s = a;
cout << s << endl;

