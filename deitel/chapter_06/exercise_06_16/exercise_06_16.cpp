#include <iostream>
#include <cstdlib>

int
main()
{
    const int random1 = (1 + std::rand() % 2);
    std::cout << random1 << std::endl;

    const int random2 = (1 + std::rand() % 100);
    std::cout << random2 << std::endl;

    const int random3 = (std::rand() % 10);
    std::cout << random3 << std::endl;

    const int random4 = (1000 + std::rand() % 113);
    std::cout << random4 << std::endl;

    const int random5 = (-1 + std::rand() % 3);
    std::cout << random5 << std::endl;

    const int random6 = (-3 + std::rand() % 15);
    std::cout << random6 << std::endl;

    return 0;
}

