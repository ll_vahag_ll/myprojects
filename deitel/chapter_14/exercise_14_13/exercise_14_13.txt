Templates in C++ are a way of keeping the source code generic.
Function templates and class templates enable programmers to specify,
with a single code segment, an entire range of related (overloaded)functions
instead of writing the same segments for each type that we use in the programm.
So usage templates save a lot of time for programmer.
