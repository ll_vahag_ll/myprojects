#ifndef __ACCOUNT_HPP__
#define __ACCOUNT_HPP__

class Account
{
public:
    Account(const double balance);
    void setBalance(const double balance);
    double getBalance() const;
    void credit(const double credit);
    bool debit(const double debit);
protected:
    double balance_;

};

#endif /// __ACCOUNT_HPP__
