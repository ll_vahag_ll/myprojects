#include <iostream>
#include <unistd.h>

bool even(const int number1);

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number (-1 to quit): ";
        }

        int number1;
        std::cin >> number1;

        if (-1 == number1) {
            return 0;
        }

        if (even(number1)) {
            std::cout << "Number is even" << std::endl;
        } else {
            std::cout << "Number is not even" << std::endl;
        }
    }

    return 0;
}

bool
even(const int number1)
{
    return 0 == number1 % 2;
}

