#include "Date.hpp"

#include <iostream>

int
main()
{
    std::cout << "Input month: ";
    int month;
    std::cin >> month;

    std::cout << "Input day: ";
    int day;
    std::cin >> day;

    std::cout << "Input year: ";
    int year;
    std::cin >> year;

    Date date1(month, day, year);
    date1.displayDate();

    return 0;
}

