#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex();
    Complex(const double realPart, const double imaginaryPart);
    void setComplex(const double realPart, const double imaginaryPart);
    Complex add(const Complex& rhv);
    Complex subtract(const Complex& rhv);
    void print();
private:
    double realPart_;
    double imaginaryPart_;
};

#endif ///__COMPLEX_HPP__
