#include <iostream>
#include <unistd.h>
#include <iomanip> 
#include <cmath>

double hypotenuse(const double side1, const double side2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first side of straight triangle: ";
    }
    double side1;
    std::cin >> side1;

    if (side1 <= 0) {
        std::cout << "Error 1: Invalid side of straight triangle" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second side of straight triangle: ";
    }
    double side2;
    std::cin >> side2;

    if (side2 <= 0) {
        std::cout << "Error 1: Invalid side of straight triangle: ";
        return 1;
    }

    std::cout << "Side1" << std::setw(12) << "Side2" << std::setw(16) << "Hypotenuse" << std::endl;
    std::cout << std::setprecision(1) << std::fixed << side1 << std::setw(12)
              << std::setprecision(1) << std::fixed << side2 << std::setw(11) 
              << std::setprecision(1) << std::fixed << hypotenuse(side1, side2) << std::endl;

    return 0;

}

double
hypotenuse(const double side1, const double side2)
{
    const double side1Square = side1 * side1;
    const double side2Square = side2 * side2;
    const double hypotenuse = std::sqrt(side1Square + side2Square);
    return hypotenuse;
}

