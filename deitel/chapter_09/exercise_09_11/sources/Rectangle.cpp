#include "headers/Rectangle.hpp"
#include <iostream>
#include <cstdlib>

Rectangle::Rectangle(const double length, const double width)
{
    setLength(length);
    setWidth(width);
}

void
Rectangle::setLength(const double length)
{
    if (length < 0.0 || length > 20.0) {
        std::cout << "Error 1: Invalid length" << std::endl;
        exit(1);
    }

    length_ = length;
}

int
Rectangle::getLength()
{
    return length_;
}

void
Rectangle::setWidth(const double width)
{
    if (width < 0.0 || width > 20.0) {
        std::cout << "Error 2: Invalid width" << std::endl;
        exit(2);
    }
    width_ = width;
}

int
Rectangle::getWidht()
{
    return width_;
}

int
Rectangle::calculatePerimeter()
{
    return (getWidht() + getLength()) * 2;
}

void
Rectangle::printPerimeter()
{
    std::cout << "Perimeter is " << calculatePerimeter() << std::endl;
}

int
Rectangle::calculateArea()
{
    return getWidht() * getLength();
}

void
Rectangle::printArea()
{
    std::cout << "Area is " << calculateArea() << std::endl;
}

