#include "headers/PhoneNumber.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Enter phone number in the form (123) 456-7890:" << std::endl;
    }
    PhoneNumber phone;
    std::cin >> phone;

    std::cout << "The phone number entered was: ";
    phone.operator<<(std::cout);
    std::cout << std::endl;
    return 0;
}

