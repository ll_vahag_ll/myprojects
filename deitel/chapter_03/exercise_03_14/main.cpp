#include "Employee.hpp"

#include <iostream>

int
main()
{
    std::string name;
    std::string surName;
    int monthSalary;

    std::cout << "Enter first employee name: ";
    std::cin >> name;
    std::cout << "Enter first employee surename: ";
    std::cin >> surName;
    std::cout << "Enter first employee month salary: ";
    std::cin >> monthSalary;

    Employee employee1(name, surName, monthSalary);
    {
        std::cout << "First employee" << std::endl;
        std::cout << "               name: " << employee1.getName() << std::endl;
        std::cout << "               surname: " << employee1.getSurName() << std::endl;
        std::cout << "               month salary: " << employee1.getMonthSalary() << std::endl;
        std::cout << "               year salary: " << employee1.getYearSalary() << std::endl;
        std::cout << "               raise salary: " << employee1.getRaiseSalary() << std::endl;
    }

    std::cout << "Enter second employee name: ";
    std::cin >> name;
    std::cout << "Enter second employee surname: ";
    std::cin >> surName;
    std::cout << "Enter second employee month  salary: ";
    std::cin >> monthSalary;

    Employee employee2(name, surName, monthSalary);
    {
        std::cout << "Second employee" << std::endl;
        std::cout << "                name: " << employee2.getName() << std::endl;
        std::cout << "                surname: " << employee2.getName() << std::endl;
        std::cout << "                month salary: " << employee2.getMonthSalary() << std::endl;
        std::cout << "                year salary: " << employee2.getYearSalary() << std::endl;
        std::cout << "                raise salary: " << employee2.getRaiseSalary() << std::endl;
    }

    return 0;
}

