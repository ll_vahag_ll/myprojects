#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

#include <iostream>

class Complex
{
    friend std::istream& operator>>(std::istream& input, Complex& in);
    friend std::ostream& operator<<(std::ostream& output, const Complex& out);
public:
    Complex();
    Complex(const double realPart, const double imaginaryPart);
    void setComplex(const double realPart, const double imaginaryPart);
    Complex add(const Complex& rhv) const;
    Complex subtract(const Complex& rhv) const;
    bool operator==(const Complex& rhv) const;
private:
    double realPart_;
    double imaginaryPart_;
};

#endif ///__COMPLEX_HPP__
