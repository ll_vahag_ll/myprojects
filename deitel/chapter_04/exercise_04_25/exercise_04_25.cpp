#include <iostream>

int
main()
{
    int size;
    std::cout << "Input sqare length from 1 to 20: ";
    std::cin >> size;

    if (size < 1) {
        std::cout << "Error 1: Invalid lenght" << std::endl;
        return 1;
    }

    if (size > 20) {
        std::cout << "Error 1: Invalid lenght" << std::endl;
        return 1;
    }

    int column = 1;
    while (column <= size) {
        int row = 1;
        while (row <= size) {
            if (1 == row) {
                std::cout << '*';
            } else if (row == size) {
                std::cout << '*';
            } else if (1 == column) {
                std::cout << '*';
            } else if (column == size) {
                std::cout << '*';
            } else {
                std::cout << ' ';
            }
            ++row;
        }
        ++column;
        std::cout << std::endl;
    }
    return 0; 
}


