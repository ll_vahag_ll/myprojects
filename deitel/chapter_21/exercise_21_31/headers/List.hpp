#ifndef __LIST_HPP__
#define __LIST_HPP__

#include "ListNode.hpp"

template <typename NODETYPE>
class List
{
public:
    List();
    ~List();
    size_t size();
    void insertAtFront(const NODETYPE& value);
    void insertAtBack(const NODETYPE& value);
    void insert(const size_t pos, const NODETYPE& value = NODETYPE());
    bool removeFromFront(NODETYPE& value);
    bool removeFromBack(NODETYPE& value);
    void erase(const size_t pos);
    bool isEmpty() const;                  
    void print() const;
private:
    ListNode<NODETYPE>* firstPtr_;
    ListNode<NODETYPE>* lastPtr_;
};

#include "sources/List.cpp"

#endif ///__LIST_HPP__

