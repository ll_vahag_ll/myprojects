#ifndef __LISTNODE_HPP__
#define __LISTNODE_HPP__

#include <cstdlib>

template <typename NODETYPE> class List;                            

template <typename NODETYPE>
class ListNode
{
    friend class List<NODETYPE>;
public:
    ListNode(const NODETYPE& data, ListNode<NODETYPE>* nextPtr = NULL);
    NODETYPE getData() const;
private:
    NODETYPE data_;
    ListNode<NODETYPE> *nextPtr_;
};

#include "sources/ListNode.cpp"

#endif /// __LISTNODE_HPP__

