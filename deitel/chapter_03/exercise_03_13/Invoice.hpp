#include <iostream>

#include <string>

class Invoice
{
public:
    Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int pricePerItem);

    void setPartNumber(std::string partNumber);
    void setPartDescription(std::string partDescription);
    void setItemQuantity(int itemQuantity);
    void setPricePerItem(int pricePerItem);

    std::string getPartNumber();
    std::string getPartDescription();
    int getItemQuantity();
    int getPricePerItem();
    int getInvoiceAmount();

private:
    std::string partNumber_;
    std::string partDescription_;
    int itemQuantity_;
    int pricePerItem_;
};

