#include "headers/Array.hpp"

#include <gtest/gtest.h>

TEST(ArrayTest, NumberOfElements)
{
    Array<int, 7> arr;
    EXPECT_EQ(arr.getSize(), 7);
}

TEST(ArrayTest, Assignement)
{
    Array<int, 3> arr1(5);
    Array<int, 3> arr2(5);
    EXPECT_TRUE(arr1 == arr2);
    arr1[1] = 7;
    EXPECT_FALSE(arr1 == arr2);
}

TEST(ArrayTest, Operators)
{
    Array<int, 7> arr1(7);
    Array<int, 7> arr2(7);
    EXPECT_EQ(arr1, arr2);
    EXPECT_TRUE(arr1 == arr2);
    EXPECT_FALSE(arr1 != arr2);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

