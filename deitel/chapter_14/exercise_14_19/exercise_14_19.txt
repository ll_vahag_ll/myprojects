You can use a non-type parameter with a class template on a container such as an array or stack, 
for example to choose the size of the stack or array, which should be type size_t.
