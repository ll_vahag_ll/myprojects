#include <iostream>
#include <unistd.h>
#include <cassert>

bool multiple(const int number1, const int number2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input first integer: ";
    }

    int number1;
    std::cin >> number1;

    if (0 == number1) {
        std::cout << "Error 1: Invalid number" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second integer: ";
    }

    int number2;
    std::cin >> number2;

    if (multiple(number1, number2)) {
        std::cout << "Second is a multiple of the first" << std::endl;
    } else {
        std::cout << "Second is not a multiple of the first" << std::endl;
    }

    return 0;
}

bool
multiple(const int number1, int number2)
{
    assert(0 != number1);
    return 0 == number2 % number1;
}

