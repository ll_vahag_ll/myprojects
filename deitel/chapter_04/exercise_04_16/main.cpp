#include <iostream>

#include <iomanip>

int
main()
{
    while (true) {
        int hours;
        std::cout << "Input hours worked (-1 to end): ";
        std::cin >> hours;
        if (-1 == hours) {
            return 0;
        }
        if (-1 > hours) {
            std::cout << "Error 1: hours can't be negative" << std::endl;
            return 1;
        }

        double rate;
        std::cout << std::setprecision(2) << std::fixed;
        std::cout << "Input hourly rate of the worker ($00.00): ";
        std::cin >> rate;
        if (0 > rate) {
            std::cout << "Error 2: rate can't be negative" << std::endl;
            return 2;
        }

        double salary = static_cast<double>(hours) * rate;
        if (hours > 40) {
            salary += rate * 0.5 * (hours - 40);
        }
        std::cout << "Salary is " << salary << "$" << std::endl;
    }
    return 0;
}

