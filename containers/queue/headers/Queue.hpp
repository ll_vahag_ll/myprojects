#ifndef __T_QUEUE_HPP__
#define __T_QUEUE_HPP__

#include <vector>

namespace cd06 {

template <typename T, typename Sequence = std::vector<T> >
class Queue : private Sequence
{
    template <typename T2, typename Sequence2>
    friend bool operator==(const Queue<T2, Sequence2>& lhv, const Queue<T2, Sequence2>& rhv);
    template <typename T2, typename Sequence2>
    friend bool operator<(const Queue<T2, Sequence2>& lhv, const Queue<T2, Sequence2>& rhv);
    template <typename T2, typename Sequence2>
    friend bool operator>(const Queue<T2, Sequence2>& lhv, const Queue<T2, Sequence2>& rhv);
    template <typename T2, typename Sequence2>
    friend bool operator<=(const Queue<T2, Sequence2>& lhv, const Queue<T2, Sequence2>& rhv);
    template <typename T2, typename Sequence2>
    friend bool operator>=(const Queue<T2, Sequence2>& lhv, const Queue<T2, Sequence2>& rhv);

public:
    typedef typename Sequence::value_type value_type;
    typedef typename Sequence::size_type size_type;

public:
    Queue();
    Queue(const Queue& rhv);

    Queue& operator=(const Queue& rhv);

    bool empty() const;
    size_type size() const;

    value_type& front();
    const value_type& front() const;

    value_type& back();
    const value_type& back() const;

    void push(const value_type& d);
    void pop();
};

}

#include "sources/Queue.cpp"

#endif /// __T_QUEUE_HPP__

