#include <iostream>

int
main()
{
    int numberl, number2;

    std::cout << "Enter first integer: ";
    std::cin >> numberl;

    std::cout << "Enter second integer: ";
    std::cin >> number2;

    int sum = numberl + number2;
    std::cout << "Sum is " << sum << std::endl;

    int product = numberl * number2;
    std::cout << "Product is " << product << std::endl;

    int difference = numberl - number2;
    std::cout << "Difference is " << difference << std::endl;
                                                    
    if (0 == number2) {
        std::cout << "Error 1: Division by 0" << std::endl;
        return 1;
    }

    int quotient = numberl / number2;
    std::cout << "Quotient is " << quotient << std::endl;

    return 0;
}

