#include "Account.hpp"

#include <iostream>

Account::Account(int balance)
{
    if (0 > balance) {
        std::cout << "Warning 1: initial balance was invalid" << std::endl;
        balance_ = 0;
        return;
    }
    balance_ = balance;
}

int
Account::getBalance()
{
    return balance_;
}

int
Account::credit(int credit)
{
    if (0 > credit) {
        std::cout << "Warning 2: credit can't be less than 0";
        return 2;
    }

    balance_ = balance_ + credit; 
    return 0;
}
int
Account::debit(int debit)
{
    if (0 > debit) {
        std::cout << "Warning 3 : debit can't be less than 0";
            return 3;
    }

    if (debit > balance_) {
        std::cout << "Warning 4: debit can't be more than your balance" << std::endl;
        return 4;
    }
    balance_ = balance_ - debit;
    return 0;
}

