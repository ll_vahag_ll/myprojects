#ifndef __TWODAYPACKAGE_HPP__
#define __TWODAYPACKAGE_HPP__

#include "Package.hpp"

class TwoDayPackage : public Package
{     
public:
    TwoDayPackage(const std::string& name, const std::string& city,
                  const std::string& state, const std::string& sender,
                  const std::string& recipient, const int& zip,
                  const std::string& address, const double& weight,
                  const double& cost, const double& flatFee);

    void setFlatFee(const double& flatFee) { flatFee_ = flatFee; } ;
    double calculateCost() const;
private:
    double flatFee_;
}; 

#endif /// __TWODAYPACKAGE_HPP__
