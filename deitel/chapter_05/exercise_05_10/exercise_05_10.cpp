#include <iostream>

int
main()
{
    for (int number1 = 1; number1 <= 5; ++number1) {
        int factorial = 1;
        for (int counter = 1; counter <= number1; ++counter) {
            factorial *= counter;
        }

        std::cout << number1 << "! = " << factorial << std::endl;
    }
    return 0;
}
/// int max value is 2147483647, factorial of 20 is larger than 2147483647, that's why we can't calculate this.

