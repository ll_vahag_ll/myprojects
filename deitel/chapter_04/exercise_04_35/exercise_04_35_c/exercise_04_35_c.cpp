#include <iostream>

int
main()
{
    std::cout << "Input accuracy of e: ";
    int accuracy;
    std::cin >> accuracy;

    if (accuracy <= 0) {
        std::cout << "Error 1: Invalid accuracy" << std::endl;
        return 1;
    }

    std::cout << "Input exponent: ";
    double exponent;
    std::cin >> exponent;

    if (exponent < 0) {
        std::cout << "Error 2: Invalid exponent" << std::endl;
        return 2;
    }

    double factorial = 1;
    double e = 1;
    int counter = 1;
    double exponentMultiplication = 1;

    while (counter <= accuracy) {
        factorial *= counter;
        ++counter;
        exponentMultiplication *= exponent;
        e += exponentMultiplication / factorial;
    }

    std::cout << "e^x = " << e << std::endl;
    return 0;
}

