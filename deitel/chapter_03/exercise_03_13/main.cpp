#include "Invoice.hpp"

#include <iostream>

int
main()
{
    std::cout << "Input part number: ";
    std::string partNumber;
    std::cin >> partNumber;

    std::cout << "Input part description: ";
    std::string partDescription;
    std::cin >> partDescription;

    std::cout << "Input item quantity: ";
    int itemQuantity;
    std::cin >> itemQuantity;

    std::cout << "Input price per item: ";
    int pricePerItem;
    std::cin >> pricePerItem;

    Invoice invoice(partNumber, partDescription, itemQuantity, pricePerItem);
    std::cout << "\nNumber: " << invoice.getPartNumber() << std::endl;
    std::cout << "Description: " << invoice.getPartDescription() << std::endl;
    std::cout << "Quantity: " << invoice.getItemQuantity() << std::endl;
    std::cout << "Price: " << invoice.getPricePerItem() << "$" << std::endl;
    std::cout << "Amount is " << invoice.getInvoiceAmount() << "$" << std::endl;
    

    return 0;
}

