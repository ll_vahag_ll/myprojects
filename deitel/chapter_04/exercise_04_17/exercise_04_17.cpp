#include <iostream>

int
main()
{
    int largest = -2147483648;
    int counter = 1;
    while (counter <= 10) {
        int number;
        std::cout << "Number " << counter << ": ";
        std::cin >> number;

        if (number > largest) {
            largest = number;
        }
        ++counter;
    }
    std::cout << "Largest: " << largest << std::endl;
    return 0;
}

