#ifndef __DATA_SET_HPP__
#define __DATA_SET_HPP__

#include <iostream>
#include <utility>

namespace cd06 {

template<typename Data> class Set;

template <typename Data>
std::istream& operator>>(std::istream& in, Set<Data>& v);

template <typename Data>
std::ostream& operator<<(std::ostream& out, const Set<Data>& v);

template<typename Data>
class Set
{    
public:
    typedef Data value_type;
    typedef Data key_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

private:
    struct Node {
        Node()
            : data_(0)
            , parent_(NULL)
            , left_(NULL)
            , right_(NULL) {}

        Node(const_reference data)
            : data_(data)
            , parent_(NULL)
            , left_(NULL)
            , right_(NULL) {}

        Node(const Data& d, Node* p = NULL, Node* l = NULL, Node* r = NULL)
            : data_(d)
            , parent_(p)
            , left_(l)
            , right_(r) {}

        ~Node() {
            parent_ = NULL;
            left_ = NULL;
            right_ = NULL;
            data_.~value_type();
        }

    public:
        Data data_;
        Node* parent_;
        Node* left_;
        Node* right_;
    };

public:
    class const_iterator {   
        friend Set<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();

        const const_iterator& operator=(const const_iterator& rhv);
        const_reference operator*() const;
        Node* operator->() const;
        bool operator!() const;
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        const_iterator& operator++();
        const_iterator& operator--();

        bool isRoot() const;
        bool isRightParent() const;
        bool isLeftParent() const;

        void createLeft(const_reference initialValue);
        void createRight(const_reference initialValue);

    private:
        explicit const_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

public:
    class iterator : public const_iterator {
        friend Set<value_type>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();

        iterator& operator=(const iterator& rhv);
        const_reference operator*();
        Node* operator->();
        bool operator==(const iterator& rhv) const;
        bool operator!=(const iterator& rhv) const;

        iterator parent() const;
        iterator left() const;
        iterator right() const;

        iterator& goParent();
        iterator& goLeft();
        iterator& goRight();

        iterator firstLeftParent(const value_type& x);
        iterator firstRightParent(const value_type& x);

        void setParent(iterator& it);
        void setLeft(iterator& it);
        void setRight(iterator& it);

        int balance();
        int height(Node* ptr);
    private:
        explicit iterator(Node* ptr);
    };

public:
    Set();
    Set(const Set& rhv);
    template<typename InputIterator>
    Set(InputIterator f, InputIterator l);
    ~Set();

    void swap(Set<value_type>& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear(Node* p);

    const Set& operator=(const Set& rhv);
    bool operator==(const Set<value_type>& rhv) const;
    bool operator!=(const Set<value_type>& rhv) const;
    bool operator<(const Set<value_type>& rhv) const;
    bool operator<=(const Set<value_type>& rhv) const;
    bool operator>(const Set<value_type>& rhv) const;
    bool operator>=(const Set<value_type>& rhv) const;

    void visit(const Node* n, const size_type d);
    void preOrder(Node* n, const size_type d);
    void inOrder(Node* n, const size_type d);
    void postOrder(Node* n, const size_type d);

    iterator begin();
    const_iterator begin() const;
    iterator end();
    const_iterator end() const;

    const_iterator leftMost(Node* pos) const;
    iterator leftMost(Node* pos);
    const_iterator rightMost(Node* pos) const;
    iterator rightMost(Node* pos);

    void visit(const Node*);
    void print();

    std::pair<iterator, bool> insert(const value_type& x);
    std::pair<iterator, bool> insert(iterator pos, const value_type& x);
    template <typename InputIterator>
    void insert(InputIterator f, InputIterator l);

    void erase(iterator pos);
    size_type erase(const key_type& k);
    void erase(iterator f, iterator l);

    bool goDownAndInsert(iterator& it, const value_type& x);
    void goUp(iterator& it, const value_type& x);
    void balance(iterator& it);
    void rotateLeft(iterator& it);
    void rotateRight(iterator& it);

    iterator find(const key_type& k) const;
    size_type count(const key_type& k) const;
    iterator lower_bound(const key_type& k) const;
    iterator upper_bound(const key_type& k) const;

private:
    Node* root_;

};

}

#include "sources/Set.cpp"

#endif /// __DATA_SET_HPP__

