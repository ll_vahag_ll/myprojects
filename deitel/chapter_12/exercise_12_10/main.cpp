#include "headers/Account.hpp"
#include "headers/SavingsAccount.hpp"
#include "headers/CheckingAccount.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{

    if (isInteractive) {
        std::cout << "Please input your account balance: ";
    }
    double balance;
    std::cin >> balance;
    if (balance < 0.0) {
        std::cout << "Error 1: Initial balance was invalid" << std::endl;
        return 1;
    }

    Account account1(balance);
    std::cout << "Your account balance is " << account1.getBalance() << '$' << std::endl;

    SavingsAccount savingsAccount(balance, 5.00);
    std::cout << "Your interest rate is " << savingsAccount.getInterestRate() << '%' << std::endl;
    savingsAccount.increaseBalance();

    CheckingAccount checkingAccount(savingsAccount.getBalance(), 100.00);
    std::cout << "Fee charged per transaction is " << checkingAccount.getFee() << '$' << std::endl;

    if (isInteractive) {
        std::cout << "Transver Menu:" << std::endl;
        std::cout << "\t1 - Add money" << std::endl;
        std::cout << "\t2 - Take money" << std::endl;
        std::cout << "Choose: ";
    }

    double menu;
    std::cin >> menu;
    if (2 < menu || 1 > menu) {
        std::cout << "Error 2: Invalid menu item" << std::endl;
        return 1;
    }
    if (1 == menu) {
        if (isInteractive) {
            std::cout << "How much do you want to add your account?: ";
        }
        double credit;
        std::cin >> credit;
        if (credit < 0.0) {
            std::cout << "Error 2: Credit can't be less than 0" << std::endl;
            return 2;
        }
        account1.credit(credit);
        checkingAccount.credit(credit);
        std::cout << "Your account balance is " << checkingAccount.getBalance() << '$' << std::endl;
        return 0;
    }

    if (isInteractive) {
        std::cout << "How much do you want to take from your account? ";
    }
    double debit;
    std::cin >> debit;
    if (debit < 0.0) {
        std::cout << "Error 3: Debit can't be less than 0" << std::endl;
        return 3;
    }
    account1.debit(debit);
    checkingAccount.debit(debit);
    std::cout << "Your account balance is " << checkingAccount.getBalance() << '$' << std::endl;
    return 0;
}

