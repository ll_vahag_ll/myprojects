#include "headers/Date.hpp"

#include <iostream>
#include <cstdlib>

const int Date::days[SIZE] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

Date::Date(const int month, const int day, const int year)
{
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
Date::setMonth(const int month)
{
    if (month < 1 || month > 12) {
        std::cout << "Error 1: Invalid month" << std::endl;
        ::exit(1);
    }
    month_ = month;
}

void
Date::setDay(const int day)
{
    if (day < 1 || day > getLastDay()) {
        std::cout << "Error 2: Invalid day" << std::endl;
        ::exit(2);
    }
    day_ = day;
}

void
Date::setYear(const int year)
{
    if (year < 0) {
        std::cout << "Error 3: Invalid year" << std::endl;
        ::exit(3);
    }
    year_ = year;
}

int
Date::getLastDay()
{
    return (2 == month_ && isLeapYear()) ? 29 : days[month_];
}

bool
Date::isLeapYear()
{
    return year_ % 400 == 0 || (year_ % 100 != 0 && year_ % 4 == 0);
}

void
Date::print()
{
    std::cout << month_ << '/' << day_ << '/' << year_;
}

void
Date::nextDay()
{
    ++day_;
    if (day_ > getLastDay()) {
        ++month_;
        day_ = 1;
        if (13 == month_) {
            ++year_;
            month_ = 1;
        }
    }
}

