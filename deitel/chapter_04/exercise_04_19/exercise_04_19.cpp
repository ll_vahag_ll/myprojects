#include <iostream>
#include <limits>

int
main()
{
    int largest = std::numeric_limits<int>::min();
    int secondLargest = largest;
    int counter = 1;

    while (counter <= 10) {
        int number;
        std::cout << "Number " << counter << ": ";
        std::cin >> number;

        if (number > largest) {
            secondLargest = largest;
            largest = number;
        } else if (number > secondLargest) {
            number = secondLargest;
        }
        ++counter;
    }
    std::cout << "Largest: " << largest << std::endl;
    std::cout << "Second largest: " << secondLargest << std::endl;
    return 0;
}

