#include <iostream>
#include <unistd.h>
#include <cassert>
#include <cstdlib>

int askAndInput();
int calculateSeconds(const int hour1, const int minute1, const int second1);
void calculateDifference();
void checkValidation(const int hour1, const int minute1, const int second1);

int
main()
{
    calculateDifference();
    return 0;
}

int
askAndInput()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the time (hours, minutes and seconds): ";
    }
    int hour1, minute1, second1;
    std::cin >> hour1 >> minute1 >> second1;
    checkValidation(hour1, minute1, second1);
    return calculateSeconds(hour1, minute1, second1);
}
    
void
calculateDifference()
{
    const int time1 = askAndInput();
    const int time2 = askAndInput();
    
    std::cout << "Amount of time in seconds between two times is " << time1 - time2 << std::endl;
}

void
checkValidation(const int hour1, const int minute1, const int second1)
{
    if (hour1 > 23 || hour1 < 0) {
        std::cout << "Error 1: Invalid hours" << std::endl;
        ::exit(1);
    }
    if (minute1 > 59 || minute1 < 0) {
        std::cout << "Error 2: Invalid minutes" << std::endl;
        ::exit(2);
    }
    if (second1 > 59 || second1 < 0) {
        std::cout << "Error 3: Invalid seconds" << std::endl;
        ::exit(3);
    }
}

int
calculateSeconds(const int hour1, const int minute1, const int second1)
{
    assert(hour1 < 23 && hour1 > 0);
    assert(minute1 < 59 && minute1 > 0);
    assert(second1 < 59 && second1 > 0);
    return hour1 * 3600 + minute1 * 60 + second1;
}

