#include <iostream>
#include <list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void
input(std::list<int>& l, const int size)
{
    if (isInteractive) {
        std::cout << "Input integers: ";
    }
    for (int i = 0; i < size; ++i) {
        int temp;
        std::cin >> temp;
        l.push_back(temp);
    }
}

void
merge(const std::list<int>& list1, const std::list<int>& list2, std::list<int>& list3)
{
    list3 = list1;
    std::list<int>::const_iterator it = list2.begin();
    for (; it != list2.end(); ++it) {
        list3.push_back(*it);
    }
    list3.sort();
}

void
print(const std::list<int>& l)
{
    for (std::list<int>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ", ";
    }
}

int
main()
{
    std::list<int> lst1;
    const int size = 5;
    input(lst1, size);
    std::cout << "First integer list contains: ";
    print(lst1);
    std::cout << std::endl;

    std::list<int> lst2;
    input(lst2, size);
    std::cout << "Second integer list contains: ";
    print(lst2);
    std::cout << std::endl;

    std::cout << "Integer list after merge: ";
    std::list<int> lst3;
    merge(lst1, lst2, lst3);
    print(lst3);
    std::cout << std::endl;
    return 0;
}

