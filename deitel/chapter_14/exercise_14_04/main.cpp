#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

template <typename T>
int
printArray(T* array, const int lowSubscript, const int highSubscript)
{
    for (int i = lowSubscript; i <= highSubscript; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "Number of printed elements: ";
    return highSubscript - lowSubscript + 1;
}

int
validateNumberOfPrintedElements(const int lowSubscript, const int highSubscript, const int count)
{
    if (lowSubscript >= highSubscript) {
        std::cout << "Error 1: lowSubscript can't be more than highSubscript";
        return 1;
    }

    if (lowSubscript < 0) {
        std::cout << "Error 2: lowSubscript can't be less than 0";
        return 2;
    }

    if (highSubscript > count - 1) {
        std::cout << "Error 3: highSubscript can't be more than array elements count";
        return 3;
    }
    return 0;
}

int
main()
{
    if (isInteractive) {
        std::cout << "Input integers: ";
    }
    const int ACOUNT = 5;
    int a[ACOUNT];
    for (int i = 0; i < ACOUNT; ++i) {
        std::cin >> a[i];
    }
    std::cout << "Array a contains:" << std::endl;
    const int validateIntegers = validateNumberOfPrintedElements(0, 4, ACOUNT);
    if (validateIntegers != 0) {
        return validateIntegers;
    }
    std::cout << printArray(a, 0, 4) << std::endl << std::endl;

    if (isInteractive) {
        std::cout << "Input doubles: ";
    }
    const int BCOUNT = 7;
    double b[BCOUNT];
    for (size_t i = 0; i < BCOUNT; ++i) {
        std::cin >> b[i];
    }
    std::cout << "Array b contains:" << std::endl;
    const int validateDoubles = validateNumberOfPrintedElements(2, 5, BCOUNT);
    if (validateDoubles != 0) {
        return validateDoubles;
    }
    std::cout << printArray(b, 2, 5) << std::endl << std::endl;

    if (isInteractive) {
        std::cout << "Input chars: ";
    }
    const int CCOUNT = 6;
    char c[CCOUNT];
    for (size_t i = 0; i < CCOUNT; ++i) {
        std::cin >> c[i];
    }
    std::cout << "Array c contains:" << std::endl;
    const int validateCharacters = validateNumberOfPrintedElements(9, 3, CCOUNT);
    if (validateCharacters != 0) {
        return validateCharacters;
    }
    std::cout << printArray(c, 9, 3) << std::endl;

    return 0;
}

