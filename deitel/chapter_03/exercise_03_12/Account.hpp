class Account
{
public:
    Account(int balance);
    int getBalance();
    int credit(int credit);
    int debit(int debit);

private:
    int balance_ ;

};
