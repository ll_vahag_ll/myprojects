#include "headers/Time.hpp"
#include <iostream>

int
main()
{
    Time time;
    time.printUniversal();
    time.printStandard();
    return 0;
}

