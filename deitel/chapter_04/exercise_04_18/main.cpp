#include <iostream>

int
main()
{
    int number = 1;
    std::cout << "N\t10*N\t100*N\t1000*N" << std::endl;

    while (number <= 5) {
        std::cout << number << "/t";
        std::cout << number * 10 << "/t";
        std::cout << number * 100 << "/t";
        std::cout << number * 1000 << "/t" << std::endl;
        ++number;
    }
    return 0;
}

