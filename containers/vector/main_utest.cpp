#include "headers/Vector.hpp"

#include <gtest/gtest.h>

TEST(IntVectorTest, DefaultConstSize)
{
    cd06::Vector<int> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(IntVectorTest, ConstSize)
{
    cd06::Vector<int> v(10);
    EXPECT_EQ(v.size(), 10);
}

TEST(IntVectorTest, DefaultConstEmpty)
{
    cd06::Vector<int> v;
    EXPECT_TRUE(v.empty());
}

TEST(IntVectorTest, Stream)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    std::ostringstream out;
    out << v;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, Copy)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    const cd06::Vector<int> cv = v;
    std::ostringstream out;
    out << cv;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, Assignment)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;
    v = v;
    
    cd06::Vector<int> v1, v2;
    v1 = v2 = v;
    std::ostringstream out;
    out << v1;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, Capacity)
{
    cd06::Vector<int> v(5);
    EXPECT_EQ(v.capacity(), 5);
}

TEST(IntVectorTest, Resize1)
{
    cd06::Vector<int> v(3);
    v.resize(4, 5);
    EXPECT_EQ(v.size(), 4);
    EXPECT_EQ(v.capacity(), 4);
}

TEST(IntVectorTest, Resize2)
{
    cd06::Vector<int> v(3);
    v.resize(2, 5);
    EXPECT_EQ(v.size(), 2);
    EXPECT_EQ(v.capacity(), 3);
}

TEST(IntVectorTest, Reserve1)
{
    cd06::Vector<int> v(4);
    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
}

TEST(IntVectorTest, Reserve2)
{
    cd06::Vector<int> v(4);
    v.reserve(5);
    EXPECT_EQ(v.capacity(), 5);
}

TEST(IntVectorTest, PushBack1)
{
    cd06::Vector<int> v(4);
    v.push_back(10);
    EXPECT_EQ(v.capacity(), 8);
    EXPECT_EQ(v.size(), 5);
}

TEST(IntVectorTest, PushBack2)
{
    cd06::Vector<int> v(3);
    v.push_back(10);
    v.push_back(10);
    v.push_back(10);
    v.push_back(10);
    EXPECT_EQ(v.capacity(), 12);
}

TEST(IntVectorTest, PopBack1)
{
    cd06::Vector<int> v(4);
    v.pop_back();
    EXPECT_EQ(v.size(), 3);
}

TEST(IntVectorTest, PopBack2)
{
    cd06::Vector<int> v(5);
    v.pop_back();
    v.pop_back();
    EXPECT_EQ(v.size(), 3);
}

TEST(IntVectorTest, equal)
{
    cd06::Vector<int>v1(10);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 == v2);
}

TEST(IntVectorTest, notEqual)
{
    cd06::Vector<int>v1(10);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 != v2);
}

TEST(IntVectorTest, smaller)
{
    cd06::Vector<int>v1(9);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 < v2);
}

TEST(IntVectorTest, smallerOrEqual)
{
    cd06::Vector<int>v1(8);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 <= v2);
}

TEST(IntVectorTest, greather)
{
    cd06::Vector<int>v1(11);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 > v2);
}

TEST(IntVectorTest, greatherOrEqual)
{
    cd06::Vector<int>v1(11);
    cd06::Vector<int>v2(10);
    EXPECT_TRUE(v1 >= v2);
}

TEST(IntVectorTest, interatorIncr)
{
    cd06::Vector<int>v(10);
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    cd06::Vector<int>::iterator it(v.begin());
    ++it;
    EXPECT_EQ(*it, 2);
    it++;
    EXPECT_EQ(*it, 3);

    EXPECT_EQ(*it, 3);
    --it;
    EXPECT_EQ(*it, 2);
    it--;
    EXPECT_EQ(*it, 1);
}

TEST(IntVectorTest, IteratorOperators)
{
    cd06::Vector<int> v1(3, 4);

    cd06::Vector<int>::const_iterator it1(v1.begin());
    cd06::Vector<int>::const_iterator it2(v1.begin());

    EXPECT_TRUE(it1 == it2);
    it1 += 3;
    EXPECT_TRUE(it1 != it2);
    EXPECT_TRUE(it1 > it2);
    it1 -= 4;
    EXPECT_TRUE(it1 <= it2);
    EXPECT_TRUE(it1 < it2);
    it1 = it2 - 1;
    it1 = it2 + 1;
    EXPECT_TRUE(it1 >= it2);
}

TEST(IntVectorTest, ReverseIteratorOperators)
{
    cd06::Vector<int> v1(10);
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v1;

    cd06::Vector<int>::const_reverse_iterator rIt1(v1.rbegin());

    cd06::Vector<int>::const_reverse_iterator rIt2(v1.rbegin());
    EXPECT_TRUE(rIt1 == rIt2);
    rIt2 += 1;
    EXPECT_TRUE(rIt1 != rIt2);
    EXPECT_TRUE(rIt1 < rIt2);
    rIt2 -= 1;
    EXPECT_TRUE(rIt1 <= rIt2);
    rIt1 = v1.rend() - 1;
    EXPECT_TRUE(rIt1 > rIt2);
    rIt1 = rIt2 + 2;
    EXPECT_TRUE(rIt1 >= rIt2);

}

TEST(IntVectorTest, Insert1)
{
    cd06::Vector<int> v1(9);
    const std::string str = "{ 1 2 3 4 5 6 8 9 10 }";
    std::istringstream in(str);
    in >> v1;

    cd06::Vector<int>::iterator it = v1.begin();
    it += 6;
    v1.insert(it, 7);

    cd06::Vector<int> v2(10);
    const std::string str2 = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in2(str2);
    in2 >> v2;
    EXPECT_EQ(v1, v2);

}

TEST(IntVectorTest, Insert2)
{
    cd06::Vector<int> v1(9);
    const std::string str1 = "{ 1 2 3 4 5 6 7 8 9 }";
    std::istringstream in1(str1);
    in1 >> v1;

    cd06::Vector<int>::iterator it = v1.begin();
    it += 6;
    v1.insert(it, 3, 7);

    cd06::Vector<int> v2(12);
    const std::string str2 = "{ 1 2 3 4 5 6 7 7 7 7 8 9 }";
    std::istringstream in2(str2);
    in2 >> v2;

    EXPECT_EQ(v1, v2);
}

TEST(IntVectorTest, Insert3)
{
    cd06::Vector<int> v1(5);
    const std::string str1 = "{ 1 2 8 9 10 }";
    std::istringstream in1(str1);
    in1 >> v1;

    cd06::Vector<int> v2(5);
    const std::string str2 = "{ 3 4 5 6 7 }";
    std::istringstream in2(str2);
    in2 >> v2;

    cd06::Vector<int>::iterator it = v1.begin();
    it += 2;
    cd06::Vector<int>::iterator f = v2.begin();
    cd06::Vector<int>::iterator l = v2.end();
    l -= 1;

    v1.insert(it, f, l);

    cd06::Vector<int> v3(10);
    const std::string str3 = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in3(str3);
    in3 >> v3;

    EXPECT_EQ(v1, v3);
}

TEST(IntVectorTest, Erase1)
{
    cd06::Vector<int> v1(10);
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v1;

    cd06::Vector<int>::iterator it = v1.begin();
    it += 3;
    v1.erase(it);
}

TEST(IntVectorTest, Erase2)
{
    cd06::Vector<int> v1(10);
    const std::string str1 = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in1(str1);
    in1 >> v1;

    typedef cd06::Vector<int>::iterator It;
    It f = v1.begin();
    f += 2;
    It l = f;
    l += 4;
    v1.erase(f, l);

    cd06::Vector<int> v2(5);
    const std::string str2 = "{ 1 2 8 9 10 }";
    std::istringstream in2(str2);
    in2 >> v2;

    EXPECT_EQ(v1, v2);
}

TEST(IntVectorTest, RangeConst)
{
    cd06::Vector<int> v1(10);
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v1;

    typedef cd06::Vector<int>::iterator It;
    It f = v1.begin();
    It l = v1.begin();
    l += 3;
    cd06::Vector<int> v2(f, l);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

