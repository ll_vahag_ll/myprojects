#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__

#include <iostream>

template <typename T> 
class Array
{
public:
    Array(size_t arraySize = 10);
    ~Array();

    void print() const;

private:
    size_t size_;
    T* ptr_;
};

template <>
class Array<float>
{
public:
    Array(size_t arraySize = 10);
    ~Array();

    void print() const;

private:
    size_t size_;
    float* ptr_;
};

#include "sources/Array.cpp"

#endif /// __ARRAY_HPP__

