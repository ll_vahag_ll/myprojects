#include <iostream>
#include <unistd.h>
#include <cstdlib>

int randomNumber();
void randomAnswerForRightAnswer();
void randomAnswerForWrongAnswer();
void checkTheEquation();
int inputAnswer(const int firstNumber, const int secondNumber);
bool checkAnswer(const int firstNumber, const int secondNumber, const int multiplication);

int
main()
{
    checkTheEquation();
    return 0;
}

int
randomNumber()
{
    return (1 + std::rand() % 9);
}

void
randomAnswerForRightAnswer()
{
    switch(std::rand() % 4) {
    case 0: std::cout << "Very good!" << std::endl; break;
    case 1: std::cout << "Excellent!" << std::endl; break;
    case 2: std::cout << "Nice work!" << std::endl; break;
    case 3: std::cout << "Keep up the good work!" << std::endl; break;
    }
}

void
randomAnswerForWrongAnswer()
{
    switch (std::rand() % 4) {
    case 0: std::cout << "No. Please try again." << std::endl; break;
    case 1: std::cout << "Wrong. Try once more." << std::endl; break;
    case 2: std::cout << "Don't give up!" << std::endl;        break;
    case 3: std::cout << "No. Keep trying." << std::endl;      break;
    }
}

void
checkTheEquation()
{

    int counter = 0;
    int rightAnswers = 0;
    while (counter <= 10) {
        const int firstNumber = randomNumber();
        const int secondNumber = randomNumber();
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is " << firstNumber << " * " << secondNumber << "? "<< std::endl;
        }
        if (inputAnswer(firstNumber, secondNumber)) {
            randomAnswerForRightAnswer();
            ++rightAnswers;
        } else {
            randomAnswerForWrongAnswer();
        }
        ++counter;
    }
    if (rightAnswers * 10 < 75) {
        std::cout << "Please ask your instructor for extra help" << std::endl;
        return;
    } 
    std::cout << "Good job!" << std::endl;
}

int
inputAnswer(const int firstNumber, const int secondNumber)
{
    int multiplication;
    std::cin >> multiplication;
    return checkAnswer(firstNumber, secondNumber, multiplication);
}

bool
checkAnswer(const int firstNumber, const int secondNumber, const int multiplication)
{
    return multiplication == firstNumber * secondNumber;
}

