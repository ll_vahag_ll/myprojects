#include <iostream>

int
main()
{
    int number1;

    std::cout << "Enter five-dight number";
    std::cin >> number1;

    if (10000 > number1) {
        std::cout << "Error 1: the number is small" << std::endl;
        return 1;
    }
    if (99999 < number1) {
        std::cout << "Error 2: the number is large" << std::endl;
        return 2;
    }
    std::cout << number1 / 10000 << "   ";
    int number2 = number1 % 10000;

    std::cout << number2 / 1000 << "   ";
    int number3 = number2 % 1000;

    std::cout << number3 / 100 << "   ";
    int number4 = number3 % 100;

    std::cout << number4 / 10 << "   ";
    std::cout << number4 % 10 << std::endl;

    return 0;
}

