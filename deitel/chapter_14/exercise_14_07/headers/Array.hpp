#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__

#include <iostream>

template <typename elementType, size_t numberOfElements = 20> 
class Array
{
public:
    Array(const elementType element = elementType());
    Array(const Array<elementType, numberOfElements>& arrayToCopy);

    size_t getSize() const;

    const Array& operator=(const Array<elementType, numberOfElements>& rhv);
    bool operator==(const Array<elementType, numberOfElements>& rhv) const;
    bool operator!=(const Array<elementType, numberOfElements>& rhv) const;

    elementType& operator[](size_t subscript);
    elementType operator[](size_t subscript) const;
private:
    elementType ptr_[numberOfElements];
};

#include "sources/Array.cpp"

#endif /// __ARRAY_HPP__

