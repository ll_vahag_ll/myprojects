#include "headers/Date.hpp"

#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input date(month, day, year): ";
    }
    int month, day, year;
    std::cin >> month >> day >> year;

    Date date1(month, day, year);
    date1.nextDay();
    date1.print();
    std::cout << std::endl;

    return 0;
}

