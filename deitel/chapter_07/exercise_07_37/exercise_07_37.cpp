#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);
int findMinimum(int array1[], int SIZE);

int
main()
{
    int SIZE = 10;
    int array1[SIZE];

    if (isInteractive) {
        std::cout << "Input array elements: ";
    }
    for (int i = 0; i <= SIZE; ++i) {
        std::cin >> array1[i];
    }

    std::cout << "Minimum is " << findMinimum(array1, SIZE) << std::endl;
    return 0;
}

int
findMinimum(int array1[], int SIZE)
{
    if (1 == SIZE) {
        return array1[0];
    }

    return std::min(array1[0], findMinimum(array1 + 1, SIZE - 1));
}

