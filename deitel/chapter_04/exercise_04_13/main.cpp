#include <iostream>

#include <iomanip>

int
main()
{
    double totalMiles = 0;
    double totalGallons = 0;
    while (true) {
        int miles;
        std::cout << "Input the miles used (-1 to quit): ";
        std::cin >> miles;

        if (-1 == miles) {
            return 0;
        }
        if (0 > miles) {
            std::cout << "Error 1: miles can't be negative" << std::endl;
            return 1;
        }

        int gallons;
        std::cout << "Input gallons: ";
        std::cin >> gallons;

        if (0 > gallons) {
            std::cout << "Error 2: gallons can't be negative" << std::endl;
            return 2;
        }

        totalMiles +=  miles;
        double tankful = static_cast<double>(miles) / gallons;
        std::cout << std::setprecision(6) << std::fixed;
        std::cout << "MPG this tankful: " << tankful << std::endl;

        totalGallons += gallons;
        double totalMPG = totalMiles / totalGallons;
        std::cout << "Total MPG: " << totalMPG << std::endl;

    }
    return 0;
}
