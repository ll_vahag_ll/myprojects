#include "headers/List.hpp"

#include <cassert>
#include <algorithm>
#include <cmath>

namespace cd06 {

template <typename T>
std::istream&
operator>>(std::istream& in, List<T>& lst)
{
    char bracket;
    in >> bracket;
    if (bracket != '{') {
        std::cerr << "Error 1: Invalid List!" << std::endl;
        ::exit(1);
    }

    typedef typename List<T>::size_type size_type;
    const size_type s = lst.size();

    typename List<T>::iterator it = lst.begin();
    for (size_t i = 0; i < s; ++i, ++it) {
        in >> *it;
    }

    in >> bracket;
    if (bracket != '}') {
        std::cerr << "Error 2: Invalid List!" << std::endl;
        ::exit(2);
    }

    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const List<T>& lst)
{
    out << "{ ";
    typedef typename List<T>::size_type size_type;
    const size_type s = lst.size();
    typename List<T>::const_iterator it = lst.begin();
    for (size_t i = 0; i < s; ++i, ++it) {
        out << *it << ' ';
    }
    out << '}';
    return out;
}

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
List<T>::List(const size_type size)
    : begin_(NULL)
    , end_(NULL)
{
    for (size_type i = 0; i < size; ++i) {
        push_front(value_type());
    }
}

template <typename T>
List<T>::List(const int size, const_reference init)
    : begin_(NULL)
    , end_(NULL)
{
    for (int i = 0; i < size; ++i) {
        push_front(init);
    }
}

template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    assert(f || f < l);
    for (; f != l; ++f) {
        push_back(*f);
    }
}

template <typename T>
List<T>::List(const List<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    const_iterator it = rhv.begin();
    for (; it != rhv.end(); ++it) {
        this->push_back(*it);
    }
}

template <typename T>
List<T>::~List()
{
    clear();
}

template <typename T>
void
List<T>::swap(List<value_type>& rhv)
{
    Node* temp = begin_;
    begin_ = rhv.begin_;
    rhv.begin_ = temp;
    temp = end_;
    end_ = rhv.end_;
    rhv.end_ = temp;
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    if (NULL == begin_) {
        return 0;
    }
    const_iterator it = begin();
    size_type size = 0;
    for (; it.ptr_ != end_->next_; ++it, ++size) {
    }
    assert(size <= max_size());
    return size;
}

template <typename T>
typename List<T>::size_type
List<T>::max_size() const
{
    return static_cast<size_type>(std::pow(2, sizeof(pointer) * 8) / (sizeof(value_type) + sizeof(begin_) + sizeof(end_))); /// For single list
}

template <typename T>
bool
List<T>::empty() const
{
    return (NULL == begin_);
}

template <typename T>
void
List<T>::resize(const size_type size, const_reference init)
{
    size_type thisSize = this->size();
    while (size > thisSize) {
        push_back(init);
        ++thisSize;
    }

    while (size < thisSize) {
        pop_back();
        --thisSize;
    }
}

template <typename T>
void
List<T>::clear()
{
    while (begin_ != NULL) {
        pop_front();
    }
}

template <typename T>
void
List<T>::push_front(const_reference element)
{
    begin_ = new Node(element, begin_, NULL);
    if (NULL == end_)  {
        end_ = begin_;
        return;
    }
    begin_->next_->prev_ = begin_;
}

template <typename T>
void
List<T>::pop_front()
{
    assert(begin_ != NULL);
    if (begin_ != end_) {
        begin_ = begin_->next_;
        begin_->prev_->~Node();
        return;
    }
    delete begin_;
    begin_ = end_ = NULL;
}

template <typename T>
void
List<T>::push_back(const_reference element)
{
    end_ = new Node(element, NULL, end_);
    if (NULL == begin_)  {
        begin_ = end_;
        return;
    }
    end_->prev_->next_ = end_;
}

template <typename T>
void
List<T>::pop_back()
{
    assert(begin_ != NULL);
    if (begin_ != end_) {
        end_ = end_->prev_;
        delete end_->next_;
        end_->next_ = NULL;
        return;
    }
    delete end_;
    begin_ = end_ = NULL;
}

template <typename T>
typename List<T>::const_reference
List<T>::front() const
{
    assert(begin_ != NULL);
    return begin_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::front()
{
    assert(begin_ != NULL);
    return begin_->data_;
}

template <typename T>
typename List<T>::const_reference
List<T>::back() const
{
    assert(end_ != NULL);
    return end_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::back()
{
    assert(end_ != NULL);
    return end_->data_;
}

template <typename T>
const List<T>&
List<T>::operator=(const List<T>& rhv)
{
    iterator it(rhv->begin_);
    for ( ; it->next_ != NULL; ++it) {
        push_back(it->data_);
    }
    return this;
}

template <typename T>
bool
List<T>::operator==(const List<T>& rhv) const
{
    if (size() == rhv.size()) {
        const_iterator it1 = begin();
        const_iterator it2 = rhv.begin();
        for (;it1 != end(); ++it1, ++it2) {
            if (it1 != it2) {
                return false;
            }
        }
        return true;
    }
    return false;
}

template <typename T>
bool
List<T>::operator!=(const List<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
List<T>::operator<(const List<T>& rhv) const
{
    if (size() < rhv.size()) {
        return true;
    }
    if (size() == rhv.size()) {
        const_iterator it1 = begin();
        const_iterator it2 = rhv.begin();
        for ( ; it1 != end(); ++it1, ++it2) {
            if (*it1 > *it2) {
                return false;
            }
            if (*it1 < *it2) {
                return true;
            }
        }
    }
    return false;
}

template <typename T>
bool
List<T>::operator<=(const List<T>& rhv) const
{
    return !(*this > rhv);

}

template <typename T>
bool
List<T>::operator>(const List<T>& rhv) const
{
    return rhv < *this;
}

template <typename T>
bool
List<T>::operator>=(const List<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    return iterator(end_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{
    return iterator(end_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    return iterator(NULL);
}



template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos, const value_type& x)
{
    assert(pos != end());
    Node* temp = new Node(x, pos.next(), pos.ptr_);
    pos.ptr_->next_ = temp;
    if (pos.ptr_ == end_) {
        end_ = temp;
        ++pos;
        return pos;
    }
    temp->next_->prev_ = temp;
    iterator it(temp);
    return it;
}

template <typename T>
void
List<T>::insert_after(iterator pos, int n, const value_type& x)
{
    while (n > 0) {
        pos = insert_after(pos, x);
        --n;
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    assert(f);
    while (f != l) {
        pos = insert_after(pos, *f);
        ++f;
    }
}


template <typename T>
typename List<T>::iterator
List<T>::insert(iterator pos, const value_type& x)
{
    if (pos.ptr_ == begin_) {
        push_front(x);
        return begin();
    }
    --pos;
    pos = insert_after(pos, x);
    return pos;
}

template <typename T>
void
List<T>::insert(iterator pos, int n, const value_type& x)
{
    while (n > 0) {
        pos = insert(pos, x);
        --n;
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(f);
    while (f != l) {
        pos = insert(pos, *f);
        ++f;
    }
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos)
{
    assert(pos != end());
    if (pos.ptr_ == begin_) {
        pop_front();
        return begin();
    }
    iterator it = begin();
    for (; it.next() != pos.ptr_; ++it) {}
    it.ptr_->next_ = pos.next();

    if (end_ == pos.ptr_) {
        end_ = it.ptr_;
    }
    delete pos.ptr_;
    return it;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator f, iterator l)
{
    assert(f != end() && l != iterator(end_->next_) && l != f);
    while (f != l) {
        f = erase(f);
    }
    return f;
}

/// const_iterator implementation /////////////////////////////////////
template <typename T>
List<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator=(const const_iterator& rhv)
{
    this->ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const T&
List<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
const T*
List<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator++()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator++(value_type)
{
    const_iterator temp;
    temp.ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator--()
{
    ptr_ = ptr_->prev_;
    return *this;
}

template <typename T>
const typename List<T>::const_iterator
List<T>::const_iterator::operator--(value_type)
{
    const_iterator temp;
    temp.ptr_ = ptr_->prev_;
    return temp;
}

template <typename T>
bool
List<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return this->ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return this->ptr_ != rhv.ptr_;
}

template <typename T>
List<T>::const_iterator::operator bool() const
{
    return ptr_ != NULL;
}

template <typename T>
typename List<T>::Node*
List<T>::const_iterator::next() const
{
    return ptr_->next_;
}

template <typename T>
typename List<T>::Node*
List<T>::const_iterator::prev() const
{
    return ptr_->prev_;
}

/// iterator implementation /////////////////////////////////////

template <typename T>
List<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
List<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv.ptr_)
{
}

template <typename T>
List<T>::iterator::~iterator()
{
    const_iterator::ptr_ = NULL;
}

template <typename T>
T&
List<T>::iterator::operator*()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_->data_;
}

template <typename T>
T*
List<T>::iterator::operator->()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_;
}

template <typename T>
const typename List<T>::iterator&
List<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
bool
List<T>::iterator::operator==(const iterator& rhv) const
{
    return const_iterator::ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::iterator::operator!=(const iterator& rhv) const
{
    return const_iterator::ptr_ != rhv.ptr_;
}

/// const_reverse_iterator implementation /////////////////////////////////////

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}


template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{
    ptr_ = NULL;
}

template <typename T>
const typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    this->ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const T&
List<T>::const_reverse_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
const T*
List<T>::const_reverse_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++()
{
    ptr_ = ptr_->prev_;
    return ptr_;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(value_type)
{
    ptr_ = ptr_->prev_;
    return ptr_;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--()
{
    ptr_ = ptr_->next_;
    return ptr_;
}

template <typename T>
const typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(value_type)
{
    ptr_ = ptr_->next_;
    return ptr_;
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator==(const const_reverse_iterator& rhv) const
{
    return this->ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator!=(const const_reverse_iterator& rhv) const
{
    return this->ptr_ != rhv.ptr_;
}

template <typename T>
List<T>::const_reverse_iterator::operator bool() const
{
    return ptr_ != NULL;
}

template <typename T>
typename List<T>::Node*
List<T>::const_reverse_iterator::next() const
{
    return ptr_->prev_;
}

template <typename T>
typename List<T>::Node*
List<T>::const_reverse_iterator::prev() const
{
    return ptr_->next_;
}

/// reverse_iterator implementation /////////////////////////////////////

template <typename T>
List<T>::reverse_iterator::reverse_iterator(Node* ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator(NULL)
{
}


template <typename T>
List<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv.ptr_)
{
}

template <typename T>
List<T>::reverse_iterator::~reverse_iterator()
{
    const_reverse_iterator::ptr_ = NULL;
}

template <typename T>
T&
List<T>::reverse_iterator::operator*()
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_->data_;
}

template <typename T>
T*
List<T>::reverse_iterator::operator->()
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_;
}

template <typename T>
const typename List<T>::reverse_iterator&
List<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{
    const_reverse_iterator::ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
bool
List<T>::reverse_iterator::operator==(const reverse_iterator& rhv) const
{
    return const_reverse_iterator::ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::reverse_iterator::operator!=(const reverse_iterator& rhv) const
{
    return const_reverse_iterator::ptr_ != rhv.ptr_;
}

}

