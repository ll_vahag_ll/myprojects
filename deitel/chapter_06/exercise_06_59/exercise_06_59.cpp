#include <iostream>
#include <unistd.h>

template <typename T>
T
maximum(const T& number1, const T& number2, const T& number3)
{
    T max = number1;
    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    return max;
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input three numbers: ";
    }
    int number1;
    int number2;
    int number3;
    std::cin >> number1 >> number2 >> number3;
    std::cout << "Maximum is " << maximum(number1, number2, number3) << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input three characters: ";
    }
    char number4;
    char number5;
    char number6;
    std::cin >> number4 >> number5 >> number6;
    std::cout << "Maximum is " << maximum(number4, number5, number6) << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input three floating-point numbers: ";
    }
    double number7;
    double number8;
    double number9;
    std::cin >> number7 >> number8 >> number9;
    std::cout << "Maximum is " << maximum(number7, number8, number9) << std::endl;

    return 0;
}

