#include <iostream>
#include <iomanip>
#include <cmath>

int
main()
{
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    int principal = 100000;
    for (int year = 1; year <= 10; ++year) {
        principal += 5 * principal / 100;
        std::cout << std::setw(4) << year << std::setw(21) << principal / 100 << '.' << principal % 100 << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

