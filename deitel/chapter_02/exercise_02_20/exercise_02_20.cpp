#include <iostream>

int
main()
{
    int radius;

    std::cout << "Enter radius ";
    std::cin >> radius;

    if (0 > radius) {
        std::cout << "Error 1: Radius can't be a minus value" << std::endl;
        return 1;
    }

    std::cout << "Diameter is " << 2 * radius << std::endl;
    std::cout << "Circumference is " << 2 * radius * 3.14159 << std::endl;
    std::cout << "Area is " << 3.14159 * radius * radius << std::endl;


    return 0;
}

