#include <iostream>
#include <list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void
input(std::list<int>& l, const int size)
{
    if (isInteractive) {
        std::cout << "Input integers: ";
    }
    int temp;
    for (int i = 0; i < size; ++i) {
        std::cin >> temp;
        l.push_back(temp);
    }
}

void
print(const std::list<int>& l)
{
    for (std::list<int>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ", ";
    }
    std::cout << std::endl;
}

void
reverse(const std::list<int>& list1, std::list<int>& list2)
{
    std::list<int>::const_reverse_iterator it = list1.rbegin();
    for (; it != list1.rend(); ++it) {
        list2.push_back(*it);
    }
    
}

int
main()
{
    std::list<int> list1;
    const int size = 10;
    input(list1, size);
    std::cout << "Integer list contains: ";
    print(list1);

    std::list<int> list2;
    reverse(list1, list2);
    std::cout << "Integer list reverse copy contains: ";
    print(list2);
    return 0;
}

