#include <iostream>
#include <unistd.h>
#include <iomanip>

double calculateCharges(const double parkHour);

int
main()
{
    double totalHour = 0;
    double totalCharge = 0;

    std::cout << "Car" << std::setw(10) << "Hours" << std::setw(12) << "Charge" << std::endl;

    for (int carNumber = 1; carNumber <= 3; ++carNumber) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input car" << carNumber << " park hours: ";
        }
        double parkHour;
        std::cin >> parkHour;
        if (parkHour < 0) {
            std::cout << "Error 1: Invalid park hour" << std::endl;
            return 1;
        }

        totalHour += parkHour;
        totalCharge += calculateCharges(parkHour);
        std::cout << carNumber << std::setw(12) << parkHour << std::setw(12) << calculateCharges(parkHour) << std::endl;
    }
    std::cout << "TOTAL" << std::setw(8) << totalHour << std::setw(12) << totalCharge << std::endl;

    
    return 0;
}

double
calculateCharges(const double parkHour)
{
    if (parkHour < 3) {
        return 2.00;
    }
    if (parkHour >= 18) {
       return 10.00;
    }
    return 2.00 + (parkHour - 3) * 0.50;
}

