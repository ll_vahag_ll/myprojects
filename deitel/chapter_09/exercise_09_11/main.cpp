#include "headers/Rectangle.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::cout << "Input rectangle length: ";
    }
    double length;
    std::cin >> length;

    if (isInteractive) {
        std::cout << "Input rectangle width: ";
    }
    double width;
    std::cin >> width;
    Rectangle rectangle(length, width);
    rectangle.printPerimeter();
    rectangle.printArea();
    
    return 0;
}

