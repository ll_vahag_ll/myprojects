#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>

Time::Time()
{
    time_t currentTime = ::time(0);
    const int second = static_cast<int>(currentTime) % 60;
    currentTime /= 60;
    const int minute = currentTime % 60;
    currentTime /= 60;
    const int hour = currentTime % 24 + 4;
    setTime(hour, minute, second);
}

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    if (hour < 0 || hour > 23) {
        std::cout << "Error 1: Invalid hour" << std::endl;
        ::exit(1);
    }
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void
Time::setMinute(const int minute)
{
    if (minute < 0 || minute > 59) {
        std::cout << "Error 2: Invalid minute" << std::endl;
        ::exit(2);
    }
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

void
Time::setSecond(const int second)
{
    if (second < 0 || second > 59) {
        std::cout << "Error 3: Invalid second" << std::endl;
        ::exit(3);
    }
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int
Time::getHour()
{
    return hour_;
}

int
Time::getMinute()
{
    return minute_;
}

int
Time::getSecond()
{
    return second_;
}

void
Time::printUniversal()
{
    const char prev = std::cout.fill (' ');
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond() << std::endl;;
    std::cout.fill(prev);
}

void
Time::printStandard()
{
    const char prev = std::cout.fill (' ');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(prev);
}

void
Time::tick()
{
    ++second_;
    if (60  == second_) {
        second_ = 0;
        ++minute_;
        if (60 == minute_) {
            minute_ = 0;
            ++hour_;
            if (24 == hour_) {
                hour_ = 0;
            }
        }
    }
}

