#include "headers/Rational.hpp"
#include <iostream>

Rational::Rational(const int numerator, const int denominator)
{
    setNumber(numerator, denominator);
}

void
Rational::setNumber(const int numerator, const int denominator)
{
    const int nod = gcd(numerator, denominator);
    numerator_ = numerator / nod;
    denominator_ = denominator / nod;
}

int
Rational::gcd(int number1, int number2)
{
     if (0 == number2) {
        return number1;
    }
    return gcd(number2, number1 % number2);
}

Rational
Rational::add(const Rational& rhv)
{
    const int numerator = numerator_ * rhv.denominator_ + rhv.numerator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::substract(const Rational& rhv)
{
    const int numerator = numerator_ * rhv.denominator_ - rhv.numerator_ * denominator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::multiplicate(const Rational& rhv)
{
    const int numerator = numerator_ * rhv.numerator_;
    const int denominator = denominator_ * rhv.denominator_;
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::divide(const Rational& rhv)
{
    const int numerator = numerator_ * rhv.denominator_;
    const int denominator = denominator_ * rhv.numerator_;
    const Rational result(numerator, denominator);
    return result;
}

void
Rational::printResult()
{
    std::cout << numerator_ << '/' << denominator_ << std::endl;
}

