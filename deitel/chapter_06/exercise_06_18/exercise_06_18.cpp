#include <iostream>
#include <unistd.h>

int integerPower(int base, int exponent);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input base: ";
    }
    int base;
    std::cin >> base;

    if (base < 0) {
        std::cout << "Error 1: Invaalid base" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input exponent: ";
    }
    int exponent;
    std::cin >> exponent;

    if (exponent < 0 ) {
        std::cout << "Error 1: Invalid exponent" << std::endl;
        return 1;
    }

    std::cout << base << "^" << exponent << " = " << integerPower(base, exponent) << std::endl;
    return 0;
}

int
integerPower(int base, int exponent)
{
    int result = 1;
    while (0 != exponent) {
        if (1 == exponent % 2) {
            result *= base;
        }
        base *= base;
        exponent /= 2;
    }
    return result;
}

