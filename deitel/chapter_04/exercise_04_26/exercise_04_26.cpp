#include <iostream>

int
main()
{
    int variable;
    std::cout << "Input five-dight integer: ";
    std::cin >> variable;
    if (variable > 99999) {
        std::cout << "Error 1: Invalid variable" << std::endl;
        return 1;
    }

    if (variable < 10000) {
        std::cout << "Error 2: Invalid variable" << std::endl;
        return 2;
    }

    int counter = 1;
    int divisionCounter = 10000;
    while (counter <= 2) {
        if (variable % 10 == variable / divisionCounter) {
            variable %= divisionCounter;
            variable /= 10;
            divisionCounter /= 100;
            ++counter;
        } else {
            std::cout << "Isn't polindrom" << std::endl;
            return 0;
        }
    }
    std::cout << "Is a polindrom" << std::endl;
    return 0;
}

