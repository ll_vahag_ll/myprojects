#include "headers/Functions.hpp"
#include <utility> 

namespace cd06 {

template <typename InputIterator, typename UnaryFunction>  
UnaryFunction
for_each(InputIterator first, InputIterator last, UnaryFunction f)
{
    for ( ; first != last; ++first) {
        f(*first);
    }
    return f;
}

template <typename InputIterator, typename UnaryPredicate>  
InputIterator
find_if(InputIterator first, InputIterator last, UnaryPredicate f)
{
    for ( ;first != last; ++first) {
        if (f(*first)) { return first; }
    }
    return last;
}

template <typename ForwardIterator, typename BinaryPredicate>  
ForwardIterator
adjacent_find(ForwardIterator first, ForwardIterator last, BinaryPredicate f)
{
    ForwardIterator fIt = first;
    ++fIt;
    for (; first != last; ++first, ++fIt) {
        if (f(*first, *fIt)) { return  first; }
    }
    return last;
}

template <typename InputIterator, typename ForwardIterator, typename BinaryPredicate>
InputIterator
find_first_of(InputIterator first1, InputIterator last1, ForwardIterator first2, ForwardIterator last2, BinaryPredicate f)
{
    for (; first1 != last1; ++ first1) {
        for (ForwardIterator it = first2; it != last2; ++it) {
            if (f(*first1, *it)) { return first1; }
        }
    }
    return last1;
}

template <typename InputIterator, typename UnaryPredicate, typename Size>  
void
count_if(InputIterator first, InputIterator last, UnaryPredicate f, Size& n)
{
    for (; first != last; ++first) {
        if (f(*first)) {
            ++n;
        }
    }
}

template <typename InputIterator1, typename InputIterator2, typename BinaryPredicate>  
std::pair<InputIterator1, InputIterator2>
mismatch(InputIterator1 first1, InputIterator1 last1, InputIterator2 first2, BinaryPredicate f)
{
    for ( ; first1 != last1 && f(*first1, *first2); ++first1, ++first2) {}
    return std::pair<InputIterator1, InputIterator2>(first1, first2);
}

}

