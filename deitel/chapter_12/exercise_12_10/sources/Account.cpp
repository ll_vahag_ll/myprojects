#include "headers/Account.hpp"
#include <iostream>
#include <cstdlib>

Account::Account(const double balance)
    : balance_(balance)
{
}

void
Account::setBalance(const double balance)
{
    balance_ = (balance >= 0) ? balance : 0;
}

double
Account::getBalance() const
{
    return balance_;
}

void
Account::credit(const double credit)
{
    balance_ += credit; 
}

bool
Account::debit(const double debit)
{
    if (debit > balance_) {
        std::cout << "Debit amount exceeded account balance." << std::endl;
        balance_ = 0.0;
        return false;
    }
    balance_ -= debit;
    return true;
}

