#ifndef __SAVINGSACCOUNT_HPP__
#define __SAVINGSACCOUNT_HPP__

class SavingsAccount
{
private:
    static double annualInterestRate_;
public:
    static void modifyInterestRate(const double rate);
public:
    SavingsAccount(const double balance = 0);
    double calculateMonthlyInterest() const;
    void increaseBalance();
    void printResult() const;
private:
    double savingsBalance_;
};

#endif ///__SAVINGSACCOUNT_HPP__

