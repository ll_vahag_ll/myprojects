progname=exercise_09_04
lib=lib$(progname).a
utest=utest_$(progname)
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03 -I.
BUILDS=builds

ifeq ($(MAKECMDGOALS),)
    BUILD_DIR:=$(BUILDS)/debug
else
    BUILD_DIR:=$(BUILDS)/$(MAKECMDGOALS)
endif

SOURCES:=main.cpp $(wildcard sources/*.cpp)
DEPENDS:=$(patsubst %.cpp,%.d,$(SOURCES))
PREPROCS:=$(patsubst %.cpp,%.ii,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,%.o,$(SOURCES))

LIB_SOURCES:=$(wildcard sources/*.cpp)
LIB_DEPENDS:=$(patsubst %.cpp,%.d,$(LIB_SOURCES))
LIB_PREPROCS:=$(patsubst %.cpp,%.ii,$(LIB_SOURCES))
LIB_ASSEMBLES:=$(patsubst %.cpp,%.s,$(LIB_SOURCES))
LIB_OBJS:=$(patsubst %.cpp,%.o,$(LIB_SOURCES))

UTEST_SOURCES:=main_utest.cpp $(wildcard sources/*.cpp)
UTEST_DEPENDS:=$(patsubst %.cpp,%.d,$(UTEST_SOURCES))
UTEST_PREPROCS:=$(patsubst %.cpp,%.ii,$(UTEST_SOURCES))
UTEST_ASSEMBLES:=$(patsubst %.cpp,%.s,$(UTEST_SOURCES))
UTEST_OBJS:=$(patsubst %.cpp,%.o,$(UTEST_SOURCES))

TEST_INPUTS:=$(wildcard test*.input)
TESTS:=$(patsubst %.input,%,$(TEST_INPUTS))

debug:   CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG

debug:   $(BUILD_DIR) $(BUILD_DIR)/$(lib) qa
release: $(BUILD_DIR) 

qa: $(TESTS)

utest: $(BUILD_DIR)/$(utest)
	./$<

test%: $(BUILD_DIR)/$(progname)
	./$< < $@.input > $@.output || echo "Negative $@..."
	echo PASSED || echo FAILED

$(BUILD_DIR)/$(utest): $(UTEST_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -lgtest -lpthread -o $@

$(BUILD_DIR)/$(lib): $(LIB_OBJS) | .gitignore
	ar -crv $@ $^

$(BUILD_DIR)/$(progname): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@

%.ii: %.cpp
	$(CXX) $(CXXFLAGS) -E $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.cpp,%.d,$<)

%.s: %.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

%.o: %.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

.gitignore:
	echo $(progname) > .gitignore
	echo $(utest)   >> .gitignore

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

install:
	cp headers/*.hpp /usr/local/include
	cp builds/debug/$(lib) /usr/local/lib

clean:
	rm -rf $(BUILDS) *.ii *.d *.s *.o *.output .gitignore $(progname)

.PRECIOUS: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES)

sinclude $(DEPENDS) $(UTEST_DEPENDS)

