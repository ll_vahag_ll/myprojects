#include <iostream>
#include <unistd.h>
#include <cmath>

double roundToInteger(const double number1);
double roundToTenths(const double number1);
double roundToHundredths(const double number1);
double roundToThousandths(const double number1);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    double number1;
    std::cin >> number1;

    std::cout << "Original: " << number1 << std::endl;
    std::cout << "Rounded: " << roundToInteger(number1) << std::endl;
    std::cout << "Rounded to tenths : " << roundToTenths(number1) << std::endl;
    std::cout << "Rounded to hundredths : " << roundToHundredths(number1) << std::endl;
    std::cout << "Rounded to ten thousandths: " << roundToThousandths(number1) << std::endl;

    return 0;
}

double
roundToInteger(const double number1)
{
    return std::floor(number1);
}

double
roundToTenths(const double number1)
{
    return std::floor(number1 * 10 + 0.5) / 10;
}

double
roundToHundredths(const double number1)
{
    return std::floor(number1 * 100 + 0.5) / 100;
}

double
roundToThousandths(const double number1)
{
    return std::floor(number1 * 1000 + 0.5) / 1000;
}

