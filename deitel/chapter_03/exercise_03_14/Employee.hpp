#include <string>

class Employee
{
public:
    Employee (std::string name, std::string surName, int monthSalary);
    void setName(std::string name);
    std::string getName();
    void setSurName (std::string surName);
    std::string getSurName();
    int setMonthSalary (int monthSalary);
    int getMonthSalary();
    int getYearSalary();
    int getRaiseSalary();

private:
    std::string name_;
    std::string surName_;
    int monthSalary_;
};

