#include "headers/Time.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    Time time1;
    time1.printUniversal();
    time1.printStandard();

    if (isInteractive) {
        std::cout << "Input time(hour, minute, second): ";
    }
    int hour, minute, second;
    std::cin >> hour >> minute >> second;

    Time time2(hour, minute, second);
    time2.tick();
    time2.printUniversal();
    time2.printStandard();
    return 0;
}

