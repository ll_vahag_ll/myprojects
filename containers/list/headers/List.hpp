#ifndef __T_LIST_HPP__
#define __T_LIST_HPP__

#include <iostream>

namespace cd06 {

template <typename T> class List;

template <typename T>
std::istream& operator>>(std::istream& in, List<T>& v);

template <typename T>
std::ostream& operator<<(std::ostream& out, const List<T>& v);

template <typename T>
class List
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

private:
    struct Node {
        Node() {};
        Node(const_reference d = 0, Node* n = NULL, Node* p = NULL)
            : data_(d), next_(n), prev_(p) {}
        value_type data_;
        Node* next_;
        Node* prev_;
    };
public:
    class const_iterator {
        friend List<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        
        const const_iterator operator++();
        const const_iterator operator++(value_type);
        const const_iterator operator--();
        const const_iterator operator--(value_type);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        operator bool() const;
    private:
        Node* next() const;
        Node* prev() const;
    private:
        explicit const_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

public:
    class iterator : public const_iterator {
        friend List<value_type>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        value_type& operator*();
        value_type* operator->();
        const iterator& operator=(const iterator& rhv);
        bool operator==(const iterator& rhv) const;
        bool operator!=(const iterator& rhv) const;
    private:
        explicit iterator(Node* ptr);
    };

public:
    class const_reverse_iterator {
        friend List<value_type>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const value_type& operator*() const;
        const value_type* operator->() const;
        
        const const_reverse_iterator operator++();
        const const_reverse_iterator operator++(value_type);
        const const_reverse_iterator operator--();
        const const_reverse_iterator operator--(value_type);
        bool operator==(const const_reverse_iterator& rhv) const;
        bool operator!=(const const_reverse_iterator& rhv) const;
        operator bool() const;
    private:
        Node* next() const;
        Node* prev() const;
    private:
        explicit const_reverse_iterator(Node* ptr);
    private:
        Node* ptr_;
    };

public:
    class reverse_iterator : public const_reverse_iterator {
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();
        value_type& operator*();
        value_type* operator->();
        const reverse_iterator& operator=(const reverse_iterator& rhv);
        bool operator==(const reverse_iterator& rhv) const;
        bool operator!=(const reverse_iterator& rhv) const;
    private:
        explicit reverse_iterator(Node* ptr);
    };

public:
    List();
    List(const size_type size);
    List(const int size, const_reference init = value_type());
    List(const List<value_type>& rhv);
    template <typename InputIterator>
    List(InputIterator f, InputIterator l);
    ~List();
    
    void swap(List<value_type>& rhv);

    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void resize(const size_type size, const_reference init = value_type());
    void clear();

    void push_front(const_reference element);
    void pop_front();
    void push_back(const_reference element);
    void pop_back();
    const_reference front() const;
    reference front();
    const_reference back() const;
    reference back();

    const List<value_type>& operator=(const List<value_type>& rhv);
    bool operator==(const List<value_type>& rhv) const;
    bool operator!=(const List<value_type>& rhv) const;
    bool operator<(const List<value_type>& rhv) const;
    bool operator<=(const List<value_type>& rhv) const;
    bool operator>(const List<value_type>& rhv) const;
    bool operator>=(const List<value_type>& rhv) const;

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();

public:
    iterator insert(iterator pos, const value_type& x);
    void insert(iterator pos, const int n, const T& x);
    template <typename InputIterator>
    void insert(iterator pos, InputIterator f, InputIterator l);
    iterator insert_after(iterator pos, const value_type& x);
    void insert_after(iterator pos, const int n, const value_type& x);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);

    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);

private:
    Node* begin_;
    Node* end_;
};

}

#include "sources/List.cpp"

#endif /// __T_LIST_HPP__

