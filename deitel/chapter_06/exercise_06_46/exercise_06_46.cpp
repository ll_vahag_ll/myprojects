#include <iostream>

int
main()
{
    static int count = 1;
    std::cout << count << std::endl;
    ++count;
    main();
    return 0;
}
///A call stack or function stack is used for several related purposes,
///but the main reason for having one is to keep track of the point to which each active subroutine should return control when it finishes executing.
///A stack overflow occurs when too much memory is used on the call stack.
///Here function main() is called repeatedly and its return address is stored in the stack. After stack memory is full It shows stack overflow error.
