#include "headers/List.hpp"

template <typename NODETYPE>
ListNode<NODETYPE>::ListNode(const NODETYPE& data, ListNode<NODETYPE>* nextPtr)
    : data_(data), nextPtr_(nextPtr)
{
}

template <typename NODETYPE>
NODETYPE
ListNode<NODETYPE>::getData() const
{
    return data_;
}

