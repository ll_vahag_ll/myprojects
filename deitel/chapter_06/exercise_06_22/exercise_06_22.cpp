#include <iostream>
#include <unistd.h>
#include <cassert>

void sideSize(const int side1);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input length: ";
    }

    int side1;
    std::cin >> side1;

    if (side1 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    sideSize(side1);
    return 0;
}

void
sideSize(const int side1)
{
    assert(side1 > 0);
    for (int counter = 1; counter <= side1; ++counter) {
        for (int calculator = 1; calculator <= side1; ++calculator) {
            std::cout << "* ";
        }
        std::cout << std::endl;
    }
}

