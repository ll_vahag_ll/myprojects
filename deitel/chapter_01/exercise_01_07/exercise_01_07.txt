 C++ attracts so much attention today because
 working with it you can program each fragment yourself,
 with it you can write programs of any complexity.
 OOP not only helps in writing a complex piece of code easily,
 but it also allows users to handle and maintain them easily as well.
