#include <iostream>
#include <unistd.h>
#include <vector>

const bool isInteractive = ::isatty(STDIN_FILENO);

namespace cd06 {

template <class InputIterator, class ForwardIterator>
InputIterator
find_first_of(InputIterator first1, InputIterator last1,
              ForwardIterator first2, ForwardIterator last2)
{
    for (; first1 != last1; ++first1) {
        for (ForwardIterator it = first2; it != last2; ++it) {
            if (*first1 == *it) {
                return first1;
            }
        }
    }
    return last1;
}

template <typename T>
void
input(std::vector<T>& v, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (int i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

}

int
main()
{
    if (isInteractive) {
        std::cout << "Input vector size: ";
    }
    int SIZE;
    std::cin >> SIZE;

    std::vector<int> v1;
    cd06::input(v1, SIZE);

    std::vector<int> v2;
    cd06::input(v2, SIZE);

    std::vector<int>::iterator result = cd06::find_first_of(v1.begin(), v1.end(), v2.begin(), v2.end());

    if (result == v1.end()) {
        std::cout << "No elements of v1 are equal to any element of v2" << std::endl;
        return 0;
    }
    std::cout << "First equal elemets pair is " << "{ " << *result << ", " << *result << " }" << std::endl;
    return 0;
}

