#include <iostream>

void printElements(const int SIZE, const int array1[]);
void sortingElements(const int SIZE, int array1[]);

int
main()
{
    const int SIZE = 10;
    int array1[SIZE] = { 9, 2, 11, 24, 5, 8, 27, 15, 33, 7 };
    std::cout << "Unsorted array: ";
    printElements(SIZE, array1);

    sortingElements(SIZE, array1);
    std::cout << "Sorted elements: ";
    printElements(SIZE, array1);
    return 0;
}

void
printElements(const int SIZE, const int array1[])
{
    for (int i = 0; i < SIZE; ++i) {
        std::cout << array1[i] << ", ";
    }
    std::cout << std::endl;
}

void
sortingElements(const int SIZE, int array1[])
{
    for (int i = 0; i < SIZE; ++i) {
        for (int j = i + 1; j < SIZE; ++j) {
            if (array1[j] < array1[i]) {
                const int temp = array1[i];
                array1[i] = array1[j];
                array1[j] = temp;
            }
        }
    }
}

