#include "MiniComputer.hpp"

#include <iostream>

int
main()
{
    /// Create and run MiniComputer
    MiniComputer mini;
    return mini.run();
}

