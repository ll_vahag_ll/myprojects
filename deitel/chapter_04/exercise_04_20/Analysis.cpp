#include <iostream>

#include "Analysis.hpp"

void
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCounter = 1;

    while (studentCounter <= 10) {
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        int result;
        std::cin >> result;

        if (1 == result) {
            ++passes;
            ++studentCounter;
        } else if (2 == result) {
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "Warning 1: Invalid result\nPlease input correct result" << std::endl;
        }
    }
    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;

    if (passes > 8) {
        std::cout << "Raise tuition " << std::endl;
    }
}

