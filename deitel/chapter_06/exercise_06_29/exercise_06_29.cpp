#include <iostream>
#include <unistd.h>

bool perfect(const int number);
void perfectNumberFactors(const int number);

int
main()
{
    for (int number = 1; number <= 1000; ++number) {
        if (perfect(number)) {
            perfectNumberFactors(number);
            std::cout << '/' << number << std::endl;
        }
    }
    return 0;
}

bool
perfect(const int number)
{
    int calculator = 0;
    for (int counter = 1; counter <= number / 2; ++counter) {
        if (0 == number % counter) {
            calculator += counter;
        }
    }
    return (number == calculator);
}

void
perfectNumberFactors(const int number)
{
    int calculator = 0;
    for (int counter = 1; counter <= number / 2; ++counter) {
        if (0 == number % counter) {
            calculator += counter;
            std::cout << '/' << counter;
        }
    }
}

