#include "headers/Queue.hpp"

#include <gtest/gtest.h>
#include <vector>

TEST(QueueTest, DefaultConst)
{
    cd06::Queue<int> stack1;
}

TEST(QueueTest, Equality)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(7);
    stack1.push(7);
    cd06::Queue<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_TRUE(stack1.size() == stack2.size());
}

TEST(QueueTest, Less)
{
    cd06::Queue<int> stack1;
    stack1.push(6);
    stack1.push(7);
    cd06::Queue<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(10);
    EXPECT_TRUE(stack1.size() < stack2.size());
    EXPECT_TRUE(stack1.front() < stack2.front());
    EXPECT_TRUE(stack1.back() < stack2.back());
}

TEST(QueueTest, More)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Queue<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_FALSE(stack1.size() > stack2.size());
}

TEST(QueueTest, LessOrEqual)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Queue<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_TRUE(stack1.size() <= stack2.size());
    stack1.push(7);
    stack1.push(7);
    EXPECT_FALSE(stack1.size() <= stack2.size());

}

TEST(QueueTest, MoreOrEqual)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(7);
    cd06::Queue<int> stack2;
    stack2.push(7);
    stack2.push(7);
    stack2.push(7);
    EXPECT_FALSE(stack1.size() >= stack2.size());
    stack1.push(7);
    stack1.push(7);
    EXPECT_TRUE(stack1.size() >= stack2.size());

}

TEST(QueueTest, Empty)
{
    cd06::Queue<int> stack1;
    EXPECT_TRUE(stack1.empty());
}

TEST(QueueTest, front)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(8);
    EXPECT_EQ(stack1.front(), 7);
}

TEST(QueueTest, pop)
{
    cd06::Queue<int> stack1;
    stack1.push(7);
    stack1.push(8);
    stack1.push(9);
    EXPECT_EQ(stack1.front(), 7);
    stack1.pop();
    EXPECT_EQ(stack1.front(), 8);
    EXPECT_EQ(stack1.size(), 2);
}
   
int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

