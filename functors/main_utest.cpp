#include "headers/Functions.hpp"
#include "headers/Functors.hpp"

#include <gtest/gtest.h>
#include <vector>
#include <list>

template <typename T>
void
print(std::vector<T> v)
{
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

TEST(Functions, for_each)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(4);
    v.push_back(8);
    v.push_back(8);
    v.push_back(5);
    v.push_back(7);

    /// print(v);
    cd06::square<size_t> sqr;
    cd06::for_each(v.begin(), v.end(), sqr);
    /// print(v);
}

TEST(Functions, find_if)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(3);
    v.push_back(11);
    v.push_back(27);
    v.push_back(6);
    v.push_back(7);

    cd06::isEven<size_t> even;
    std::vector<size_t>::iterator it = cd06::find_if(v.begin(), v.end(), even);
    std::cout << *it << std::endl;
}

TEST(Functions, adjacent_find)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(3);
    v.push_back(7);
    v.push_back(6);
    v.push_back(6);
    v.push_back(7);

    cd06::isEqual<size_t> eq;
    std::vector<size_t>::iterator it = cd06::adjacent_find(v.begin(), v.end(), eq);
    std::cout << *it << std::endl;
}

TEST(Functions, find_first_of)
{
    std::vector<size_t> v1;
    v1.push_back(1);
    v1.push_back(3);
    v1.push_back(7);
    v1.push_back(13);
    v1.push_back(6);
    v1.push_back(7);

    std::vector<size_t> v2;
    v2.push_back(2);
    v2.push_back(4);
    v2.push_back(9);
    v2.push_back(13);
    v2.push_back(27);
    v2.push_back(81);

    cd06::isEqual<size_t> eq;
    std::vector<size_t>::iterator it = cd06::find_first_of(v1.begin(), v1.end(), v2.begin(), v2.end(), eq);
    std::cout << *it << std::endl;
}

TEST(Functions, count_if)
{
    std::vector<size_t> v;
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(7);
    v.push_back(7);
    v.push_back(8);
    v.push_back(9);

    cd06::isEven<size_t> even;
    size_t sumOfEven = 0;
    cd06::count_if(v.begin(), v.end(), even, sumOfEven);
    /// std::cout << sumOfEven << std::endl;
}

TEST(Functions, mismatch)
{
    std::vector<size_t> v;
    v.push_back(3);
    v.push_back(2);
    v.push_back(9);
    v.push_back(3);
    v.push_back(7);
    v.push_back(4);
    v.push_back(9);
    v.push_back(8);
    v.push_back(9);

    std::list<size_t> ls1;
    ls1.push_back(3);
    ls1.push_back(2);
    ls1.push_back(9);
    ls1.push_back(3);
    ls1.push_back(7);
    ls1.push_back(5);

    cd06::isEqual<size_t> equal;
    typedef std::vector<size_t>::iterator ItV;
    typedef std::list<size_t>::iterator ItLst;
    std::pair<ItV, ItLst> pIt = cd06::mismatch(v.begin(), v.end(), ls1.begin(), equal);
    std::cout << *pIt.first << " | " << *pIt.second << std::endl;
}


int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
