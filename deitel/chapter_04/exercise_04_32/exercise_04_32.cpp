#include <iostream>

int
main()
{
    std::cout << "Input first side of a triangle: ";
    double side1;
    std::cin >> side1;
    std::cout << "Input second side of a triangle: ";
    double side2;
    std::cin >> side2;
    std::cout << "Input third side of a triangle: ";
    double side3;
    std::cin >> side3;

    if (side1 <= 0) {
        std::cout << "Error 1: Inavalid side" << std::endl;
        return 1;
    }

    if (side2 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    if (side3 <= 0) {
        std::cout << "Error 1: Invalid side" << std::endl;
        return 1;
    }

    if (side1 + side2 < side3) {
        std::cout << "They could not represent the sides of a triangle." << std::endl;
        return 0;
    }

    if (side1 + side3 < side2) {
        std::cout << "They could not represent the sides of a triangle." << std::endl;
        return 0;
    }

    if (side2 + side3 < side1) {
        std::cout << "They could not represent the sides of a triangle." << std::endl;
        return 0;
    }
    std::cout << "They could represent the sides of a triangle." << std::endl;
    return 0;
}

