#include <iostream>
#include <iomanip>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

template <typename T>
void selectionSort(T* const array, const size_t SIZE);

template <typename T>
void swap(T* const element1Ptr, T* const element2Ptr);


template <typename T>
void input(T* const array, const size_t SIZE);

int
main()
{
    if (isInteractive) {
        std::cout << "Input integers: ";
    }
    const size_t SIZE = 10;
    int a1[SIZE];
    input(a1, SIZE);

    std::cout << "Data items in original order\n";
    for (size_t i = 0; i < SIZE; ++i) {
        std::cout << std::setw(4) << a1[i];
    }
    selectionSort(a1, SIZE);
    std::cout << "\nData items in ascending order\n";
    for (size_t j = 0; j < SIZE; ++j) { 
        std::cout << std::setw(4) << a1[j];
    }
    std::cout << std::endl;

    if (isInteractive) {
        std::cout << "Input floating precision numbers: ";
    }
    float a2[SIZE];
    input(a2, SIZE);

    std::cout << "Data items in original order\n";
    for (size_t i = 0; i < SIZE; ++i) {
        std::cout << std::setw(6) << a2[i];
    }
    selectionSort(a2, SIZE);
    std::cout << "\nData items in ascending order\n";
    for (size_t j = 0; j < SIZE; ++j) { 
        std::cout << std::setw(6) << a2[j];
    }
    std::cout << std::endl;
    return 0;
}

template <typename T>
void
selectionSort(T* const array, const size_t SIZE)
{
    for (size_t i = 0; i < SIZE - 1; ++i) {
        size_t smallest = i;
        for (size_t index = i + 1; index < SIZE; ++index) {
            if (array[index] < array[smallest]) {
                smallest = index;
            }
        }
        swap(&array[i], &array[smallest]);
    }
}

template <typename T>
void
swap(T* const element1Ptr, T* const element2Ptr)
{
    const T hold = *element1Ptr;
    *element1Ptr = *element2Ptr;
    *element2Ptr = hold;
}

template <typename T>
void
input(T* const array, const size_t SIZE)
{
    for (size_t i = 0; i < SIZE; ++i) {
        std::cin >> array[i];
    }
}

