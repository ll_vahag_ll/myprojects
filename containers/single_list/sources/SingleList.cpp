#include "headers/SingleList.hpp"

#include <cassert>
#include <algorithm>
#include <cmath>

namespace cd06 {

template <typename T>
std::istream&
operator>>(std::istream& in, SingleList<T>& lst)
{
    char bracket;
    in >> bracket;
    if (bracket != '{') {
        std::cerr << "Error 1: Invalid SingleList!" << std::endl;
        ::exit(1);
    }

    typedef typename SingleList<T>::size_type size_type;
    const size_type s = lst.size();

    typename SingleList<T>::iterator it = lst.begin();
    for (size_t i = 0; i < s; ++i, ++it) {
        in >> *it;
    }

    in >> bracket;
    if (bracket != '}') {
        std::cerr << "Error 2: Invalid SingleList!" << std::endl;
        ::exit(2);
    }

    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const SingleList<T>& lst)
{
    out << "{ ";
    typedef typename SingleList<T>::size_type size_type;
    const size_type s = lst.size();
    typename SingleList<T>::const_iterator it = lst.begin();
    for (size_t i = 0; i < s; ++i, ++it) {
        out << *it << ' ';
    }
    out << '}';
    return out;
}

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
SingleList<T>::SingleList(const size_type size)
    : begin_(NULL)
    , end_(NULL)
{
    for (size_type i = 0; i < size; ++i) {
        push_front(value_type());
    }
}

template <typename T>
SingleList<T>::SingleList(const int size, const_reference init)
    : begin_(NULL)
    , end_(NULL)
{
    for (int i = 0; i < size; ++i) {
        push_front(init);
    }
}

template <typename T>
template <typename InputIterator>
SingleList<T>::SingleList(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
{
    assert(f || f < l);
    for (; f != l; ++f) {
        push_back(*f);
    }
}

template <typename T>
SingleList<T>::SingleList(const SingleList<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
{
    const_iterator it = rhv.begin();
    for (; it != rhv.end(); ++it) {
        this->push_back(*it);
    }
}

template <typename T>
SingleList<T>::~SingleList()
{
    clear();
}

template <typename T>
void
SingleList<T>::swap(SingleList<value_type>& rhv)
{
    Node* temp = begin_;
    begin_ = rhv.begin_;
    rhv.begin_ = temp;
    temp = end_;
    end_ = rhv.end_;
    rhv.end_ = temp;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::get(const size_type index) const
{
    assert(index < size());
    const_iterator it = begin();
    for (size_t i = 0; i < index; ++i, ++it) {}
    return *it;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    if (NULL == begin_) {
        return 0;
    }
    const_iterator it = begin();
    size_type size = 0;
    for (; it.ptr_ != end_->next_; ++it, ++size) {
    }
    assert(size <= max_size());
    return size;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::max_size() const
{
    return static_cast<size_type>(std::pow(2, sizeof(pointer) * 8) / (sizeof(value_type) + sizeof(begin_) + sizeof(end_))); /// For single list
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return (NULL == begin_);
}

template <typename T>
void
SingleList<T>::resize(const size_type size, const_reference init)
{
    size_type thisSize = this->size();
    while (size > thisSize) {
        push_back(init);
        ++thisSize;
    }

    while (size < thisSize) {
        iterator it(end_);
        erase(it);
        --thisSize;
    }
}

template <typename T>
void
SingleList<T>::clear()
{
    while (begin_ != NULL) {
        pop_front();
    }
}

template <typename T>
void
SingleList<T>::push_front(const_reference element)
{
    begin_ = new Node(element, begin_);
    if (NULL == begin_->next_)  {
        end_ = begin_;
    }
}

template <typename T>
void
SingleList<T>::pop_front()
{
    assert(NULL != begin_);
    Node* temp = begin_;
    if (begin_ != end_) {
        begin_ = begin_->next_;
        delete temp;
        return;
    }
    delete begin_;
    begin_ = end_ = NULL;
}

template <typename T>
void
SingleList<T>::push_back(const_reference element)
{
    end_ = new Node(element, end_);
    if (NULL == begin_)  {
        begin_ = end_;
        return;
    }
    end_->next_->next_ = end_;
    end_->next_ = NULL;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::front() const
{
    assert(begin_ != NULL);
    return begin_->data_;
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::front()
{
    assert(begin_ != NULL);
    return begin_->data_;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::back() const
{
    assert(end_ != NULL);
    return end_->data_;
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::back()
{
    assert(end_ != NULL);
    return end_->data_;
}

template <typename T>
const SingleList<T>&
SingleList<T>::operator=(const SingleList<T>& rhv)
{
    iterator it(rhv->begin_);
    for ( ; it->next_ != NULL; ++it) {
        push_back(it->data_);
    }
    return this;
}

template <typename T>
bool
SingleList<T>::operator==(const SingleList<T>& rhv) const
{
    if (size() == rhv.size()) {
        const_iterator it1 = begin();
        const_iterator it2 = rhv.begin();
        for (;it1 != end(); ++it1, ++it2) {
            if (it1 != it2) {
                return false;
            }
        }
        return true;
    }
    return false;
}

template <typename T>
bool
SingleList<T>::operator!=(const SingleList<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
SingleList<T>::operator<(const SingleList<T>& rhv) const
{
    if (size() < rhv.size()) {
        return true;
    }
    if (size() == rhv.size()) {
        const_iterator it1 = begin();
        const_iterator it2 = rhv.begin();
        for ( ; it1 != end(); ++it1, ++it2) {
            if (*it1 > *it2) {
                return false;
            }
            if (*it1 < *it2) {
                return true;
            }
        }
    }
    return false;
}

template <typename T>
bool
SingleList<T>::operator<=(const SingleList<T>& rhv) const
{
    return !(*this > rhv);

}

template <typename T>
bool
SingleList<T>::operator>(const SingleList<T>& rhv) const
{
    return rhv < *this;
}

template <typename T>
bool
SingleList<T>::operator>=(const SingleList<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return iterator(NULL);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos, const value_type& x)
{
    assert(pos.ptr_ != NULL);
    if (NULL == begin_) {
        begin_ = new Node(x);
        begin_ = end_;
        return begin();
    }
    Node* temp = new Node(x, pos.next());
    pos.ptr_->next_ = temp;
    if (pos == iterator(end_)) {
        end_ = temp;
    }

    return iterator(temp);
}

template <typename T>
void
SingleList<T>::insert_after(iterator pos, int n, const value_type& x)
{
    while (n > 0) {
        pos = insert_after(pos, x);
        --n;
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    if (NULL == begin_) {
        while (f != l) {
            push_back(*f);
            ++f;
        }
        return;
    }
    while (f != l) {
        pos = insert_after(pos, *f);
        ++f;
    }
}


template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert(iterator pos, const value_type& x)
{
    insert_after(pos, x);
    iterator it = begin();
    for (; it.ptr_ != pos.ptr_; ++it) {}
    ++pos;
    std::swap(*it, *pos);
    return it;
}

template <typename T>
void
SingleList<T>::insert(iterator pos, int n, const value_type& x)
{
    while (n > 0) {
        pos = insert(pos, x);
        --n;
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(f);
    while (f != l) {
        pos = insert(pos, *f);
        ++f;
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos)
{
    assert(pos != end());
    if (pos.ptr_ == begin_) {
        begin_ = begin_->next_;
        delete pos.ptr_;
        return begin();
    }
    iterator it = begin();
    for (; it.next() != pos.ptr_; ++it) {}
    it.ptr_->next_ = pos.next();

    if (end_ == pos.ptr_) {
        end_ = it.ptr_;
    }
    delete pos.ptr_;
    return iterator(it.next());
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator f, iterator l)
{
    assert(f != end() && l != iterator(end_->next_) && l != f);
    while (f != l) {
        f = erase(f);
    }
    return f;
}

/// const_iterator implementation /////////////////////////////////////

template <typename T>
SingleList<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
SingleList<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator=(const const_iterator& rhv)
{
    this->ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
const T&
SingleList<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
const T*
SingleList<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator++()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
const typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++(value_type)
{
    const_iterator temp;
    temp.ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return this->ptr_ == rhv.ptr_;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return this->ptr_ != rhv.ptr_;
}

template <typename T>
SingleList<T>::const_iterator::operator bool() const
{
    return ptr_ != NULL;
}

template <typename T>
typename SingleList<T>::Node*
SingleList<T>::const_iterator::next() const
{
    return ptr_->next_;
}

/// iterator implementation /////////////////////////////////////

template <typename T>
SingleList<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
SingleList<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv.ptr_)
{
}

template <typename T>
SingleList<T>::iterator::~iterator()
{
    const_iterator::ptr_ = NULL;
}

template <typename T>
T&
SingleList<T>::iterator::operator*()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_->data_;
}

template <typename T>
T*
SingleList<T>::iterator::operator->()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_;
}

template <typename T>
const typename SingleList<T>::iterator&
SingleList<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::ptr_ = rhv.ptr_;
    return *this;
}

}
