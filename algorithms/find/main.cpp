#include <iostream>
#include <unistd.h>
#include <vector>

const bool isInteractive = ::isatty(STDIN_FILENO);

namespace cd06 {

template<class InputIterator, class EqualityComparable>
InputIterator
find(InputIterator first,
     InputIterator last,
     const EqualityComparable& value)
{
    for (; first != last; ++first) {
        if (*first == value) {
            return first;
        }
    }
    return last;
}

template <typename T>
void
input(std::vector<T>& v, const int SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (int i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

}

int
main()
{
    if (isInteractive) {
        std::cout << "Input vector size: ";
    }
    int SIZE;
    std::cin >> SIZE;

    std::vector<int> v;
    cd06::input(v, SIZE);

    if (isInteractive) {
        std::cout << "Input element to find: ";
    }
    int element;
    std::cin >> element;

    std::vector<int>::iterator result = cd06::find(v.begin(), v.end(), element);

    if (result == v.end()) {
        std::cout << "Vector does not contain number " << element << std::endl;
        return 0;
    }
    std::cout << "Vector contains number " << element << std::endl;
    return 0;
}

