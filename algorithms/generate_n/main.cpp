#include <iostream>
#include <cassert>
#include <cstdlib>
#include <vector>
#include <list>

template <typename OutputIterator, typename Size, typename Generator>
OutputIterator
generate_n(OutputIterator f, Size n, Generator gen)
{
    assert(n >= 0);
    for (Size i = 0; i != n; ++f, ++i) {
        *f = gen();
    }
    return f;
}

template <typename T>
void
printVector(const std::vector<T>& rhv)
{
    typename std::vector<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
printList(const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

int
main()
{
    std::vector<int> v(10);
    std::cout << "Vector before generate: ";
    printVector(v);

    generate_n(v.begin(), v.size(), std::rand);
    std::cout << "Vector after generate: ";
    printVector(v);

    std::list<int> l(10);
    std::cout << "List before generate: ";
    printList(l);

    generate_n(l.begin(), v.size(), std::rand);
    std::cout << "List after generate: ";
    printList(l);
    return 0;
}

