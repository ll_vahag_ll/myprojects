#include "headers/Complex.hpp"
#include <iostream>

std::istream&
operator>>(std::istream& input, Complex& in)
{
    double realPart;
    input >> realPart;

    double imaginaryPart;
    input >> imaginaryPart;
    in.setComplex(realPart, imaginaryPart);
    return input;
}

std::ostream&
operator<<(std::ostream& output, const Complex& out)
{
    output << '(' << out.realPart_ << ", " << out.imaginaryPart_ << ')' << std::endl;
    return output;
}

Complex::Complex(const double realPart, const double imaginaryPart)
{
    setComplex(realPart, imaginaryPart);
}

void
Complex::setComplex(const double realPart, const double imaginaryPart)
{
    realPart_ = realPart;
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::operator+(const Complex& rhv) const
{
    const double realPart = realPart_ + rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ + rhv.imaginaryPart_;
    const Complex result(realPart, imaginaryPart);
    return result;
}

Complex
Complex::operator-(const Complex& rhv) const
{
    const double realPart  = realPart_ - rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ - rhv.imaginaryPart_;
    const Complex result(realPart, imaginaryPart);
    return result;
}

Complex
Complex::operator*(const Complex& rhv) const
{
    const double realPart  = realPart_ * rhv.realPart_;
    const double imaginaryPart = imaginaryPart_ * rhv.imaginaryPart_;
    const Complex result(realPart, imaginaryPart);
    return result;
}

bool
Complex::operator==(const Complex& rhv) const
{
    return realPart_ == rhv.realPart_ && imaginaryPart_ == rhv.imaginaryPart_;
}

bool
Complex::operator!=(const Complex& rhv) const
{
    return realPart_ != rhv.realPart_ && imaginaryPart_ != rhv.imaginaryPart_;
}

