#ifndef __FUNCTORS_HPP__
#define __FUNCTORS_HPP__

#include <bits/stdc++.h>
#include <cstdlib>

namespace cd06 {

template <typename T>
struct square
{
    void operator()(T& x = 1) { x *= x; }
};

template <typename T>
struct sum
{
    T operator()(const T& x, const T& y) { return x + y; }
};

template <typename T>
struct isEven
{
    bool operator()(const T& x) { return !(x % 2); }

};

template <typename T>
struct isEqual
{
    bool operator()(const T& x, const T& y) { return x == y; }
};

template <typename Result>
struct Generator
{
    Generator(const int begin = 0, const int end = INT_MAX)
        : begin_(begin), mod_(end - begin) {}
    Result operator()() { return Result(begin_ + ::rand() % mod_); }
private:
    int begin_, mod_;
};

}

#endif /// __FUNCTORS_HPP__

