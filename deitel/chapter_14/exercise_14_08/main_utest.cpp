#include "headers/Array.hpp"

#include <gtest/gtest.h>

TEST(ArrayTest, Print)
{
    Array<int> arr(7);
    Array<float> arrFloat(7);
    arr.print();
    arrFloat.print();
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

