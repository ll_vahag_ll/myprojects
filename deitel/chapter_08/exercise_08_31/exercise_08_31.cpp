#include <iostream>
#include <unistd.h>
#include <cstring>

const bool isInteractive = ::isatty(STDIN_FILENO);
void compareStrings(char* firstLine, char* secondLine, const int numberOfSymbols);

int
main()
{
    if (isInteractive) {
        std::cout << "Input two strings: ";
    }
    const int SIZE = 20;
    char firstLine[SIZE];
    std::cin.getline(firstLine, SIZE);
    char secondLine[SIZE];
    std::cin.getline(secondLine, SIZE);

    if (isInteractive) {
        std::cout << "Input symbols quantity to compare: ";
    }
    int numberOfSymbols;
    std::cin >> numberOfSymbols;
    if (numberOfSymbols < 1) {
        std::cout << "Error 1: Invalid symbol quantity" << std::endl;
        return 1;
    }
    compareStrings(firstLine, secondLine, numberOfSymbols);
    return 0;
}

void
compareStrings(char* firstLine, char* secondLine, const int numberOfSymbols)
{
    const int compare = strncmp(firstLine, secondLine, numberOfSymbols);
    if (compare < 0) {
        std::cout << "First string is less than second" << std::endl;
        return;
    }

    if (compare > 0) {
        std::cout << "First string is greater than second" << std::endl;
        return;
    }
    std::cout << "First string is equal to second" << std::endl;
}

