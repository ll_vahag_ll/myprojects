#include <iostream>
#include <unistd.h>
#include <iomanip>

void celsiusToFarenheit();
void fahrenheitToCelsius();

int
main()
{
    celsiusToFarenheit();
    fahrenheitToCelsius();
    return 0;
}

void
celsiusToFarenheit()
{
    for (int celsiusRow = 0; celsiusRow <= 100; ++celsiusRow) {
        std::cout << celsiusRow << "C - " << celsiusRow * 9 / 5 +32 << "F" << std::endl;
    }
    std::cout << std::endl;
}

void
fahrenheitToCelsius()
{
    for (int fahrenheitRow = 32; fahrenheitRow <= 212; ++fahrenheitRow) {
        std::cout << fahrenheitRow << "F - " << (fahrenheitRow - 32) * 5 / 9 << "C" << std::endl;
    }
}

