#include <iostream>
#include <cassert>
#include <vector>
#include <list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

template <class OutputIterator, class Size, class T>
OutputIterator
fill_N(OutputIterator f, Size n, const T& value)
{
    assert(n >= 0);
    for (Size i = 0; i != n; ++f, ++i) {
        *f = value;
    }
    return f;
}

template <typename T>
void
printVector(const std::vector<T>& rhv)
{
    typename std::vector<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
printList(const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

int
main()
{
    std::vector<int> v(10);
    std::cout << "Vector before fill: ";
    printVector(v);

    if (isInteractive) {
        std::cout << "Input integer to fill: ";
    }
    int value1;
    std::cin >> value1;

    fill_N(v.begin(), v.size(), value1);
    std::cout << "Vector after fill: ";
    printVector(v);

    std::list<int> l(10);
    std::cout << "List before fill: ";
    printList(l);

    if (isInteractive) {
        std::cout << "Input integer to fill: ";
    }
    int value2;
    std::cin >> value2;

    fill_N(l.begin(), l.size(), value2);
    std::cout << "List after fill: ";
    printList(l);
    return 0;
}

