#include "Account.hpp"

#include <iostream>

int
main()
{
    int balance;

    std::cout << "Please input your first account balance: ";
    std::cin >> balance;
    Account account1(balance);
    std::cout << "Please input your second account balance: ";
    std::cin >> balance;
    Account account2(balance);

    std::cout << "Your first account balance is " << account1.getBalance() << std::endl;
    std::cout << "Your second account balance is " << account2.getBalance() << std::endl;

    std::cout << "Transver Menu:" << std::endl;
    std::cout << "    1 - Add money" << std::endl;
    std::cout << "    2 - Take money" << std::endl;
    std::cout << "Choose: ";
    int menu;
    std::cin >> menu;

    if (2 < menu) {
        std::cout << "Error 1: Invalid number" << std::endl;
        return 1;
    }

    if (1 > menu) {
        std::cout << "Error 2: Invalid number" << std::endl;
        return 2;
    }

    if (1 == menu) {
        int credit1;
        int credit2;
        std::cout << "How much do you want to add your first account? ";
        std::cin >> credit1;
        std::cout << "How much do you want to add your second account? ";
        std::cin >> credit2;
        account1.credit(credit1);
        std::cout << "Your first account balance is " << account1.getBalance() << std::endl;
        account2.credit(credit2);
        std::cout << "Your second account balance is " << account2.getBalance() << std::endl;
        return 0;
    }

    int debit1;
    int debit2;
    std::cout << "How much do you want to take from your first account? ";
    std::cin >> debit1;
    std::cout << "How much do you want to take from your second account? ";
    std::cin >> debit2;
    account1.debit(debit1);
    std::cout << "Your first account balance is " << account1.getBalance() << std::endl;
    account2.debit(debit2);
    std::cout << "Your second account balance is "<< account2.getBalance() << std::endl;
    
    return 0;
}

