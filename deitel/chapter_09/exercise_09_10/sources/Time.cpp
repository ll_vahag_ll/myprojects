#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    if (getErrorIndicator() != 0) {
        return;
    }
    setMinute(minute);
    if (getErrorIndicator() != 0) {
        return;
    }
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    const bool isValid = hour > 0 && hour < 24;
    hour_ = isValid ? hour : 0;
    setErrorIndicator(isValid ? 0 : 1);
}

void
Time::setMinute(const int minute)
{
    const bool isValid = minute > 0 && minute < 60;
    minute_ = isValid ? minute : 0;
    setErrorIndicator(isValid ? 0 : 2);
}

void
Time::setSecond(const int second)
{
    const bool isValid = (second > 0 || second < 60);
    second_ = isValid ? second : 0;
    setErrorIndicator(isValid ? 0 : 3);
}

void
Time::setErrorIndicator(const int errorIndicator)
{
    errorIndicator_ = errorIndicator;
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

int
Time::getErrorIndicator() const
{
    return errorIndicator_;
}

void
Time::printUniversal() const
{
    const char prev = std::cout.fill (' ');
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond() << std::endl;;
    std::cout.fill(prev);
}

void
Time::printStandard() const
{
    const char prev = std::cout.fill (' ');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(prev);
}

