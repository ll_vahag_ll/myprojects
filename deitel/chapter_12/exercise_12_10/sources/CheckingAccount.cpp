#include "headers/CheckingAccount.hpp"

CheckingAccount::CheckingAccount(const double balance, const double fee)
    : Account(balance)
    , fee_()
{
    setFee(fee);
}

void
CheckingAccount::setFee(const double fee)
{
    fee_ = (fee < 0.0) ? 0.0 : fee;
}

void
CheckingAccount::chargeFee()
{
    Account::setBalance(getBalance() - fee_);
}

void
CheckingAccount::credit(const double credit)
{
    Account::credit(credit);
    chargeFee();
}

bool
CheckingAccount::debit(const double debit)
{
    const bool success = Account::debit(debit);
    if (success) {
        chargeFee();
        return true;
    }
    return false;
}

double
CheckingAccount::getFee() const
{
    return fee_;
}
