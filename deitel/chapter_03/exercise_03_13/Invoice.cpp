#include "Invoice.hpp"

#include <iostream>

Invoice::Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int pricePerItem)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setItemQuantity(itemQuantity);
    setPricePerItem(pricePerItem);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
}

void
Invoice::setItemQuantity(int itemQuantity)
{
    if (itemQuantity < 0) {
        std::cout << "Warning 1: Invalid quantity\nresetting to 0" << std::endl;
        itemQuantity_ = 0;
        return;
    }

    itemQuantity_ = itemQuantity;
}

void
Invoice::setPricePerItem(int pricePerItem)
{
    if (pricePerItem < 0) {
        std::cout << "Warning 2: Invalid price per item\nresetting to 0" << std::endl;
        pricePerItem_ = 0;
        return;
    }

    pricePerItem_ = pricePerItem;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

std::string
Invoice::getPartDescription()
{
    return partDescription_;
}

int
Invoice::getItemQuantity()
{
    return itemQuantity_;
}

int
Invoice::getPricePerItem()
{
    return pricePerItem_;
}

int
Invoice::getInvoiceAmount()
{
    return itemQuantity_ * pricePerItem_;
}

