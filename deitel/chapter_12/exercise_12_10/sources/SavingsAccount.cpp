#include "headers/SavingsAccount.hpp"

SavingsAccount::SavingsAccount(const double balance, const double interestRate)
    : Account(balance)
    , interestRate_(interestRate)
{
}

void
SavingsAccount::setInterestRate(const double interestRate)
{
    interestRate_ = interestRate;
}

double
SavingsAccount::calculateInterest() const
{
    return getBalance() * interestRate_;
}

void
SavingsAccount::increaseBalance()
{
    Account::setBalance(calculateInterest());
}

int
SavingsAccount::getInterestRate() const
{
    return interestRate_;
}

