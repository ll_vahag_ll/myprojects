#ifndef __PACKAGE_HPP__
#define __PACKAGE_HPP__

#include <iostream>
#include <cstring>

class Package
{
public:
    Package(const std::string& name, const std::string& city,
            const std::string& state, const std::string& sender,
            const std::string& recipient, const int& zip,
            const std::string& address, const double& weight,
            const double& cost);

    void setName(const std::string& name) { name_ = name; }
    void setCity(const std::string& city) { city_ = city; }
    void setState(const std::string& state) { state_ = state; }
    void setZip(const int& zip) { zip_ = zip; }
    void setAddress(const std::string& address) { address_ = address; } 
    void setSender(const std::string& sender) { sender_ = sender; } 
    void setRecipient(const std::string& recipient) { recipient_ = recipient; }
    void setWeight(const double& weight) { weight_ = weight; }
    void setCost(const double& cost) { cost_ = cost; }
    std::string getName() const { return name_; } 
    std::string getCity() const { return city_; } 
    std::string getState() const { return state_; } 
    int getZip() const { return zip_; } 
    std::string getAddress() const { return address_; } 
    std::string getSender() const { return sender_; } 
    std::string getRecipient() const { return recipient_; }
    double getWeight() const { return weight_; }
    double calculateCost() const;
private:
    std::string name_;
    std::string city_;
    std::string state_;
    std::string sender_;
    std::string recipient_;
    int zip_; 
    std::string address_; 
    double weight_;
    double cost_;
};

#endif /// __PACKAGE_HPP__

