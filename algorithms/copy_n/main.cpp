#include <iostream>
#include <cassert>
#include <vector>
#include <list>

template <typename InputIterator, typename Size, typename OutputIterator>
OutputIterator
copy_n(InputIterator first, Size count, OutputIterator result)
{
    for (; count > 0; ++first, ++result, --count) {
        *result = *first;
    }
    return result;
}

template <typename T>
void
printVector(const std::vector<T>& rhv)
{
    typename std::vector<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
printList(const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it = rhv.begin();
    for ( ; it != rhv.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

int
main()
{
    std::vector<int> v;
    const int SIZE = 10;
    for (int i = 0; i != SIZE; ++i) {
        v.push_back(i * i);
    }

    std::cout << "List before copy_n: ";
    std::list<int> l;
    printList(l);

    std::cout << "List after copy_n: ";
    copy_n(v.begin(), v.size(), std::back_inserter(l));
    printList(l);
    return 0;
}

