#include <iostream>

#include <unistd.h>

int
main()
{
    for (int counter = 1; counter <=5; ++counter) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Quotient " << counter << ": " ;
        }
        int quotient;
        std::cin >> quotient;
        if (quotient < 1 || quotient > 30) {
            std::cout << "Error 1: Invalid quotient" << std::endl;
            return 1;
        }
        for (int row = 1; row <= quotient; ++row) {
            std::cout << '*';
        }
        std::cout << std::endl;
    }
    return 0;
}


