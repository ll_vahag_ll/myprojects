#include <string>

class MiniComputer
{
public:
    MiniComputer();
    MiniComputer(std::string name);
    void setComputerName(std::string name);
    std::string getComputerName();
    int run();

private:
    void printProgramNameAndVersion();
    
    void printMainMenu();
    void printMainMenuItem(int index);

    void printLoadMenu();
    void printLoadMenuItem(int index);
    char getRegisterName(int index);
    void validateLoad(int load);
    void executeLoadIntoRegister(int load);
    void printRegisterMenu(int load);
    void setRegisterValue(int load, int registerValue);
    int getRegisterValue(int load);
    void executeLoadCommand();
    
    void printPrintMenu();
    void printPrintMenuItem(int index);
    void validatePrint(int print);
    void printPrintRegisterMenu();
    void printPrintRegisterMenuItem(int index);
    void executePrintCommand();

    int getCommandFromUser();
    void validateCommand(int command);
    void executeCommand(int command);
    void executeExitCommand();
    void executeStoreCommand();
    void executeAddCommand();
    void executePrintRegister();
    void executePrintString();

private:
    std::string computerName_;
    int registerA_;
    int registerB_;
    int registerC_;
    int registerD_;

};

