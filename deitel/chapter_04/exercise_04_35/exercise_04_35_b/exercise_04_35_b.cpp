#include <iostream>

int
main()
{
    std::cout << "Input accuracy of e: ";
    int accuracy;
    std::cin >> accuracy;

    if (accuracy <= 0) {
        std::cout << "Error 1: Invalid accuracy" << std::endl;
        return 1;
    }

    double factorial = 1;
    double e = 1;
    int counter = 1;

    while (counter <= accuracy) {
        factorial *= counter;
        ++counter;
        e += 1 / factorial;
    }

    std::cout << "e = " << e << std::endl;
    return 0;
}

