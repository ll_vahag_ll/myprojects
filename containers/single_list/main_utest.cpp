#include "headers/SingleList.hpp"

#include <gtest/gtest.h>

TEST(SingleListTest, DefaultConstSize)
{
    cd06::SingleList<int> lst;
    EXPECT_EQ(lst.size(), 0);
}

TEST(SingleListTest, CopyConstructor)
{
    typedef cd06::SingleList<int> Sl;
    typedef cd06::SingleList<int>::iterator iterator;
    Sl lst1(5, 7);
    Sl lst2(lst1);
    iterator it = lst1.begin();
    iterator it1 = lst2.begin();
    for (; it != lst1.end(); ++it) {
        EXPECT_EQ(*it, *it1);
    }
}

TEST(SingleListTest, RangeConst)
{
    cd06::SingleList<int> lst1(7, 7);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator f = lst1.begin();
    ++f;
    iterator l = f;
    ++l;
    ++l;
    ++l;
    ++l;
    ++l;
    ///std::cout << lst1 << std::endl;
    cd06::SingleList<int> lst2(f, l);
    ///std::cout << lst2 << std::endl;
}

TEST(SingleListTest, Destructor)
{
    cd06::SingleList<int> lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
}

TEST(SingleListTest, PushFront1)
{
    cd06::SingleList<int> lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    EXPECT_EQ(lst.size(), 3);
}

TEST(SingleListTest, PopFront1)
{
    cd06::SingleList<int> lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    lst.pop_front();
    EXPECT_EQ(lst.size(), 2);
}

TEST(SingleListTest, PushBack1)
{
    cd06::SingleList<int> lst;
    lst.push_back(5);
    lst.push_back(6);
    lst.push_back(7);
    EXPECT_EQ(lst.size(), 3);
}

TEST(SingleListTest, Resize1)
{
    cd06::SingleList<int> lst;
    lst.push_back(1);
    lst.push_back(2);
    lst.push_back(3);
    lst.push_back(4);
    lst.push_back(5);
    lst.resize(3, 2);
    EXPECT_EQ(lst.size(), 3);

}

TEST(SingleListTest, Resize2)
{
    cd06::SingleList<int> lst;
    lst.push_back(1);
    lst.push_back(2);
    lst.push_back(3);
    lst.push_back(4);
    lst.push_back(5);
    lst.resize(7, 2);
    EXPECT_EQ(lst.size(), 7);

}

TEST(SingleListTest, Operators)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1(9, 6);
    Sl lst2(7, 9);
    EXPECT_FALSE(lst1 == lst2);
    EXPECT_TRUE(lst1 != lst2);
    EXPECT_TRUE(lst1 >= lst2);
    Sl lst3(4, 4);
    lst3.push_back(3);
    EXPECT_FALSE(lst1 < lst3);
    EXPECT_TRUE(lst1 > lst3);
    EXPECT_FALSE(lst1 <= lst3);
}

TEST(SingleListTest, insert_after1)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ///std::cout << lst1 << std::endl;
    lst1.insert_after(it, 7);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Insert1)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ///std::cout << lst1 << std::endl;
    lst1.insert(it, 7);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, insert_after2)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ///std::cout << lst1 << std::endl;
    lst1.insert_after(it, 5, 0);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Insert2)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ///std::cout << lst1 << std::endl;
    lst1.insert(it, 5, 0);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, insert_after3)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    lst1.push_back(6);
    lst1.push_back(7);

    Sl lst2;
    lst2.push_back(0);
    lst2.push_back(0);
    lst2.push_back(0);
    lst2.push_back(0);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    iterator f = lst2.begin();
    iterator l = lst2.end();
    ///std::cout << lst1 << std::endl;
    lst1.insert_after(it, f, l);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Insert3)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    lst1.push_back(6);
    lst1.push_back(7);

    Sl lst2;
    lst2.push_back(0);
    lst2.push_back(0);
    lst2.push_back(0);
    lst2.push_back(0);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    iterator f = lst2.begin();
    iterator l = lst2.end();
    ///std::cout << lst1 << std::endl;
    lst1.insert(it, f, l);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Erase1)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    lst1.push_back(6);
    lst1.push_back(7);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ///std::cout << lst1 << std::endl;
    lst1.erase(it);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Erase2)
{
    typedef cd06::SingleList<int> Sl;
    Sl lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    lst1.push_back(6);
    lst1.push_back(7);
    typedef cd06::SingleList<int>::iterator iterator;
    iterator f = lst1.begin();
    iterator l = f;
    ++f;
    ++l;
    ++l;
    ++l;
    ++l;
    ++l;
    ///std::cout << lst1 << std::endl;
    lst1.erase(f, l);
    ///std::cout << lst1 << std::endl;
}

TEST(SingleListTest, Swap)
{
    cd06::SingleList<int> lst1(5, 6);
    cd06::SingleList<int> lst2(7, 2);
    EXPECT_EQ(lst1.size(), 5);
    EXPECT_EQ(lst2.size(), 7);
    lst1.swap(lst2);
    EXPECT_EQ(lst1.size(), 7);
    EXPECT_EQ(lst2.size(), 5);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

