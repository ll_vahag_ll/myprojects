#include "headers/Array.hpp"
#include <iostream>

template <typename T>
Array<T>::Array(size_t arraySize)
    : size_((arraySize > 0 ? arraySize : 10))
    , ptr_(new T[size_])
{
    for (size_t i = 0; i < size_; ++i) {
        ptr_[i] = 0;
    }
}

template <typename T>
Array<T>::~Array()
{
    if (ptr_ != NULL) {
        delete[] ptr_;
        ptr_ = NULL;
    }
} 

template <typename T>
void
Array<T>::print() const
{
    std::cout << "Array<T>" << std::endl;
}

Array<float>::Array(const size_t arraySize)
    : size_(arraySize)
    , ptr_(new float[size_])
{
    for (size_t i = 0; i < size_; ++i) {
        ptr_[i] = 0;
    }
}

Array<float>::~Array()
{
    if (ptr_ != NULL) {
        delete [] ptr_;
        ptr_ = NULL;
    }
} 

void
Array<float>::print() const
{
    std::cout << "Array<float>" << std::endl;
}

