#include <iostream>
#include <vector>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);


template <typename T>
void
bubbleSort(std::vector<T>& v, const size_t SIZE)
{
    for (size_t i = 0; i < SIZE; ++i) {
        bool temp = true;
        for (size_t j = 1; j < SIZE; ++j) {
            if (v[j] < v[j - 1]) {
                std::swap(v[j], v[j - 1]);
                temp = false; 
            }
        }
        if (temp) { break; }
    }
}

template <typename T>
void
print(std::vector<T>& v, const size_t SIZE)
{
    for (size_t i = 0; i < SIZE; ++i) {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
void
input(std::vector<T>& v, const size_t SIZE)
{
    if (isInteractive) {
        std::cout << "Input numbers: ";
    }

    for (size_t i = 0; i < SIZE; ++i) {
        T element;
        std::cin >> element;
        v.push_back(element);
    }
}

int
main()
{
    const size_t SIZE = 10;
    std::vector<int> v;
    input(v, SIZE);
    std::cout << "Vector before sort: ";
    print(v, SIZE);
    bubbleSort(v, SIZE);
    std::cout << "Vector after sort: ";
    print(v, SIZE);
    return 0;
}

