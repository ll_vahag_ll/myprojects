#include "headers/Set.hpp"

#include <cassert>
#include <algorithm>
#include <cmath>

namespace cd06 {

template<typename Data>
Set<Data>::Set()
    : root_(NULL)
{
}

template<typename Data>
Set<Data>::Set(const Set& rhv)
    : root_(NULL)
{
}

template<typename Data>
template<typename InputIterator>
Set<Data>::Set(InputIterator f, InputIterator l)
    : root_(NULL)
{
    /// Not implemented yet
}

template<typename Data>
Set<Data>::~Set()
{
   clear(root_); 
}

template<typename Data>
typename Set<Data>::size_type
Set<Data>::size() const
{
    size_type size = 0;
    if (empty()) { return size; }
    for (const_iterator it = begin(); it != end(); ++it, ++size) {}
    return size;
}

template<typename Data>
typename Set<Data>::size_type
Set<Data>::max_size() const
{
    return static_cast<size_type>(std::pow(2, sizeof(pointer) * 8) / (sizeof(value_type) + sizeof(root_))); /// For set
}

template<typename Data>
bool
Set<Data>::empty() const
{
    return (NULL == root_);
}

template<typename Data>
void
Set<Data>::clear(Node* p)
{
    if (p != NULL) {
        clear(p->left_);
        clear(p->right_);
        delete p;
        p = NULL;
    }
    root_ = NULL;
}

template<typename Data>
const Set<Data>&
Set<Data>::operator=(const Set& rhv)
{
    /// Not implemented yet
}

template<typename Data>
bool
Set<Data>::operator==(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
bool
Set<Data>::operator!=(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
bool
Set<Data>::operator<(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
bool
Set<Data>::operator<=(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
bool
Set<Data>::operator>(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
bool
Set<Data>::operator>=(const Set<value_type>& rhv) const
{
   /// Not implemented yet
   return true;
}

template<typename Data>
void
Set<Data>::preOrder(Node* n, const size_type d)
{
    if (NULL == n) {
        return;
    }
    visit(n, d);
    preOrder(n->left_, d + 1);
    preOrder(n->right_, d + 1);
}

template<typename Data>
void
Set<Data>::inOrder(Node* n, const size_type d)
{
    if (NULL == n) {
        return;
    }
    inOrder(n->left_, d + 1);
    visit(n, d);
    inOrder(n->right_, d + 1);
}


template<typename Data>
void
Set<Data>::postOrder(Node* n, const size_type d)
{
    if (NULL == n) {
        return;
    }
    postOrder(n->left_, d + 1);
    postOrder(n->right_, d + 1);
    visit(n, d);
}

template<typename Data>
void
Set<Data>::visit(const Node* n, const size_type d)
{
    std::cout << *n << ' ' << d << ' ';
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::begin()
{
    iterator it(root_);
    while (it.ptr_->left_ != NULL) {
        it.ptr_ = it.ptr_->left_;
    }
    return it;
}

template<typename Data>
typename Set<Data>::const_iterator
Set<Data>::begin() const
{
    const_iterator it(root_);
    while (it.ptr_->left_ != NULL) {
        it.ptr_ = it.ptr_->left_;
    }
    return it;
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::end()
{
    return iterator(root_->parent_);

}

template<typename Data>
typename Set<Data>::const_iterator
Set<Data>::end() const
{
    return const_iterator(root_->parent_);
}

template<typename Data>
typename Set<Data>::const_iterator
Set<Data>::leftMost(Node* pos) const
{
    assert(pos!= NULL);
    while (pos->left_ != NULL) {
        pos = pos->left_;
    }
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::leftMost(Node* pos)
{
    assert(pos!= NULL);
    while (pos->left_ != NULL) {
        pos = pos->left_;
    }
}

template<typename Data>
typename Set<Data>::const_iterator
Set<Data>::rightMost(Node* pos) const
{
    assert(pos!= NULL);
    while (pos->right_ != NULL) {
        pos = pos->right_;
    }
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::rightMost(Node* pos)
{
    assert(pos != NULL);
    while (pos->right_ != NULL) {
        pos = pos->right_;
    }
}

template<typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(const value_type& x)
{
    return insert(iterator(root_), x);
}

template<typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(iterator pos, const value_type& x)
{
    goUp(pos, x);
    const bool isAdded = goDownAndInsert(pos, x);
    balance(pos);
    return std::pair<iterator, bool>(pos, isAdded);
}

template<typename Data>
template <typename InputIterator>
void
Set<Data>::insert(InputIterator f, InputIterator l)
{
    
}

template<typename Data>
bool
Set<Data>::goDownAndInsert(iterator& it, const value_type& x)
{
    if (x < *it) {
        if (it.left().ptr_ == NULL) {
            it.createLeft(x);
            return true;
        }
        return goDownAndInsert(it.goLeft(), x);
    }
    if (x > *it) {
        if (it.right().ptr_ == NULL) {
            it.createRight(x);
            return true;
        }
        return goDownAndInsert(it.goRight(), x);
    }
    return false;
}

template<typename Data>
void
Set<Data>::goUp(iterator& it, const value_type& x)
{
    if (!it || !it.parent() || x == *it) {
        return;
    }
    iterator temp = (x < *it) ? it.firstLeftParent(x) : it.firstRightParent(x);
    if (temp.isRoot()) {
        return;
    }
    const bool isRightPlace = (x < *it) ? (*temp < x) : (*temp > x);
    if (isRightPlace) {
        return;
    }
    it = temp;
    goUp(temp, x);
}

template<typename Data>
void
Set<Data>::balance(iterator& it)
{
    if (NULL == it.ptr_) { return; }
    const int factor = it.balance();
    iterator tempL = it.left();
    iterator tempR = it.right();
    if (factor > 1) {
        if (tempL.balance() < 0) {
            rotateRight(tempL);
        }
        rotateLeft(it);
    } else if (factor <= -1) {
        if (tempR.balance() > 0) {
            rotateLeft(tempR);
        }
        rotateRight(it);
    }
    balance(it.goParent());
}

template<typename Data>
void
Set<Data>::rotateLeft(iterator& it)
{
    iterator itParent = it.parent(), itLeft = it.left(), itLeftRight = itLeft.right();
    if (itParent != end()) {
        const bool isLeftParent = it.isLeftParent();
        isLeftParent ? itParent.setRight(itLeft) : itParent.setLeft(itLeft);
    } else {
        root_ = itLeft.ptr_;
    }
    it.setLeft(itLeftRight);
    if (itLeftRight.ptr_ != NULL) {
        itLeftRight.setParent(it);
    }
    itLeft.setRight(it);
    it.setParent(itLeft);
    itLeft.setParent(itParent);
    it = itLeft;
}

template<typename Data>
void
Set<Data>::rotateRight(iterator& it)
{
    iterator itParent = it.parent(), itRight = it.right(), itRightLeft = itRight.left();
    if (itParent != end()) {
        const bool isLeftParent = it.isLeftParent();
        isLeftParent ? itParent.setRight(itRight) : itParent.setLeft(itRight);
    } else {
        root_ = itRight.ptr_;
    }
    it.setRight(itRightLeft);
    if (itRightLeft.ptr_ != NULL) {
        itRightLeft.setParent(it);
    }
    itRight.setLeft(it);
    it.setParent(itRight);
    itRight.setParent(itParent);
    it = itRight;
}

template <typename Data>
int
Set<Data>::iterator::balance()
{
    return height(const_iterator::ptr_->left_) - height(const_iterator::ptr_->right_);
}

template <typename Data>
int
Set<Data>::iterator::height(Node* ptr)
{
    if (NULL == ptr) {
        return -1;
    }
    return std::max(height(ptr->left_), height(ptr->right_)) + 1;
}

/// _______________________const_iterator implementation___________________________


template<typename Data>
Set<Data>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template<typename Data>
Set<Data>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template<typename Data>
Set<Data>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template<typename Data>
Set<Data>::const_iterator::~const_iterator()
{
    if (ptr_ != NULL) {
        delete ptr_;
        ptr_ = NULL;
    }
}

template<typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator=(const const_iterator& rhv)
{
    this->ptr_ = rhv.ptr_;
    return *this;
}

template<typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template<typename Data>
typename Set<Data>::Node*
Set<Data>::const_iterator::operator->() const
{
    return ptr_;
}

template<typename Data>
bool
Set<Data>::const_iterator::operator!() const
{
    return (NULL == ptr_);
}

template<typename Data>
bool
Set<Data>::const_iterator::operator==(const const_iterator& rhv) const
{
    return (this->ptr_ == rhv.ptr_);
}

template<typename Data>
bool
Set<Data>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return (this->ptr_ != rhv.ptr_);
}

template<typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator++()
{
    if (isRoot()) {
        ptr_ = leftMost(ptr_->right_);
        return ptr_;
    }
    if (ptr_->right_ != NULL) {
        ptr_ = ptr_->right_;
        return ptr_;
    }
    ptr_ = firstRightParent(*ptr_);
    return ptr_;
}

template<typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator--()
{
    /// Not implemented yet
}

template<typename Data>
bool
Set<Data>::const_iterator::isRoot() const
{
    return ptr_->parent_ == NULL;
}

template<typename Data>
bool
Set<Data>::const_iterator::isRightParent() const
{
    assert(ptr_->parent_ != NULL);
    return ptr_->parent_->left_ == ptr_;
}

template<typename Data>
bool
Set<Data>::const_iterator::isLeftParent() const
{
    assert(ptr_->parent_ != NULL);
    return ptr_->parent_->right_ == ptr_;
}

template<typename Data>
void
Set<Data>::const_iterator::createLeft(const_reference initialValue)
{
    assert(ptr_->left_ == NULL);
    ptr_->left_ = new Node(initialValue, ptr_);
    ptr_ = ptr_->left_;
}

template<typename Data>
void
Set<Data>::const_iterator::createRight(const_reference initialValue)
{
    assert(ptr_->right_ == NULL);
    ptr_->right_ = new Node(initialValue, ptr_);
    ptr_ = ptr_->right_;
}

/// ______________________iterator implementation___________________________


template<typename Data>
Set<Data>::iterator::iterator()
    : const_iterator(NULL)
{
}

template<typename Data>
Set<Data>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template<typename Data>
Set<Data>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv.ptr_)
{
}

template<typename Data>
Set<Data>::iterator::~iterator()
{
        const_iterator::ptr_ = NULL;
}

template<typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::operator=(const iterator& rhv)
{
    const_iterator::ptr_ = rhv.ptr_;
    return *this;
}

template<typename Data>
typename Set<Data>::const_reference
Set<Data>::iterator::operator*()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_->data_;
}

template<typename Data>
typename Set<Data>::Node*
Set<Data>::iterator::operator->()
{
    assert(const_iterator::ptr_ != NULL);
    return const_iterator::ptr_;
}

template<typename Data>
bool
Set<Data>::iterator::operator==(const iterator& rhv) const
{
    return const_iterator::ptr_ == rhv.ptr_;
}

template<typename Data>
bool
Set<Data>::iterator::operator!=(const iterator& rhv) const
{
    return const_iterator::ptr_ != rhv.ptr_;
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::parent() const
{
    return iterator(const_iterator::ptr_->parent_);
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::left() const
{
    return iterator(const_iterator::ptr_->left_);
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::right() const
{
    return iterator(const_iterator::ptr_->right_);
}

template<typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goParent()
{
    const_iterator::ptr_ = const_iterator::ptr_->parent_;
    return *this;
}

template<typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goLeft()
{
    assert(const_iterator::ptr_->left_ != NULL);
    const_iterator::ptr_ = const_iterator::ptr_->left_;
    return *this;
}

template<typename Data>
typename Set<Data>::iterator&
Set<Data>::iterator::goRight()
{
    assert(const_iterator::ptr_->right_ != NULL);
    const_iterator::ptr_ = const_iterator::ptr_->right_;
    return *this;
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::firstLeftParent(const value_type& x)
{
    while (const_iterator::ptr_->parent_ != NULL) {
        if (const_iterator::ptr_ == const_iterator::ptr_->parent_->right_) {
            const_iterator::ptr_ = const_iterator::ptr_->parent_;
            const_iterator::ptr_->data_ = x;
            return iterator(const_iterator::ptr_);
        }
        const_iterator::ptr_ = const_iterator::ptr_->parent_;
    }
    return iterator(const_iterator::ptr_);
}

template<typename Data>
typename Set<Data>::iterator
Set<Data>::iterator::firstRightParent(const value_type& x)
{
    while (const_iterator::ptr_->parent_ != NULL) {
        if (const_iterator::ptr_ == const_iterator::ptr_->parent_->left_) {
            const_iterator::ptr_ = const_iterator::ptr_->parent_;
            const_iterator::ptr_->data_ = x;
            return iterator(const_iterator::ptr_);
        }
        const_iterator::ptr_ = const_iterator::ptr_->parent_;
    }
    return iterator(const_iterator::ptr_);
}


template<typename Data>
void
Set<Data>::iterator::setParent(iterator& it)
{
    const_iterator::ptr_->parent_ = it.ptr_;
}

template<typename Data>
void
Set<Data>::iterator::setLeft(iterator& it)
{
    const_iterator::ptr_->left_ = it.ptr_;
}

template<typename Data>
void
Set<Data>::iterator::setRight(iterator& it)
{
    const_iterator::ptr_->right_ = it.ptr_;
}

}

