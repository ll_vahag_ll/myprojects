#include <iostream>

int
main()
{
    int product = 1;
    for (int oddNumber = 1; oddNumber <= 15; oddNumber += 2) {
        product *= oddNumber;
    }
    std::cout << product << std::endl;

    return 0;
}

