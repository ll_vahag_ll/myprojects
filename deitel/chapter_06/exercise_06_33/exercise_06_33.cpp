#include <iostream>
#include <unistd.h>
#include <cassert>

int qualityPoints(const int average);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input average: ";
    }

    int average;
    std::cin >> average;

    if (average < 0) {
        std::cout << "Error 1: Invalid average" << std::endl;
        return 1;
    }

    std::cout << "Quality point is " << qualityPoints(average) << std::endl;
    return 0;
}

int
qualityPoints(const int average)
{
    assert("average < 0 && average > 100");
    switch (average / 10) {
    case 10: case 9: return 4;
    case 8: return 3;
    case 7: return 2;
    case 6: return 1;
    default: return 0;
    }
    return 0;
}

