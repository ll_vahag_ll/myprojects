#include <iostream>
#include <unistd.h>

int
main()
{
    double totalRetailValue = 0;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input product number (-1 to end): ";
        }
        int productNumber;
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        }
        if (productNumber < 1 || productNumber > 5) {
            std::cout << "Error 1: Invalid product number" << std::endl;
            return 1;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input quantity sold: ";
        }
        int quantitySold;
        std::cin >> quantitySold;
        if (quantitySold < 0) {
            std::cout << "Error 2: Invalid sold quantity" << std::endl;
            return 2;
        }

        switch (productNumber) {
        case 1: totalRetailValue += quantitySold * 2.98; break;
        case 2: totalRetailValue += quantitySold * 4.50; break;
        case 3: totalRetailValue += quantitySold * 9.98; break;
        case 4: totalRetailValue += quantitySold * 4.49; break;
        case 5: totalRetailValue += quantitySold * 6.87; break;
        }
    }
    std::cout << "The total retail value of all products sold: " << totalRetailValue << std::endl;
    return 0;
}

